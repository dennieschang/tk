unit block;

interface

uses
   Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
   System.Classes, Vcl.Graphics,
   Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons,
   Vcl.ExtCtrls,
   Vcl.Grids, Vcl.DBGrids, Vcl.Menus, FireDAC.Stan.Intf, FireDAC.Stan.Option,
   FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
   FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
   FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
   TfBlock = class(TForm)
      Panel1: TPanel;
      BitBtn1: TBitBtn;
      BitBtn2: TBitBtn;
      Label1: TLabel;
      DBGrid1: TDBGrid;
      PopupMenu1: TPopupMenu;
      D1: TMenuItem;
      q_blacksheep_: TFDQuery;
      ds_blacksheep: TDataSource;
      q_blacksheep_bsid: TFDAutoIncField;
      q_blacksheep_mcid: TSmallintField;
      q_blacksheep_bsdtcreate: TDateTimeField;
      q_blacksheep_bspgphone: TWideStringField;
      q_blacksheep_bsdvno: TWideStringField;
      q_blacksheep_bsreason: TSmallintField;
      q_blacksheep_bsremark: TWideStringField;
      procedure D1Click(Sender: TObject);
      procedure DBGrid1TitleClick(Column: TColumn);
      procedure BitBtn2Click(Sender: TObject);
      procedure FormCreate(Sender: TObject);
      procedure FormShow(Sender: TObject);
      procedure BitBtn1Click(Sender: TObject);
      procedure FormClose(Sender: TObject; var Action: TCloseAction);
      procedure q_blacksheep_NewRecord(DataSet: TDataSet);
      procedure q_blacksheep_bsreasonSetText(Sender: TField;
          const Text: string);
      procedure q_blacksheep_bsreasonGetText(Sender: TField; var Text: string;
          DisplayText: Boolean);
      procedure q_blacksheep_bsdvnoSetText(Sender: TField; const Text: string);
      procedure q_blacksheep_bsdvnoGetText(Sender: TField; var Text: string;
          DisplayText: Boolean);
   private
      { Private declarations }
      gb_Asc: Boolean;
      gs_Field: String;
      firstShow: Boolean;
   public
      { Public declarations }
   end;

var
   fBlock: TfBlock;

implementation

uses CDMsg, unitSendCar, unitGlobel;

{$R *.dfm}

procedure TfBlock.BitBtn1Click(Sender: TObject);
begin
   Close;
end;

procedure TfBlock.BitBtn2Click(Sender: TObject);
begin
   q_blacksheep_.Append;
end;

procedure TfBlock.D1Click(Sender: TObject);
begin
   if gf_ShowMsg('確定要刪除這筆資料嗎？', 'CSR', mtConfirmation, 1) = mrNo then
      Exit;

   q_blacksheep_.Delete;
end;

procedure TfBlock.DBGrid1TitleClick(Column: TColumn);
begin
   if not AnsiSameText(Column.FieldName, 'bsremark') then begin
      if gs_Field = Column.FieldName then
         gb_Asc := not gb_Asc
      else begin
         gs_Field := Column.FieldName;
         gb_Asc := True;
      end;

      q_blacksheep_.Close;
      q_blacksheep_.SQL.Text := 'select * from blacksheep where mcid = ' +
          IntToStr(MotorCade.iID) + '   and bspgphone = ' +
          QuotedStr(frmSendCar.cds_ExtBuffer_.FieldByName('ebPhone').AsString) +
          ' order by ' + gs_Field;

      if not gb_Asc then
         q_blacksheep_.SQL.Text := q_blacksheep_.SQL.Text + ' desc';

      q_blacksheep_.Open();
   end;
end;

procedure TfBlock.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   q_blacksheep_.CheckBrowseMode;
   Action := caHide;
end;

procedure TfBlock.FormCreate(Sender: TObject);
begin
   firstShow := True;
   // DBGrid1.Columns[0].PickList.Assign(frmSendCar.cmbDrvNO.Items);
end;

procedure TfBlock.FormShow(Sender: TObject);
begin
   if firstShow then begin
      firstShow := False;
      DBGrid1.Columns[0].PickList.Assign(frmSendCar.cmbDrvNO.Items);
   end;

   q_blacksheep_.Close;
   q_blacksheep_.SQL.Text := 'select * from blacksheep where mcid = ' +
       IntToStr(MotorCade.iID) + '   and bspgphone = ' +
       QuotedStr(frmSendCar.cds_ExtBuffer_.FieldByName('ebPhone').AsString);
   q_blacksheep_.Open();
   gs_Field := '';
   gb_Asc := True;
end;

procedure TfBlock.q_blacksheep_bsdvnoGetText(Sender: TField; var Text: string;
    DisplayText: Boolean);
var
   i: Integer;
begin
   Text := Sender.AsString;
   // for i := 0 to DBGrid1.Columns[0].PickList.Count - 1 do
   // if AnsiPos(Sender.AsString, DBGrid1.Columns[0].PickList[i]) > 0 then
   // begin
   // Text := DBGrid1.Columns[0].PickList[i];
   // Break;
   // end;
end;

procedure TfBlock.q_blacksheep_bsdvnoSetText(Sender: TField;
    const Text: string);
var
   ipos: Integer;
   sTemp: String;
begin
   ipos := pos(' ', Text) - 1;

   if ipos > 0 then
      sTemp := Copy(Text, 1, ipos)
   else
      sTemp := Text;

   Sender.AsString := sTemp;
end;

procedure TfBlock.q_blacksheep_bsreasonGetText(Sender: TField; var Text: string;
    DisplayText: Boolean);
begin
   case Sender.AsInteger of
      1:
         Text := '1 服務態度不佳';
      2:
         Text := '2 車輛不乾淨';
      3:
         Text := '3 駕駛習慣不好';
      4:
         Text := '4 車輛太老舊';
      5:
         Text := '5 繞路';
      6:
         Text := '6 強加車費';
      9:
         Text := '9 其他';
   end;
end;

procedure TfBlock.q_blacksheep_bsreasonSetText(Sender: TField;
    const Text: string);
begin
   if Text = '' then
      Sender.Value := null
   else
      Sender.AsInteger := StrToInt(Text[1]);
end;

procedure TfBlock.q_blacksheep_NewRecord(DataSet: TDataSet);
begin
   q_blacksheep_.FieldByName('bsdtcreate').AsDateTime := Now;
   q_blacksheep_.FieldByName('mcid').AsInteger := MotorCade.iID;
   q_blacksheep_.FieldByName('bspgphone').AsString :=
       frmSendCar.cds_ExtBuffer_.FieldByName('ebPhone').AsString;
end;

end.
