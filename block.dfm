object fBlock: TfBlock
  Left = 0
  Top = 0
  Caption = #23458#25142#40657#21517#21934
  ClientHeight = 335
  ClientWidth = 559
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 559
    Height = 65
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 557
      Height = 28
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = #23458#25142#38651#35441' 0911619955'#40657#21517#21934
      Layout = tlCenter
    end
    object BitBtn1: TBitBtn
      Left = 467
      Top = 30
      Width = 89
      Height = 31
      Caption = #38364#38281
      Kind = bkClose
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object BitBtn2: TBitBtn
      Left = 3
      Top = 30
      Width = 101
      Height = 31
      Caption = #26032#22686
      Default = True
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BitBtn2Click
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 65
    Width = 559
    Height = 270
    Align = alClient
    DataSource = ds_blacksheep
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -19
    Font.Name = #32048#26126#39636
    Font.Style = []
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -16
    TitleFont.Name = #32048#26126#39636
    TitleFont.Style = []
    OnTitleClick = DBGrid1TitleClick
    Columns = <
      item
        Expanded = False
        FieldName = 'bsdvno'
        Title.Caption = #21496#27231#21488#34399
        Width = 145
        Visible = True
      end
      item
        Alignment = taLeftJustify
        Expanded = False
        FieldName = 'bsreason'
        PickList.Strings = (
          '1 '#26381#21209#24907#24230#19981#20339
          '2 '#36554#36635#19981#20094#28136
          '3 '#39381#39387#32722#24931#19981#22909
          '4 '#36554#36635#22826#32769#33290
          '5 '#32350#36335
          '6 '#24375#21152#36554#36027
          '9 '#20854#20182)
        Title.Alignment = taCenter
        Title.Caption = #29702'  '#30001
        Width = 141
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'bsremark'
        Title.Alignment = taCenter
        Title.Caption = #20633'  '#35387
        Width = 229
        Visible = True
      end>
  end
  object PopupMenu1: TPopupMenu
    Left = 452
    Top = 212
    object D1: TMenuItem
      Caption = #21034#38500'(&D)'
      OnClick = D1Click
    end
  end
  object q_blacksheep_: TFDQuery
    OnNewRecord = q_blacksheep_NewRecord
    Connection = DM1.FDConnection1
    SQL.Strings = (
      'select * from blacksheep where bspgphone = :tel')
    Left = 268
    Top = 164
    ParamData = <
      item
        Name = 'TEL'
        DataType = ftString
        FDDataType = dtWideString
        ParamType = ptInput
      end>
    object q_blacksheep_bsid: TFDAutoIncField
      FieldName = 'bsid'
      Origin = 'bsid'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object q_blacksheep_mcid: TSmallintField
      AutoGenerateValue = arDefault
      FieldName = 'mcid'
      Origin = 'mcid'
    end
    object q_blacksheep_bsdtcreate: TDateTimeField
      AutoGenerateValue = arDefault
      FieldName = 'bsdtcreate'
      Origin = 'bsdtcreate'
    end
    object q_blacksheep_bspgphone: TWideStringField
      AutoGenerateValue = arDefault
      FieldName = 'bspgphone'
      Origin = 'bspgphone'
    end
    object q_blacksheep_bsdvno: TWideStringField
      AutoGenerateValue = arDefault
      FieldName = 'bsdvno'
      Origin = 'bsdvno'
      OnGetText = q_blacksheep_bsdvnoGetText
      OnSetText = q_blacksheep_bsdvnoSetText
      Size = 10
    end
    object q_blacksheep_bsreason: TSmallintField
      AutoGenerateValue = arDefault
      FieldName = 'bsreason'
      Origin = 'bsreason'
      OnGetText = q_blacksheep_bsreasonGetText
      OnSetText = q_blacksheep_bsreasonSetText
    end
    object q_blacksheep_bsremark: TWideStringField
      AutoGenerateValue = arDefault
      FieldName = 'bsremark'
      Origin = 'bsremark'
      Size = 100
    end
  end
  object ds_blacksheep: TDataSource
    DataSet = q_blacksheep_
    Left = 356
    Top = 168
  end
end
