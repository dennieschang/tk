Unit PSO;

interface

uses
   Winapi.Windows, System.SysUtils, System.Classes, System.Variants,
   System.DateUtils, IdURI, IdGlobal,
   FireDAC.Stan.Intf, FireDAC.Stan.Option,
   FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
   FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, Data.DB,
   FireDAC.Comp.Client, FireDAC.Comp.DataSet, HTTPApp, System.Types, Math;

type
   TQueueStatus = (qsIn, qsOutSys, qsOutSelf, qsOutGiveup);
   TCarProp = (cpCxno, cpQuid, cpSit, cpPaicheCancel, cpRestorePaiBan);
   TQueueProp = (qpName);
   TrecVarArray = array of Variant;
   TProcessDataEvent = procedure(pTempRcv: String) of object;

   TPSO = class
   private
      FDConnection: TFDConnection;
      Fmcid: Integer;
      cds_mtrcd_: TFDMemTable;
      FStepFlag: String;
      FTStrings: TStrings;
      FCancelPaich: Boolean;
      procedure SetMcid(Value: Integer);

      // PS Code Group
      function UpdateDrvQueuetx(qupid, dvid: string; mcid: word; dvno: String;
          at_Status: TQueueStatus): String;

      function UpdateCallTxPickup(cxno: string; dvid: string; lng, lat: double;
          cxstu: byte): Boolean; // quid : String): boolean;
      function UpdateCALLTXoflnglat(cxno: string; lng, lat: double;
          cxstu: byte): Boolean;

      function gf_GetQuid(qupid: string): String;

      function gf_GetQupid(carList_id: Integer; quid: String): String;
      function gf_CheckCSRuser(mcid: Integer; as_ID, as_Pwd: String): String;
      function gf_UpdateCallTxCxSuccess(as_cxno, as_success: String): String;
      function gf_GetDvno(dvid: String): String;
      function gf_GetMcid(dvid: String): Integer;
      function gf_GetCalltx(dvid: String): String;

      function gf_ExecuteSQL(carList_id: Integer; av_SQL: Variant): String;

      // main
      procedure gp_ShowMsg(mcid: Integer; as_Msg: String);
      procedure Log(const Value: string);

      // W-140516 7. ���Q���� �]VTA/�q��APP�^
      function gf_PaicheCancel(carList_id: Integer;
          car_id, cxno: String): String;
      function InsertMsgouttx(dvid: Integer; pmotxt: string): Boolean;

      // W-140516 8. CSR �e�T���H�q��
      function gf_NotifyMsg(pmcid, pdvid: Integer; pdvno, msg: String): String;

      function gf_RestorePaiBan(carList_id: Integer;
          car_id, qupid: String): String;

      function gf_Get0Value(carList_id: Integer; as_SQL: String): Variant;
      function gf_Get1Record(carList_id: Integer; as_SQL: String): TrecVarArray;
      // jieshu 14/7/6 �B�z�L�^��

      procedure gp_CheckCarLive(carList_id: Integer; qupid, cxno: string);
      // private
      function updateCarByID(carList_id: Integer; carID, dvno: string;
          carLng, carLat: double; lnglattype: string; SocketIP: string;
          drvstatus: Integer; var resultError: string;
          dvName, as_Term: String): string;

      function getStrBycc(var Str: string; cc: string): string; // ����r��q cc ��m�I��
      function pickTaxiByfind(carList_id: Integer; carID: string;
          cxno, cxname, cxtel, cxfraddr, cxtoaddr, cxgotime, cxgodis,
          cxfrlocref: string): String;
      function WritePmsg(dvid: Integer; cxno, cxname, cxtel, cxfraddr, cxtoaddr,
          cxgotime, cxgodis, cxfrlocref: string; ai_BidRun: byte): Boolean;

      function WaitTaxiAns(carList_id: Integer;
          carID, qupid, cxno, dvno, queuetx_pid: string): string; // ���ݥq���^�Ь����n�D
      function updateAns(carList_id: Integer; car_id: string; Carans: byte;
          cxno: string): Boolean; // �N�q���������ݨD���׫O�s

      function assignMission2Car(dvid: Integer; cxno, cxname, cxtel, cxfraddr,
          cxtoaddr, cxgotime, cxgodis, cxfrlocref: string;
          ai_BidRun: byte): string;
      function ConvertDegreeToRadians(const Degrees: double): double;
      function DistanceBetweenPoint2(const StartLat, StartLong, EndLat,
          EndLong: double): double;

      function goQiangBiaoCar(carList_id: Integer; cxfrlng, cxfrlat: double;
          cxtolng, cxtolat: double; cxname, cxtel, cxface, cxfrlocref, cxfraddr,
          cxtolocref, cxtoaddr, cxno, cxgotime, cxgodis: string;
          var dvnmList, ls_Term: String; ai_BidQty, ai_BidRun: byte): Integer;

      function AcarSitToDrvStatus(sit: Integer): Integer;

   const
      ls_Field: Array [0 .. 13] of String = ('dvflagNew', 'dvflagNoSmoke',
          'dvflagRV', 'dvflagWheelChair', 'dvflagPet', 'dvflagbigpet',
          'dvflagcarcharge', 'dvflagbill', 'dvflagextnew', 'dvflagnopinang',
          'dvflagtinymove', 'dvflagstoreshipping', 'dvflagbike', 'dvflagcarry');

   public
      Constructor Create; overload;
      destructor Destroy(); override;
      function GetQueueID(carList_id: Integer; cxfrlng, cxfrlat: double;
          var theQueueName: string; var theQueueCount: Integer): string;

      function search_qterritory(mcid: Integer; long, lat: double;
          var theQueueName: string): String;
   public
      property Connection: TFDConnection read FDConnection write FDConnection;
      property Lines: TStrings read FTStrings write FTStrings;
      property mcid: Integer read Fmcid write SetMcid;
      property dvno[dvid: String]: String read gf_GetDvno;
      property StepFlag: String read FStepFlag write FStepFlag;
      property CancelPaich: Boolean read FCancelPaich write FCancelPaich;

   public
      ProcessData: TProcessDataEvent;
      function GetData(pSQL: String; const iPack: Integer = -1): Variant;
      // procedure  ProcessData(pTempRcv:String); virtual; abstract;

      function gf_ProcessPaiche(as_Msg: String): String;

   end;

implementation

constructor TPSO.Create;
begin
   inherited;
   cds_mtrcd_ := TFDMemTable.Create(nil);
   FCancelPaich := False;
end;

destructor TPSO.Destroy;
begin
   cds_mtrcd_.Close;
   cds_mtrcd_.Free;
   FDConnection := Nil;
   FreeAndNil(FDConnection);
   inherited;
end;

function TPSO.GetData(pSQL: String; const iPack: Integer = -1): Variant;
begin
   if pSQL <> '' Then
      Result := '';

end;

procedure TPSO.SetMcid(Value: Integer);
var
   aFDQuery: TFDQuery;
begin
   if Fmcid <> Value then begin
      Fmcid := Value;
      aFDQuery := TFDQuery.Create(nil);
      try
         aFDQuery.Connection := FDConnection;
         aFDQuery.SQL.Clear;
         aFDQuery.SQL.Add('select * from mtrcd where mcid=:mcid');
         aFDQuery.Params.ParamByName('mcid').AsInteger := Value;
         aFDQuery.Open();
         aFDQuery.FetchAll;
         cds_mtrcd_.CopyDataSet(aFDQuery, [coStructure, coRestart, coAppend]);
         aFDQuery.Close;
      finally
         aFDQuery.Free;
      end;
   end;

end;

// queue in and out for driver
function TPSO.UpdateDrvQueuetx(qupid, dvid: string; mcid: word; dvno: String;
    at_Status: TQueueStatus): String;
var
   gtime: Tdatetime;
   ls_quid, asql1: String;
   aFDQuery: TFDQuery;
   aFDCommand: TFDCommand;
   aCDS: TFDQuery;
begin
   if (dvno = '') or (mcid = null) then
      exit;

   gtime := Now;
   aCDS := TFDQuery.Create(nil);
   aFDQuery := TFDQuery.Create(nil);
   aFDCommand := TFDCommand.Create(nil);
   try
      with aFDQuery Do begin

         Connection := FDConnection;
         try
            asql1 := 'select qupid,quid from queuetx where mcid = ' +
                IntToStr(mcid) + '   and dvno = ''' + dvno +
                '''   and qxstatus = 0';

            aCDS.Connection := FDConnection;
            aCDS.SQL.Clear;
            aCDS.SQL.Add(asql1);
            aCDS.Open();

            if at_Status = qsIn then begin

               if aCDS.IsEmpty then // �S�������Z���
               begin
                  Close;
                  SQL.Clear;
                  ls_quid := gf_GetQuid(qupid);
                  // W-140429  3. ��q��n�J�ƯZ���AVTA�N insert queuetx set mcid,dvno,qxdtin,dvid qxstatus = 0
                  SQL.Add('INSERT INTO queuetx SET mcid=:a1,qupid=:a2,dvno=:a3,qxdtin=:a4,dvid=:a5,qxstatus = 0, quid = '
                      + ls_quid);
                  Params.ParamByName('a1').AsInteger := mcid;
                  Params.ParamByName('a2').AsString := qupid;
                  Params.ParamByName('a3').AsString := dvno;
                  Params.ParamByName('a4').AsDateTime := gtime;
                  Params.ParamByName('a5').AsString := dvid;
                  ExecSQL;
               end
               else if (qupid <> aCDS.FieldByName('qupid').AsString) and
                   (aCDS.FieldByName('qupid').AsString <> '') then // �n�i���Z�P�����Z���P
               begin // �����Z
                  UpdateDrvQueuetx(aCDS.FieldByName('qupid').AsString, dvid,
                      mcid, dvno, qsOutSelf);
                  UpdateDrvQueuetx(qupid, dvid, mcid, dvno, at_Status); // ���s�i�Z

               end;
            end
            else begin

               if Not aCDS.IsEmpty then // �S�������Z���
               begin
                  with aFDCommand Do begin
                     Connection := FDConnection;
                     CommandText.Clear;
                     CommandText.Add
                         ('UPDATE queuetx SET qxdtout=:a1, qxstatus = :a2' +
                         ' WHERE mcid = :w1 and qupid = :w2 and dvid = :w3 and qxstatus = 0');
                     Params.ParamByName('a1').AsDateTime := gtime;
                     Params.ParamByName('a2').AsInteger := Ord(at_Status);
                     Params.ParamByName('w1').AsInteger := mcid;
                     Params.ParamByName('w2').AsString := qupid;
                     Params.ParamByName('w3').AsString := dvid;
                     Execute();
                  end;
               end;
            end;

            Result := '';
         except
            on E: Exception do
               Result := E.Message;
         end;

         Active := False;
         aCDS.Close;

      end;
   finally
      if Assigned(aFDCommand) then
         aFDCommand.Free;
      if Assigned(aFDQuery) then
         aFDQuery.Free;
      if Assigned(aCDS) then
         aCDS.Free;
   end;
end;

function TPSO.UpdateCALLTXoflnglat(cxno: string; lng, lat: double;
    cxstu: byte): Boolean;
var
   aFDCommand: TFDCommand;
   gtime: Tdatetime;
begin
   gtime := Now;
   aFDCommand := TFDCommand.Create(nil);
   with aFDCommand Do begin
      try
         Connection := FDConnection;
         Close;
         CommandText.Clear;
         CommandText.Add
             ('UPDATE calltx SET cxoflong = :a1, cxoflat = :a2, cxdtfinish = :a5 '
             + ', cxdtcsrclick = NOW() WHERE cxno = :a3');
         Params.ParamByName('a1').AsFloat := lng;
         Params.ParamByName('a2').AsFloat := lat;
         Params.ParamByName('a3').AsInteger := StrToIntDef(cxno, 0);
         Params.ParamByName('a5').AsDateTime := gtime;

         Execute();
         Result := true;
      finally
         Active := False;
         Free;
      end;
   end;
end;

// W-140429 13. �q�󱵨�ȤH�n�AVTA �nupdate cxdtpickup
function TPSO.UpdateCallTxPickup(cxno, dvid: string; lng, lat: double;
    cxstu: byte): Boolean; // quid: String): boolean;
var
   aFDCommand: TFDCommand;
begin
   aFDCommand := TFDCommand.Create(nil);
   with aFDCommand Do begin
      try
         Connection := FDConnection;
         CommandText.Clear;
         CommandText.Add('UPDATE calltx SET cxsuccess = :b1, cxonlong = :a1,' +
             ' cxonlat = :a2, cxdtpickup = :a4, cxdtcsrclick = NOW() ' +
             ' WHERE cxno = :a3');
         Params.ParamByName('b1').AsInteger := 1; // �q������H�A�]���������\
         Params.ParamByName('a1').AsFloat := lng;
         Params.ParamByName('a2').AsFloat := lat;
         Params.ParamByName('a3').AsInteger := StrToIntDef(cxno, 0);
         Params.ParamByName('a4').AsDateTime := Now;
         Execute();
         Result := true;
      finally
         Free;
      end;
   end;
end;

function TPSO.gf_GetQuid(qupid: string): String;
var
   i: Integer;
   aCDS1: TFDQuery;
begin
   Result := '';
   { Done select quid from queue where pid = qupid }
   aCDS1 := TFDQuery.Create(nil);
   try
      aCDS1.Connection := FDConnection;
      aCDS1.SQL.Clear;
      aCDS1.SQL.Add('select quid from queue where pid =' + qupid);
      aCDS1.Open();
      Result := aCDS1.FieldByName('quid').AsString;
      aCDS1.Close;
   finally
      if Assigned(aCDS1) then
         aCDS1.Free;
   end;

end;

function TPSO.gf_GetCalltx(dvid: String): String;
var
   aFDQuery: TFDQuery;
   ls_Name, ls_Tel: String;

   function lf_DateValue(as_FN: String): String;
   begin
      if aFDQuery.FieldByName(as_FN).IsNull then
         Result := ''
      else
         Result := FormatDateTime('yyyy/mm/dd hh:nn:ss',
             aFDQuery.FieldByName(as_FN).AsDateTime);
   end;

begin
   aFDQuery := TFDQuery.Create(nil);
   try
      with aFDQuery Do begin

         Connection := FDConnection;
         try

            Close;
            // Ū���q���̫ᬣ����
            SQL.Clear;
            SQL.Add('select cxfrlong, cxfrlat, cxtolong, cxtolat, b.pgnm, cxpgphone,'
                + '       cxfrlocref, cxfrloc, cxtolocref, cxtoloc, cxno, cxdtpickup,'
                + '       cxdtconfirm, cxdtpgnotfound, cxdtfinish, b.pgdatatodrv, a.mcid,'
                + '       dvno' +
                '  from calltx a, psgr b where a.pgid = b.pgid and dvid = ' +
                dvid + ' order by cxdtcreate desc limit 1'); //
            Open;
            // cxdtconfirm is NOT null and cxdtpgnotfound is null and cxdtfinish is null'
            if not IsEmpty and not FieldByName('cxdtconfirm').IsNull and
                FieldByName('cxdtpgnotfound').IsNull and
                FieldByName('cxdtfinish').IsNull then begin
               if FieldByName('cxdtpickup').IsNull then
                  Result := 'F' // �e������
               else
                  Result := 'G'; // ����ȤH
               // 5. �p�G���� �� pgdatatodrv �O 0, �e���q���������T����̡A�Ч�m�W�M�q�ܲM���ťաC
               if FieldByName('pgdatatodrv').AsInteger = 0 then begin
                  ls_Name := '';
                  ls_Tel := '';
               end
               else begin
                  ls_Name :=
                      HttpEncode(UTF8Encode(FieldByName('pgnm').AsString));
                  ls_Tel := FieldByName('cxpgphone').AsString;
               end;

               Result := Result + ',' + FormatFloat('0.000000',
                   FieldByName('cxfrlong').AsFloat) + '-' +
                   FormatFloat('0.000000', FieldByName('cxfrlat').AsFloat) + ','
                   + FormatFloat('0.000000', FieldByName('cxtolong').AsFloat) +
                   '-' + FormatFloat('0.000000', FieldByName('cxtolat').AsFloat)
                   + ',' + ls_Name // Acar^.Pname
                   + ',' + ls_Tel // Acar^.Pphone
                   + ',' + '' // Acar^.Pface
                   + ',' + HttpEncode
                   (UTF8Encode(FieldByName('cxfrlocref').AsString))
               // Acar^.Plocation
                   + ',' + HttpEncode
                   (UTF8Encode(FieldByName('cxfrloc').AsString)) + ',' +
                   HttpEncode(UTF8Encode(FieldByName('cxtolocref').AsString))
               // Acar^.Plocation
                   + ',' + HttpEncode
                   (UTF8Encode(FieldByName('cxtoloc').AsString)) + ',' +
                   FieldByName('cxno').AsString + ',' + '0'
               // �ɶ�Acar^.Plocation
                   + ',' + '0' // �Z��
                   + ';';
            end
            else
               Result := '';

         except
            on E: Exception do
               Result := 'E,' + E.Message;
         end;
         Active := False;
      end;
   finally
      if Assigned(aFDQuery) then
         aFDQuery.Free;
   end;
end;

function TPSO.gf_CheckCSRuser(mcid: Integer; as_ID, as_Pwd: String): String;
var

   aFDQuery: TFDQuery;
begin
   if as_ID = '' then begin
      Result := '�b���]' + as_ID + '�^���~�I';
      exit;
   end
   else
      Result := '';

   if as_ID = '0' then
      exit; // csid = 0 �N�O���ȦۧU�A�����ˬd�b���K�X jieshu 14/6/6

   aFDQuery := TFDQuery.Create(nil);
   with aFDQuery Do begin
      try
         Connection := FDConnection;
         try
            SQL.Clear;
            SQL.Add('select uspw from user where usid = ' + quotedstr(as_ID) +
                ' and mcid = ' + IntToStr(mcid));
            Open;

            if IsEmpty then
               Result := '�b�����~�I'
            else if as_Pwd <> FieldByName('uspw').AsString then
               Result := '�K�X���~�I'
         except
            on E: Exception do
               Result := '�X�{���~�G' + E.Message;
         end;
      finally
         Active := False;
         Free;
      end;
   end;
end;

function TPSO.gf_UpdateCallTxCxSuccess(as_cxno, as_success: String): String;
var
   aSql: String;
begin
   Result := '';
   if (as_success = '') or (as_cxno = '') then
      exit;

   aSql := 'Update CallTx Set cxsuccess = ' + as_success +
       ', cxdtvtacancel = now() where cxno = ' + as_cxno;

   Result := gf_ExecuteSQL(0, aSql);
end;

function TPSO.gf_GetQupid(carList_id: Integer; quid: String): String;
var
   aCDS1: TFDQuery;
begin
   Result := '';
   { Done select qupid from queue where mcid, quid }
   aCDS1 := TFDQuery.Create(nil);
   try
      aCDS1.Connection := FDConnection;
      aCDS1.SQL.Clear;
      aCDS1.SQL.Add('select pid from queue where mcid=' + IntToStr(carList_id) +
          ' and quid =' + quid);
      aCDS1.Open();
      Result := aCDS1.FieldByName('pid').AsString;
      aCDS1.Close;
   finally
      if Assigned(aCDS1) then
         aCDS1.Free;
   end;

end;

function TPSO.gf_GetDvno(dvid: String): String;
var
   aCDS1: TFDQuery;
begin
   Result := '';
   if dvid = '' then
      exit;

   { Done .... select dvno from drv where dvid }
   aCDS1 := TFDQuery.Create(nil);
   try
      aCDS1.Connection := FDConnection;
      aCDS1.SQL.Clear;
      aCDS1.SQL.Add('select dvno from drv where dvid=' + dvid);
      aCDS1.Open();

      if Not aCDS1.IsEmpty then
         Result := aCDS1.FieldByName('dvno').AsString
      else
         Result := '��ID�G' + dvid;
      aCDS1.Close;
   finally
      if Assigned(aCDS1) then
         aCDS1.Free;
   end;

end;

function TPSO.gf_GetMcid(dvid: String): Integer;
var
   aCDS1: TFDQuery;
begin
   Result := 0;
   if dvid = '' then
      exit;
   { Done select mcid from drv where dvid =dvid }
   aCDS1 := TFDQuery.Create(nil);
   try
      aCDS1.Connection := FDConnection;
      aCDS1.SQL.Clear;
      aCDS1.SQL.Add('select mcid from drv where dvid=' + dvid);
      aCDS1.Open();
      Result := aCDS1.FieldByName('mcid').AsInteger;
      aCDS1.Close;
   finally
      if Assigned(aCDS1) then
         aCDS1.Free;
   end;

end;

function TPSO.gf_ExecuteSQL(carList_id: Integer; av_SQL: Variant): String;
var
   li_i: Integer;
   aFDCommand: TFDCommand;
begin
   Result := '';
   aFDCommand := TFDCommand.Create(nil);
   try
      with aFDCommand Do begin

         Connection := FDConnection;
         if VarIsArray(av_SQL) then begin
            for li_i := 0 to VarArrayHighBound(av_SQL, 0) do begin

               try
                  CommandText.Clear;
                  CommandText.Add(av_SQL[li_i]);
                  ResourceOptions.CmdExecMode := amNonBlocking;
                  Execute();
               except
                  on E: Exception do begin
                     Result := '�X�{���~�G' + E.Message + '�A' + av_SQL[li_i];
                     gp_ShowMsg(carList_id, Result);
                  end;
               end;
            end;
         end
         else begin
            try
               CommandText.Clear;
               CommandText.Add(av_SQL);
               Execute();
            except
               on E: Exception do begin
                  Result := '�X�{���~�G' + E.Message + '�A' + av_SQL;
                  gp_ShowMsg(carList_id, '�X�{���~�G' + E.Message + '�A' + av_SQL);
               end;
            end;
         end;
         Active := False;

      end;
   finally
      if Assigned(aFDCommand) then
         aFDCommand.Free;

   end;
end;

procedure TPSO.gp_ShowMsg(mcid: Integer; as_Msg: String);
var
   ls_Msg: String;
begin
   ls_Msg := '[' + FormatFloat('0000', mcid) + ']: ' + as_Msg;
   Log(ls_Msg);
end;

procedure TPSO.Log(const Value: string);
var
   LogFile: String;
begin
   if Assigned(ProcessData) then
      ProcessData('M,' + Value + #10);

end;

// ���� �]VTA/�q��APP�^
function TPSO.gf_PaicheCancel(carList_id: Integer;
    car_id, cxno: String): String;
var
   i, cancelcode: Integer;
   dvno, msgtxt, err: String;
   asql1, asql2, asql7: String;
   aCDS1: TFDQuery;
begin
   { Done
     select dvno, dvstatus, dvdtreport from drv where dvid

     1. check > 500 then showerror .... driver lost ...offline, can't cancel exit
     2.'select dvid, cxdtpickup, csid from calltx WHERE cxno = ' +
     cxno and dvid = car_id;
     3.  if notfound.... no the calltx
     4. dxdtpickup is Not null then the driver has pick up the psgr yet  , error msg

     5. msgouttx..... to driver msgtype='C'
     6.1 if csid = 0 then cancelcode = 5 else cancelcode = 3
     6.2 update calltx set cxdtvtacancel  cxsuccess = cancelcode
     7. update drv set dvstatus = 1 where dvid = car_id


   }
   if (carList_id = null) or (car_id = '') or (cxno = '') then begin
      Result := 'gf_PaicheCancel �ǤJ�Ȥ��i���ŭ�';
      exit;
   end;

   aCDS1 := TFDQuery.Create(NIL);
   try
      // 1.
      asql1 := 'select dvno, dvstatus, dvdtreport' +
          ',case when dvdtreport >= (Now()-500) then ''Y'' else ''N'' end isOnline '
          + ' from drv ' + ' where mcid=' + IntToStr(carList_id) +
          ' and dvid=' + car_id;
      aCDS1.Connection := FDConnection;
      aCDS1.SQL.Clear;
      aCDS1.SQL.Add(asql1);
      aCDS1.Open();

      dvno := aCDS1.FieldByName('dvno').AsString;
      if aCDS1.FieldByName('isOnline').AsString = 'N' then begin
         Result := '������' + cxno + '�A�q���x��' + aCDS1.FieldByName('dvno').AsString +
             '���W�u�A�L�k�����C';
         aCDS1.Close;
         exit;
      end;
      // 2.
      aCDS1.Close;
      asql2 := 'select dvid, cxdtpickup, csid from calltx WHERE cxno = ' + cxno
          + ' and dvid =' + car_id;
      aCDS1.SQL.Clear;
      aCDS1.SQL.Add(asql2);
      aCDS1.Open();

      // 3.
      if aCDS1.IsEmpty then begin
         Result := '�u' + cxno + '�v�d�L�������渹';
         aCDS1.Close;
         exit;
      end;
      // 4.
      if not aCDS1.FieldByName('cxdtpickup').IsNull then begin
         Result := '�u' + cxno + '�v�q���w���쭼��';
         aCDS1.Close;
         exit;
      end;

      // 5.
      msgtxt := 'C;';
      if InsertMsgouttx(StrToInt(car_id), msgtxt) = False then begin
         Result := '�u' + cxno + '�v�o�X���������q��MSGOUTTX �g�J���ѡA�q���x��' + dvno;
         gp_ShowMsg(carList_id, Result);
      end
      else begin
         // 6. 7.
         if aCDS1.FieldByName('csid').AsString = '0' then
            cancelcode := 5
         else
            cancelcode := 3;
         asql7 := 'update calltx set cxcanceldvno=''' + dvno +
             ''' ,cxdtvtacancel=now(),cxsuccess =' + IntToStr(cancelcode) +
             ' where  cxno = ' + cxno;
         err := gf_ExecuteSQL(carList_id, asql7);
         Result := err;
         if err <> '' then
            gp_ShowMsg(carList_id, '�u' + cxno + '�v�o�X���������q�����ѡA�q���x��' + dvno +
                '  ' + err);
         asql7 := 'update drv set dvstatus = 1 where  dvid = ' + car_id;
         err := gf_ExecuteSQL(carList_id, asql7);
         Result := err;
         if err <> '' then
            gp_ShowMsg(carList_id, '�u' + cxno + '�v�o�X���������q�����ѡA�q���x��' + dvno +
                '  ' + err)
         else
            gp_ShowMsg(carList_id, '�u' + cxno + '�v�o�X���������q���A�q���x��' + dvno);

      end;
      aCDS1.Close;

   finally
      if Assigned(aCDS1) then
         aCDS1.Free;
   end;

end;

Function TPSO.InsertMsgouttx(dvid: Integer; pmotxt: string): Boolean;
var
   res: String;
begin
   Result := False;

   try
      res := gf_ExecuteSQL(dvid,
          'insert into msgouttx(modtcreate,dvid,motxt,miseqid)' +
          'values(now(),' + IntToStr(dvid) + ',' + quotedstr(pmotxt) + ', 0)');
      Result := (res = '');

   except
      Result := False;
   end;

end;

function TPSO.gf_NotifyMsg(pmcid, pdvid: Integer; pdvno, msg: String): String;
var
   asql1, msgtxt: String;
   aerrmsg: string;
begin
   if (pdvid = null) then begin
      Result := 'gf_NotifyMsg  �ǤJ�Ȥ��i���ŭ�';
      exit;
   end;
   msgtxt := 'M' + msg + Chr(10);

   if InsertMsgouttx(pdvid, HttpEncode(UTF8Encode(msgtxt))) = False then begin
      Result := 'MSGOUTTX �g�J����';
      aerrmsg := '�o�G�T���u' + msg + '�vMSGOUTTX �g�J���ѡA�q���s��' + IntToStr(pdvid);
      gp_ShowMsg(pmcid, aerrmsg);
   end
   else begin
      gp_ShowMsg(pmcid, '�o�G�T���u' + msg + '�v�A�q���x��' + pdvno);

   end;
   Result := '';

end;

function TPSO.gf_RestorePaiBan(carList_id: Integer;
    car_id, qupid: String): String;
var
   dvno, msgtxt: string;
   asql1, asql4, err: String;
   aCDS1: TFDQuery;
begin

   { done

     1. select dvstatus from drv where dvid = car_id and  now() - dvdtreport < 500
     2. if not found then the driver is lost
     3. if dvstatus <> 1 then showerror

     4. update queuetx set qxstatus = 0, qxdtout = null where mcid = carlist_id and qupid = qupid and dvid = car_id
     order qxdtin desc limit 1
   }
   if (carList_id = null) or (car_id = '') or (qupid = '') then begin
      Result := 'gf_RestorePaiBan �ǤJ�Ȥ��i���ŭ�';
      exit;
   end;

   Result := '';
   aCDS1 := TFDQuery.Create(NIL);
   try
      // 1.
      asql1 := 'select dvno, dvstatus, dvdtreport' +
          ',case when dvdtreport >= (now()-500) then ''Y'' else ''N'' end isOnline '
          + ' from drv  ' + ' where mcid=' + IntToStr(carList_id) +
          ' and dvid=' + car_id;
      aCDS1.Connection := FDConnection;
      aCDS1.SQL.Clear;
      aCDS1.SQL.Add(asql1);
      aCDS1.Open();
      dvno := aCDS1.FieldByName('dvno').AsString;
      // 2.
      if aCDS1.FieldByName('isOnline').AsString = 'N' then begin
         Result := '�x��' + dvno + ' + �������G' + IntToStr(carList_id) + '�Aqupid�G' +
             qupid + ' ���W�u�I';
         aCDS1.Close;
         exit;
      end;
      // 3.
      if aCDS1.FieldByName('dvstatus').AsInteger <> 1 then begin
         Result := '�x��' + dvno + ' + �������G' + IntToStr(carList_id) + ' �D�Ũ��I';
         aCDS1.Close;
         exit;
      end;
      // 4.

      msgtxt := 'B' + qupid + ',';
      if InsertMsgouttx(StrToInt(car_id), msgtxt) = False then begin
         Result := '�o�X��_�ƯZ�q��MSGOUTTX �g�J���ѡA�q���x��' + dvno;
         gp_ShowMsg(carList_id, Result);
      end
      else begin

         asql4 := 'update queuetx set qxstatus = 0, qxdtout = null ' +
             ' where mcid=' + IntToStr(carList_id) + ' and dvid=' + car_id +
             ' and qupid=' + qupid + '  order by qxdtin desc limit 1 ';
         err := gf_ExecuteSQL(carList_id, asql4);
         Result := err;
         if err = '' then
            gp_ShowMsg(carList_id, '�o�X��_�ƯZ�q���A�q���x��' + dvno);

      end;
      aCDS1.Close;

   finally
      if Assigned(aCDS1) then
         aCDS1.Free;
   end;

end;

function TPSO.gf_Get0Value(carList_id: Integer; as_SQL: String): Variant;
var
   aFDQuery: TFDQuery;
begin
   Result := null;
   aFDQuery := TFDQuery.Create(nil);
   try
      with aFDQuery Do begin

         Connection := FDConnection;
         Close;
         SQL.Clear;
         SQL.Add(as_SQL);
         Open;
         Result := Fields[0].Value;
         Close;
      end;
   finally
      if Assigned(aFDQuery) then
         aFDQuery.Free;

   end;
end;

function TPSO.gf_Get1Record(carList_id: Integer; as_SQL: String): TrecVarArray;
var
   li_i: Integer;
   aFDQuery: TFDQuery;
begin

   aFDQuery := TFDQuery.Create(nil);
   with aFDQuery Do begin
      try
         Connection := FDConnection;
         Close;
         SQL.Clear;
         SQL.Add(as_SQL);
         try
            Open;
            SetLength(Result, FieldCount);
            for li_i := 0 to FieldCount - 1 do
               Result[li_i] := Fields[li_i].Value;
         except
            on E: Exception do
               gp_ShowMsg(carList_id, '�X�{���~�G' + E.Message + '�A' + SQL.Text);
         end;

      finally
         Active := False;
         Free;
      end;
   end;
end;

procedure TPSO.gp_CheckCarLive(carList_id: Integer; qupid, cxno: string);
var
   aSql: String;
begin

   // �p�G�q���O���h�T���A�ӥB �q���̫�^���ɶ� �W�L 5 �����A�а� �t�ΰ��Z jieshu 2014.11.10

   { Done : check drv car live�Fselect dvdtreport from drv where dvid = Xdvid }
   // aCDSdrv := TClientDataSet.Create(Self);
   try

      aSql := 'UPDATE queuetx SET qxdtout=now(), qxstatus = ' +
          IntToStr(Ord(qsOutSys)) + ' WHERE mcid =' + IntToStr(carList_id) +
          ' and qupid = ' + qupid + ' and qxstatus = 0' + ' and dvid  in (' +
          'select dvid from drv ' + ' where mcid=' + IntToStr(carList_id) +
          ' and dvdtreport < (now()-500) )';
      gf_ExecuteSQL(carList_id, aSql);
      gp_ShowMsg(carList_id, '�]' + cxno + '�^' + '����:' + IntToStr(carList_id) +
          '�ƯZ�I:' + qupid + ' �t�ΰ��Z');

   except
      on E: Exception do
         gp_ShowMsg(carList_id, '�]�ˬd�q���^�X�{���~�G' + E.Message);
   end;

end;

// update Car data by ID
// lnglattype : ����y�СA�۹�y��
function TPSO.updateCarByID(carList_id: Integer; carID, dvno: string;
    carLng, carLat: double; lnglattype: string; SocketIP: string;
    drvstatus: Integer; var resultError: string;
    dvName, as_Term: String): string;
var
   // Acar: carLocationType;
   i: Integer;
   findCar: Boolean;
   gtime: Tdatetime;
   ls_QuIdx: String;
   ls_Term: TArray<String>;
   mcxytolerance: Real;
   aSql: String;
begin
   Result := ''; // �L����
   // DONE    if carlng = 0 and carlat = 0 then update dvstatus, dvdtreport=now()
   // else update drv set dvstatus, dvdtreport = Now(), dvlong, dvlat where dvid
   if IntToStr(drvstatus) = '' then
      aSql := 'update drv set dvdtreport=now(), '
   else
      aSql := 'update drv set dvdtreport=now(),dvstatus=' + IntToStr(drvstatus);

   if (carLng = 0) and (carLat = 0) then

      aSql := aSql + ' where dvid=' + carID
   else
      aSql := aSql + ',dvlong=' + FloatToStr(carLng) + ',dvlat=' +
          FloatToStr(carLat) + '  where dvid=' + carID;
   gf_ExecuteSQL(carList_id, aSql);

end;

function TPSO.search_qterritory(mcid: Integer; long, lat: double;
    var theQueueName: String): String;
var
   Zquery, lt_Detail: TFDQuery;
   T: array of TPoint;
   P: TPoint;
   i: Integer;

   function PointInPolygon(Point: TPoint;
       const Polygon: array of TPoint): Boolean;
   var
      rgn: HRGN;
   begin
      rgn := CreatePolygonRgn(Polygon[0], Length(Polygon), WINDING);
      Result := PtInRegion(rgn, Point.X, Point.Y);
      DeleteObject(rgn);
   end;

begin
   Zquery := TFDQuery.Create(nil);
   lt_Detail := TFDQuery.Create(nil);
   P := TPoint.Create(Round(long * 1000000), Round(lat * 1000000));
   // long �O X,lat �O Y
   try
      try
         Zquery.Connection := FDConnection;
         lt_Detail.Connection := FDConnection;
         Zquery.SQL.Clear;
         Zquery.SQL.Add('select pid, quid,qunm from queue where mcid = ' +
             IntToStr(mcid));
         Zquery.Open(); // * Ū�X�Ө����Ҧ����ƯZ�I */

         while not Zquery.Eof do begin
            // if Self.showDetailMessage.Checked then
            // gp_ShowMsg(mcid, '�ˬd����(' + FloatToStr(long) + ',' + FloatToStr(lat) +
            // ') �O�_�b�ƯZ�I ' + Zquery.FieldByName('quid').AsString + ' ����a��');
            // *** �Ө��� Xmcid ���ƯZ�� Xquid���ϰ� �V �h��ΩҦ����I���g�n�� */
            lt_Detail.Close;
            lt_Detail.SQL.Clear;
            lt_Detail.SQL.Add
                ('select Round(qtlong1*1000000,0) qtlong, Round(qtlat1*1000000,0) qtlat from qterritory'
                + ' where mcid = ' + IntToStr(mcid) + ' and quid = ' +
                Zquery.FieldByName('quid').AsString + ' order by seqid');
            lt_Detail.Open();
            // * T[i] �u�����11��ƪ���ƶ� long �M lat�A���O�N���ӳ��I���g�n�� */
            // if Self.showDetailMessage.Checked then
            // gp_ShowMsg(mcid, '�ƯZ�I ' + Zquery.FieldByName('quid').AsString +
            // ' ��a�����I�ơG' + IntToStr(lt_Detail.RecordCount));

            if lt_Detail.RecordCount < 3 then // * �L�Ī��h��� */
            begin
               Zquery.Next;
               Continue;
            end;

            SetLength(T, lt_Detail.RecordCount);
            i := 0;

            while not lt_Detail.Eof do begin // long �O X,lat �O Y
               T[i] := TPoint.Create(lt_Detail.FieldByName('qtlong').AsInteger,
                   lt_Detail.FieldByName('qtlat').AsInteger);

               Inc(i);
               lt_Detail.Next;
            end;
            theQueueName := '';
            if PointInPolygon(P, T) then begin
               theQueueName := Zquery.FieldByName('qunm').AsString;
               Result := Zquery.FieldByName('pid').AsString;
               exit;
            end;

            Zquery.Next;
         end;

         gp_ShowMsg(mcid, '����(' + FloatToStr(long) + ',' + FloatToStr(lat) +
             ') ���b���󪺱ƯZ�I����a');
         Result := '';
      except
         on E: Exception do
            gp_ShowMsg(mcid, 'search_qterritory�X�{���~�G' + E.Message);
      end;
      Zquery.Close;
      lt_Detail.Close;
   finally
      if Assigned(Zquery) then
         Zquery.Free;
      if Assigned(lt_Detail) then
         lt_Detail.Free;

   end;
end;

function TPSO.getStrBycc(var Str: string; cc: string): string;
var
   P: Integer;
begin
   P := Pos(cc, Str);
   Result := Copy(Str, 1, P - 1);
   Str := Copy(Str, P + 1, Length(Str) - P);
end;

function TPSO.pickTaxiByfind(carList_id: Integer; carID: string;
    cxno, cxname, cxtel, cxfraddr, cxtoaddr, cxgotime, cxgodis,
    cxfrlocref: string): string;
begin
   WritePmsg(StrToInt(carID), cxno, cxname, cxtel, cxfraddr, cxtoaddr, cxgotime,
       cxgodis, cxfrlocref, 0);
   Result := carID;
end;

function TPSO.WritePmsg(dvid: Integer; cxno, cxname, cxtel, cxfraddr, cxtoaddr,
    cxgotime, cxgodis, cxfrlocref: string; ai_BidRun: byte): Boolean;
var
   msgtxt, ls_QuIdx: String;
   aCDScalltx, aCDSdrv: TFDQuery;
begin
   msgtxt := '';

   { Done  select * from calltx where cxno =Acar^.cxno }
   if dvid = null then begin
      gp_ShowMsg(0, '�]' + cxno + '�^�o�X���ȥ��ȸ߰�MSGOUTTX �g�J���ѡAdvid �ŭ�');
      Result := False;
      exit;
   end;


   // long �O X,lat �O Y

   aCDSdrv := TFDQuery.Create(nil);
   aCDScalltx := TFDQuery.Create(nil);
   try
      aCDSdrv.Connection := FDConnection;
      aCDScalltx.Connection := FDConnection;
      aCDSdrv.SQL.Clear;
      aCDSdrv.SQL.Add('select * from DRV where dvid=' + IntToStr(dvid));
      aCDSdrv.Open();
      aCDScalltx.SQL.Clear;
      aCDScalltx.SQL.Add(' select * from calltx where cxno =' + cxno);
      aCDScalltx.Open();

      // if Acar^.sendAsk then // �n���q�������D
      if not aCDScalltx.IsEmpty then begin

         cxfrlocref := StringReplace(cxfrlocref, '%E6%96%B0%E8%BB%8A', '', []);
         cxfrlocref := StringReplace(cxfrlocref, '%E9%9D%9E%E5%B8%B8%E6%96%B0', '', []);
         With aCDScalltx Do begin

            msgtxt := 'P' + FloatToStr
                (Trunc(FieldByName('cxfrlong').AsFloat * 100000) / 100000) + '-'
                + FloatToStr(Trunc(FieldByName('cxfrlat').AsFloat * 100000) /
                100000) + ',' +
                FloatToStr(Trunc(FieldByName('cxtolong').AsFloat * 100000) /
                100000) + '-' +
                FloatToStr(Trunc(FieldByName('cxtolat').AsFloat * 100000) /
                100000) + ',' + cxname + ',' + cxtel + ',' + ' ' + ',' +
                '' + cxfrlocref // �q������
                + ',' + cxfraddr + ',' +
                HttpEncode(UTF8Encode(FieldByName('cxtolocref').AsString)) + ','
                + cxtoaddr + ',' + FieldByName('cxno').AsString + ',' + cxgotime
                + ',' + cxgodis + ';';

            if FieldByName('cxflagON').AsInteger = 1 then
               msgtxt := msgtxt + 'Y;'
            else
               msgtxt := msgtxt + 'N;';
            msgtxt := msgtxt + IntToStr(ai_BidRun) + ';';
            // �ĴX�^�X jieshu 2015.5.2

            if InsertMsgouttx(dvid, msgtxt) = False then
               gp_ShowMsg(aCDSdrv.FieldByName('mcid').AsInteger,
                   '�]' + cxno + '�^�o�X���ȥ��ȸ߰�MSGOUTTX �g�J���ѡA�q���x��' +
                   aCDSdrv.FieldByName('dvno').AsString);
         end;
      end;

      aCDScalltx.Close;
      aCDSdrv.Close;
      Result := true;
   finally
      if Assigned(aCDScalltx) Then
         aCDScalltx.Free;
      if Assigned(aCDSdrv) Then
         aCDSdrv.Free;

   end;

end;

// ���ݥq���^���A���^���ɡA�����r��|�Ǧ^CSR
function TPSO.WaitTaxiAns(carList_id: Integer;
    carID, qupid, cxno, dvno, queuetx_pid: string): string;
var
   // Acar: carLocationType;
   // wtime: integer;
   findCar: Boolean;
   i, j, timecount: Integer;
   rgdis: Real; // , rgtime_mm
   lv_Value: Variant;
   lv_Value2: TrecVarArray;
   ls_sql: String;
   WaitCarAnseerTimeOut: Integer;
begin
   WaitCarAnseerTimeOut := 15; // default
   Result := '';
   findCar := False;
   // 3. mcqxtimeout �ƯZ�����ɡA�^�����檺timeout ����
   if cds_mtrcd_.Locate('mcid', carList_id, []) then
      WaitCarAnseerTimeOut := cds_mtrcd_.FieldByName('mcqxtimeout').AsInteger;

   gp_ShowMsg(carList_id, '�����G' + IntToStr(carList_id) + 'mcqxtimeout:' +
       IntToStr(WaitCarAnseerTimeOut));

   for j := 1 to Round((WaitCarAnseerTimeOut + 5) / 3) + 1 do begin
      if CancelPaich = true Then begin
         ProcessData('B,' + '���_����' + #10);
         Break;
      end;
      for i := 1 to 12 do // �� 3 ��
      begin
         Sleep(250);
         // ProcessMessages;
      end;
      gp_ShowMsg(carList_id, '�]' + cxno + '�^' + '�ƯZ�������ݦ���:' + IntToStr(j));

      ls_sql := 'select qxdtdrvanswer from queuetx where pid =' + queuetx_pid;

      lv_Value := gf_Get0Value(carList_id, ls_sql);

      if not varisnull(lv_Value) then begin
         lv_Value2 := gf_Get1Record(carList_id,
             'update calltx set cxqxdvno = '''' where cxno = ' + cxno + ' and dvno is NULL;'+
             'select dvno, cxtimetopickup from calltx where cxno = ' + cxno);

         if varisnull(lv_Value2[0]) then begin
            gp_ShowMsg(carList_id, '�q�� ' + dvno + ' ���');
            exit; // �~��U�ӥq��
         end
         else begin
            gp_ShowMsg(carList_id, '�q�� ' + dvno + ' ����');
            // ����q�� ticket �� ����

            Result := 'P1,' + IntToStr(carList_id) + ',' + carID + ',' + '' +
                ',' + '' + ',' + FormatFloat('0.0', 0) + ',' +
                FloatToStr(lv_Value2[1]) + ';';

            exit;
         end;
      end;
   end;

end;

function TPSO.updateAns(carList_id: Integer; car_id: string; Carans: byte;
    cxno: string): Boolean;
var
   // Acar: carLocationType;
   i: Integer;
   lng, lat: double;
   // mcid: integer;
   // W-140425	3. VTA �b�n��ƯZ�����Q���\���Acalltx.quid �ʅH�ȡCjieshu 14/4/27
   dvid, quid: string;
   gtime: Tdatetime;
   lv_Value: TrecVarArray;
begin
   lng := 0;
   lat := 0;
   dvid := '';
   quid := '';
   gtime := Now;
   Result := False;

   // �u���q������H�ɤ~�⬣�����\
   // if result then // 1�Ũ�  //2�e�h����  //3���Ȥ��A�B��    //4��
   // begin

   if car_id = '' then
      exit;

   lv_Value := gf_Get1Record(carList_id,
       'select dvlong,dvlat from drv where dvid = ' + car_id);

   if not varisnull(lv_Value[0]) then begin
      lng := lv_Value[0];
   end;

   if not varisnull(lv_Value[1]) then begin
      lat := lv_Value[1];
   end;

   if Carans = 4 then
      // W-140429 13. �q��������̇P�AVTA�N�nupdate  cxdtconfirm
      // DataModule1.UpdateCALLTXonlnglat(cxno, dvid, lng, lat, Carans, quid) //4�G�]����ȤH�^�e�Ȥ�
      // W-140429 13. �q�󱵨�ȤH�n�AVTA �nupdate cxdtpickup
      UpdateCallTxPickup(cxno, dvid, lng, lat, Carans)
      // , quid) //4�G�]����ȤH�^�e�Ȥ�
      // CSR 2.3 Spec 14 06 14 jieshu 14/6/15 VTA ���� �q���^�� ���������ȡ� ���ɶ�
   else if Carans = 5 then // ��������
      gf_ExecuteSQL(carList_id,
          'Update CallTx set cxsuccess = 9, cxdtcsrclick = null, cxdtpgnotfound = NOW() where cxno = '
          + cxno)
   else if Carans = 6 then
      UpdateCALLTXoflnglat(cxno, lng, lat, Carans) // 6�G��ت�
   else if Carans = 8 then // �w��
      gf_ExecuteSQL(carList_id,
          'Update CallTx set cxdtinplace = NOW(), cxinlong = ' + FloatToStr(lng)
          + ', cxinlat = ' + FloatToStr(lat) + ' where cxno = ' + cxno);

   Result := true;
end;

function TPSO.assignMission2Car(dvid: Integer; cxno, cxname, cxtel, cxfraddr,
    cxtoaddr, cxgotime, cxgodis, cxfrlocref: string; ai_BidRun: byte): string;
begin
   WritePmsg(dvid, cxno, cxname, cxtel, cxfraddr, cxtoaddr, cxgotime, cxgodis,
       cxfrlocref, ai_BidRun);
end;

function TPSO.ConvertDegreeToRadians(const Degrees: double): double;
begin
   Result := (PI / 180) * Degrees;
end;

function TPSO.DistanceBetweenPoint2(const StartLat, StartLong, EndLat,
    EndLong: double): double;
var
   Lat1r, Lat2r, Long1r, Long2r: double;
   D: double;
const
   R: double = 6378.137; // Earth's radius (km)
begin
   Lat1r := ConvertDegreeToRadians(StartLat);
   Lat2r := ConvertDegreeToRadians(EndLat);
   Long1r := ConvertDegreeToRadians(StartLong);
   Long2r := ConvertDegreeToRadians(EndLong);

   D := Math.ArcCos(Sin(Lat1r) * Sin(Lat2r) + Cos(Lat1r) * Cos(Lat2r) *
       Cos(Long2r - Long1r)) * R;
   Result := D;
end;

// ��X�̪񪺱ƯZ�I
function TPSO.GetQueueID(carList_id: Integer; cxfrlng, cxfrlat: double;
    var theQueueName: string; var theQueueCount: Integer): string;
var
   i: Integer;
   nowdis: double;
   asql1: String;
   aCDS1: TFDQuery;
begin
   Result := '';
   nowdis := 10000000;

   if carList_id = 0 then
      exit;

   { Done
     1.
     select pid, qunm from queue
     where mcid = Xmcid
     and ((quopen = 0) or ((quopen = 1) and (H >= 6) and(H < 23))
     or ((quopen = 2) and ((H >= 23) or (H < 6))))
     order by (qulong - Xlong)*(qulong - Xlong) + (qulat - Xlat) * (qulat - Xlat)  limit 1
     2.
     if not found then show error message; exit;

     result := pid;
     theQueueName := qunm;
     theQueueCount := gf_Get0Value(carList_ID,
     'select count(*) from queuetx' + ' where mcid = ' +
     IntToStr(carList_ID) + ' and quid = ' + ApaiBan^.quid +
     ' and qxdtout is null'); // ApaiBan^.nowNum;
   }
   // if showDetailMessage.Checked then
   // gp_ShowMsg(carList_ID, '����-lat�G' + FloatToStr(cxfrlat) + '�Alng�G' +
   // FloatToStr(cxfrlng));

   aCDS1 := TFDQuery.Create(NIL);
   try
      // 1.
      asql1 := 'select pid,quid, qunm,qulong,qulat from queue ' +
          ' where mcid =' + IntToStr(carList_id) +
          ' and ((quopen = 0) or ((quopen = 1) and (hour(now()) >= 6) and (hour(now()) < 23)) '
          + ' or ((quopen = 2) and ((hour(now()) >= 23) or (hour(now()) < 6)))) '
          + ' order by (qulong -' + FloatToStr(cxfrlng) + ')' + ' *(qulong - ' +
          FloatToStr(cxfrlng) + ')' + ' +(qulat -' + FloatToStr(cxfrlat) + ')' +
          ' * (qulat - ' + FloatToStr(cxfrlat) + ')' + '   limit 1 ';

      aCDS1.Connection := FDConnection;
      aCDS1.SQL.Clear;
      aCDS1.SQL.Add(asql1);
      aCDS1.Open(); // * Ū�X�Ө����Ҧ����ƯZ�I */

      // 2.
      if aCDS1.IsEmpty then begin
         aCDS1.Close;
         exit;
      end;

      nowdis := DistanceBetweenPoint2(aCDS1.FieldByName('qulat').AsFloat,
          aCDS1.FieldByName('qulong').AsFloat, cxfrlat, cxfrlng);

      // �P���Ȫ��Z�� �j�� mcqueuemaxdist ���W���� �ƯZ�����C jieshu 14.8.16
      cds_mtrcd_.Locate('mcid', carList_id, []);
      i := cds_mtrcd_.FieldByName('mcqueuemaxdist').AsInteger;

      if nowdis * 1000 > i then begin
         Result := '-1';
         theQueueName := '�W�X�ƯZ�I ' + FormatFloat('0.0', i / 1000) + ' ���� �d��';
         theQueueCount := -1;
         aCDS1.Close;
         exit;
      end;

      Result := aCDS1.FieldByName('pid').AsString;
      theQueueName := aCDS1.FieldByName('qunm').AsString;;
      theQueueCount := gf_Get0Value(carList_id, 'select count(*) from queuetx' +
          ' where mcid = ' + IntToStr(carList_id) + ' and quid = ' +
          aCDS1.FieldByName('quid').AsString + ' and qxdtout is null');
      aCDS1.Close;

   finally
      if Assigned(aCDS1) then
         aCDS1.Free;
   end;
end;

// ��5�x�̪�
function TPSO.goQiangBiaoCar(carList_id: Integer; cxfrlng, cxfrlat: double;
    cxtolng, cxtolat: double; cxname, cxtel, cxface, cxfrlocref, cxfraddr,
    cxtolocref, cxtoaddr, cxno, cxgotime, cxgodis: string;
    var dvnmList, ls_Term: String; ai_BidQty, ai_BidRun: byte): Integer;
var
   i, j, k: Integer;
   CarCDS: TFDQuery;
   la_Term: TArray<String>;
   li_i, carcnt: Integer;
   ls_V: String;
   isCarCondition: Boolean;

   procedure gf_FindNCarRec;
   var
      aSql: String;
      mcbidrun: Integer;
   begin
      {
        set dvstatus = 99, dvdtnego = Now(), dvcxno = Xcxno
        where mcid = Xmcid
        and (dvstatus = 1 or ( dvstatus = 99 and (Now() - dvdtnego) > 300))
        and (Now() - dvdtreport) <= 500 /* �ư����h�pô */
        �q������
        order by (dvlong - Xlong)*(dvlong - Xlong) + (dvlat - Xlat) * (dvlat - Xlat)
        limit Xmcbidrun1drv �� Xmcbidrun2drv
      }

      cds_mtrcd_.Locate('mcid', carList_id, []);
      if ai_BidRun = 1 then
         mcbidrun := cds_mtrcd_.FieldByName('mcbidrun1drv').AsInteger
      else
         mcbidrun := cds_mtrcd_.FieldByName('mcbidrun2drv').AsInteger;

      aSql := 'update drv ' +
          'set  dvflagnego = ''Y'', dvdtnego = Now(), dvcxno = ' + cxno +
          ' where mcid=' + IntToStr(carList_id) +
          ' and (dvstatus = 1  and (dvflagnego = ''N''  or ( dvflagnego = ''Y'' and (Now() - dvdtnego) > 300))) '
          + ' and dvdtreport >= (Now()-500) ' // �ư����h�pô
          + ' and dvno not in (select bsdvno from blacksheep ' + 'where mcid = '
      // �¦W��
          + IntToStr(carList_id) + ' and bspgphone = ' + quotedstr(cxtel) + ')'
          + ' order by (dvlong - ' + FloatToStr(cxfrlng) + ')*(dvlong - ' +
          FloatToStr(cxfrlng) + ') + (dvlat - ' + FloatToStr(cxfrlat) +
          ') * (dvlat - ' + FloatToStr(cxfrlat) + ') ' + ' limit ' +
          IntToStr(mcbidrun);

      if gf_ExecuteSQL(carList_id, aSql) = '' then

      begin
         aSql := 'select * from drv ' + ' where mcid=' + IntToStr(carList_id) +
             ' and dvcxno = ' + cxno + ' and dvdtreport >= (Now()-500) ' +
             ' limit ' + IntToStr(mcbidrun);
      end
      else begin
         aSql := 'select * from drv  limit 0';
      end;
      CarCDS.Connection := FDConnection;
      CarCDS.SQL.Clear;
      CarCDS.SQL.Add(aSql);
      CarCDS.Open;

   end;

begin

   if (carList_id = null) or (cxno = '') then begin
      gp_ShowMsg(carList_id, 'goQiangBiaoCar cardList_id or cxno  �ǤJ�Ȥ��i���ŭ�');
      Result := 0;
      exit;
   end;

   CarCDS := TFDQuery.Create(nil);
   try

      // -------------��X�̪� N �x��
      { Done
        update drv
        set dvstatus = 99, dvdtnego = Now(), dvcxno = Xcxno
        where mcid = Xmcid
        and (dvstatus = 1 or ( dvstatus = 99 and (Now() - dvdtnego) > 300))
        and (Now() - dvdtreport) <= 500 /* �ư����h�pô */
        and dvno ���b �¦W��
        �q������
        order by (dvlong - Xlong)*(dvlong - Xlong) + (dvlat - Xlat) * (dvlat - Xlat)
        limit Xmcbidrun1drv �� Xmcbidrun2drv
      }
      gf_FindNCarRec; // open  CarCDS

      // �o�X�����T�����q��   .....
      { Done
        �o�X�ܼСG
        for i in (select dvid from drv where dvcxno = Xcxno) �o�X�ܼ�
        // assignMission2Car(Acar, cxfrlng, cxfrlat, cxtolng, cxtolat,
        //  cxname, cxtel, cxface, cxfrlocref, cxfraddr, cxtolocref,
        //  cxtoaddr, cxno, cxgotime, cxgodis, as_Term, ai_BidRun);
        assignMission2Car(dvid, cxno, ai_BidRun);
      }

      dvnmList := '';
      la_Term := ls_Term.Split([',']);
      carcnt := 0;
      while Not CarCDS.Eof do begin
         isCarCondition := true; // ���w�]�ŦX����
         for li_i := 0 to High(la_Term) do begin
            if la_Term[li_i] = 'Y' then // �n�P�_ �s���B�L�ϡBRV���B�����ȡB���d��
            begin
               if la_Term[li_i] <> CarCDS.FieldByName(ls_Field[li_i]).AsString
               then begin // ���ű���
                  {
                    if showDetailMessage.Checked then
                    begin
                    for j := 0 to High(ls_Field) do
                    ls_V := ls_V + ls_Field[j] + ',';
                    gp_ShowMsg(CarCDS.FieldByName('mcid').AsInteger,
                    '�q�����š]�s���B�L�ϡBRV���B�����ȡB���d���B�j�d���B�ɹq�B����B�D�`�s�B�L�b�}�^����G' + ls_Term +
                    '�F' + ls_V);
                    end;
                  }
                  isCarCondition := False;
                  Break;
               end;
            end;
         end;

         if (isCarCondition = true) and (CancelPaich = False) Then begin
            Inc(carcnt);
            assignMission2Car(CarCDS.FieldByName('dvid').AsInteger, cxno,
                cxname, cxtel, cxfraddr, cxtoaddr, cxgotime, cxgodis,
                cxfrlocref, ai_BidRun);
            gf_ExecuteSQL(carList_id,
                'insert into bid(cxno, mcid, dvno, bdlong, bdlat, bdrunno) values('
                + cxno + ',' + IntToStr(carList_id) + ',' +
                quotedstr(CarCDS.FieldByName('dvno').AsString) + ',' +
                FloatToStr(CarCDS.FieldByName('dvlong').AsFloat) + ',' +
                FloatToStr(CarCDS.FieldByName('dvlat').AsFloat) + ',' +
                IntToStr(ai_BidRun) + ')');
            dvnmList := dvnmList + CarCDS.FieldByName('dvno').AsString + ', ';
         end
         else begin
            gf_ExecuteSQL(carList_id,
                'update drv set dvflagnego=''N'' where dvid=' +
                CarCDS.FieldByName('dvid').AsString);
         end;

         CarCDS.Next;
      end;
      Result := carcnt; // ��쪺������
      CarCDS.Close;

   finally
      if Assigned(CarCDS) Then
         CarCDS.Free;

   end;

end;

Function TPSO.AcarSitToDrvStatus(sit: Integer): Integer;
begin
   if ((sit = 0) or (sit = 1) or (sit = 3) or (sit = 5) or (sit = 6)) then begin
      Result := 1; // 1�Ũ�  //2�e�h����  //3���Ȥ��A�B��    //4��
   end
   else if (sit = 2) then begin
      Result := 2; // �e�h����
   end
   else if (sit = 4) then begin
      Result := 3; // ���Ȥ��A�B��
   end
   else begin
      Result := 0; // ��
   end;

end;

function TPSO.gf_ProcessPaiche(as_Msg: String): String;

var
   NowMissionName: string;

   i, j: Integer;
   temp, ls_sql: string;
   ld_UseCar: Tdatetime;
   Carans: byte;
   carList_id: Integer;
   paiche_id, paiche_password, car_password, newcar_password: string;
   car_id: string;
   cxfrlng, cxfrlat, cxtolng, cxtolat: double; // ���Ȯy��
   cxname, cxtel, cxface, cxfraddr, cxfrlocref, cxtoaddr, cxtolocref, cxgotime,
       cxgodis: string; // ���ȸ��
   cxno: string; // �����渹
   find5Car: Integer;

   operNO: string;
   tlng, tlat: double;
   rgdis: Real;

   blockSQL, quid, dvno, nextdvno, res, tmpquid: string; // �q���n�D�i�J���ƯZ�I-Temp
   // paiban5Arr: TDynamicbpaibanReocrdArray;

   haveCar: Boolean;
   theQueueID: string; // ��쪺�ƯZ�I
   theQueueName: string;
   GoQueueCarNO: string;
   theQueueCount: Integer;
   quittype, SocketIP, SocketType, ls_Term, addMin: string;
   li_NotifyQty, li_RecvQty: Integer;

   la_Term: TArray<String>;
   li_i: Integer;
   aFDQuery: TFDQuery;
   lb_OK: Boolean;
   ls_V, queuetx_strpid, asql1, tmpStr, sqlCmd: String;
   lv_Data: TrecVarArray;
   Get0Value_res: Variant;

   function lp_RunBidMode(ai_BidRun: byte; as_BidRun: String): string;
   var
      li_Qty: byte;
      j, i: Integer;
      aSql, err, runNo: String;
   begin
      // 5�x�m�мҦ�,�P�ɰe�X����

      if (ai_BidRun = null) or (CancelPaich = true) Then begin
         if CancelPaich then
            ProcessData('B,' + '���_����' + #10);
         exit;
      end;

      cds_mtrcd_.Locate('mcid', carList_id, []);

      if ai_BidRun = 1 then
         li_Qty := cds_mtrcd_.FieldByName('mcbidrun1drv').AsInteger
      else
         li_Qty := cds_mtrcd_.FieldByName('mcbidrun2drv').AsInteger;

      find5Car := goQiangBiaoCar(carList_id, cxfrlng, cxfrlat, cxtolng, cxtolat,
          cxname, cxtel, cxface, cxfrlocref, HttpEncode(UTF8Encode('�m�� ')) +
          cxfraddr, cxtolocref, cxtoaddr, cxno, cxgotime, cxgodis, temp,
          ls_Term, li_Qty, ai_BidRun); // �h�䤭�x��
      // bid[bidno].AskQty := find5Car;
      gp_ShowMsg(carList_id, '�]' + cxno + '�^����' + as_BidRun + '�^�X���m�мҦ��G�m�Ш��X�p: '
          + IntToStr(find5Car) + ' �x ' + temp);

      // 2016.3.15 update calltx, ��s cxstep, cxqxdvno
      if ai_BidRun = 1 then
         runNo := '1'
      else
         runNo := '2';
      sqlCmd := 'update calltx set cxstep=''B' + runNo +
          ''' where dvno is null and cxno = ' + cxno;
      gf_ExecuteSQL(0, sqlCmd);

      if (find5Car > 0) then begin
         temp := '��' + as_BidRun + '�^�X�m�Хq���G' + temp;
         // todo AThread.Connection.IOHandler.
         ProcessData('B,' + temp + #10);

         if cds_mtrcd_.Locate('mcid', carList_id, []) then
            li_RecvQty := cds_mtrcd_.FieldByName('mcbidtimeout').AsInteger
         else
            li_RecvQty := 20;

         Result := 'N';


         // ���ݥq���m�Ц^�� begin

         gp_ShowMsg(carList_id, '�����G' + IntToStr(carList_id) + 'mcbidtimeout:' +
             IntToStr(li_RecvQty));
         temp := '';
         for j := 1 to Round(li_RecvQty / 3) + 1 do begin

            for i := 1 to 12 do // �C����3��
            begin
               Sleep(250);
               if CancelPaich = true Then begin
                  ProcessData('B,' + '���_����' + #10);
                  Break;
               end;
               // Application.ProcessMessages;
            end;
            // gp_ShowMsg(carList_ID, '�]' + cxno + '�^' + '�m�Ц^�е��ݦ���:' + IntToStr(j));
            ProcessData('B,�C');

            if gf_Get0Value(carList_id, 'select count(*) from bid where cxno = '
                + cxno + ' and bdrunno = ' + IntToStr(ai_BidRun) +
                ' and bdaction is not null ') >= 1 then begin
               Get0Value_res := gf_Get0Value(carList_id,
                   'select dvno from calltx where cxno = ' + cxno);

               if VarisStr(Get0Value_res) then
                  temp := VarToStr(Get0Value_res)
               else
                  temp := '';

               if temp <> '' then
                  Break;
            end;
         end;

         // ���ݥq���m�Ц^�� end
         // �m�е����^�_dvflagnego   �q���ߧY�i�H���W�i�H���U�� case
         aSql := 'update drv a, bid b set a.dvflagnego = ''N'' ' +
             ' where b.cxno =' + cxno + ' and b.dvno = a.dvno ';
         err := gf_ExecuteSQL(carList_id, aSql);
         if err <> '' then
            gp_ShowMsg(carList_id, '�]' + cxno + '�^' + ' ��sdvflagnego����' + err);

         if temp = '' then begin
            Result := 'N';
            gp_ShowMsg(carList_id, '�]' + cxno + '�^�����ڵ�');
            exit;
         end
         else begin
            Result := 'Y';
            ProcessData('Y');
            // gp_ShowMsg(carList_ID, #10+'�]' + cxno + '�^�x���G' + temp + ' ����');
         end;

      end
      else begin
         gp_ShowMsg(carList_id, '�]' + cxno + '�^�L���i��');
         Result := 'N';
         exit;

      end;

   end; // procedure lp_RunBidMode

begin // main program for CSR-to-VTA
   StepFlag := 'PS�}�l';
   carList_id := 0;
   try
      Result := 'N';
      temp := as_Msg;
      SocketType := Copy(temp, 1, 1); // [1];
      temp := Copy(temp, 2, Length(temp) - 1);
      // SetLength(paiban5Arr, 5); // 5�ӱƯZ�I���Ȧs��

      if (SocketType = 'C') or (SocketType = 'c') then // �n�D����
      begin
         NowMissionName := '�n�D����';
         StepFlag := 'PS' + NowMissionName;
         // �ѫʥ]----------
         // 0,1,0,3,22.639620-2252,0.0-0.0,%E4%BD%95%E9%87%91%E7%87%9F,0972002378,5555,,120.302109,,,4427,3333,4444;
         carList_id := StrToIntDef(getStrBycc(temp, ','), 0);
         // if showDetailMessage.Checked then
         // gp_ShowMsg(carList_ID, 'Socket Data �n�D����>>> ' + temp);

         operNO := getStrBycc(temp, ','); // �ާ@���s�� �@��CSR���T�ӥx 1�A2�A3
         paiche_id := getStrBycc(temp, ','); // ������id
         paiche_password := getStrBycc(temp, ','); // �K�X
         cxfrlng := StrToFloat(getStrBycc(temp, '-')); // �X�o�a�y��
         cxfrlat := StrToFloat(getStrBycc(temp, ',')); //
         cxtolng := StrToFloat(getStrBycc(temp, '-')); // �ت��a�y��
         cxtolat := StrToFloat(getStrBycc(temp, ',')); //

         cxname := getStrBycc(temp, ',');
         cxtel := getStrBycc(temp, ',');
         cxface := getStrBycc(temp, ',');

         cxfrlocref := getStrBycc(temp, ','); // �q������ ��������

         cxfraddr := getStrBycc(temp, ',');
         cxfraddr := IdURI.TIdURI.URLDecode(cxfraddr, IndyTextEncoding_UTF8);

         cxtolocref := getStrBycc(temp, ',');
         cxtoaddr := getStrBycc(temp, ',');

         cxno := getStrBycc(temp, ','); // �����渹
         cxgotime := getStrBycc(temp, ','); // ���ȥX�o�a�ܥت��a���ɶ�
         cxgodis := getStrBycc(temp, ';'); // ���ȥX�o�a�ܥت��a���Z��

         tmpStr := HttpDecode(getStrBycc(temp, ','));
         theQueueName := tmpStr;
         // UTF8ToString
         // (HttpDecode(AnsiString(getStrBycc(temp, ','))));
         tmpStr := HttpDecode(getStrBycc(temp, ','));
         theQueueID := getStrBycc(temp, ';');
         ls_Term := getStrBycc(temp, ';'); // �s���B�L�ϡBRV���B�����ȡB���d��
         if CancelPaich = true Then begin
            ProcessData('B,' + '���_����' + #10);
            exit;
         end;
         // todo AThread.Connection.IOHandler
         ProcessData('E');

         if operNO = '999' then // �w������ jieshu 14/6/17
         begin
            StepFlag := 'PS�w������';
            ls_sql := 'select dcdtpgneed from defercall where cxno = ' + cxno;
            ld_UseCar := gf_Get0Value(carList_id, ls_sql);
            cxfrlocref := cxfrlocref +
                HttpEncode(UTF8Encode('***�w��' + FormatDateTime('hh�Inn��',
                ld_UseCar) + '�Ψ�'));
            gp_ShowMsg(carList_id,
                '  �Ȥ�Ψ��G' + FormatDateTime('yyyy/mm/dd hh:nn', ld_UseCar) +
                '�ASQL�G' + ls_sql);
         end;
         // 5. �p�G���� �� pgdatatodrv �O 0, �e���q���������T����̡A�Ч�m�W�M�q�ܲM���ťաC
         if gf_Get0Value(carList_id,
             'select b.pgdatatodrv from calltx a, psgr b' +
             ' where a.pgid = b.pgid and cxno = ' + cxno) = 0 then begin
            // jieshu 14.8.16
            cxname := '';
            cxtel := '';
         end;
         // ���a����
         gp_ShowMsg(carList_id, '�]' + cxno + '�^*****CSR �n�D����' + '  ������ID: ' +
             paiche_id + ' ���w�ƯZ�I(qupid): ' + theQueueName + '(' + theQueueID +
             ')' + '  ����: ' + IntToStr(carList_id) + '  ����:' +
             UTF8ToString(HttpDecode(AnsiString(cxname))) + '  �q��:' + cxtel +
             '  �X�o: ' + FloatToStr(cxfrlat) + ',' + FloatToStr(cxfrlng) +
             '  �ت�: ' + FloatToStr(cxtolat) + ',' + FloatToStr(cxtolng));

         // -----------------�}�l�䨮---------------------
         begin
            // 1:�ƯZ�I���� begin

            if theQueueID = '-1' then begin
               // �����m��
               haveCar := False;
            end
            else begin
               // �h��ƯZ�I
               StepFlag := 'PS�ƯZ�Ҧ�';
               if theQueueName = '' then begin
                  theQueueID := search_qterritory(carList_id, cxfrlng, cxfrlat,
                      theQueueName);

                  if theQueueID = '' then begin
                     theQueueID := GetQueueID(carList_id, cxfrlng, cxfrlat,
                         theQueueName, theQueueCount); // ��X�̪񪺱ƯZ�I
                     temp := '�y�̪�z';
                  end
                  else
                     temp := '�y�d���ϡz';
               end
               else begin
                  temp := '�y���w�z';
               end;
               // todo AThread.Connection.IOHandler.
               ProcessData('A,' + temp + '�ƯZ�I�G' + theQueueName + #10);
               haveCar := False; // �T�{�O�_���ƯZ�I����������

               // W-140429 9. �ƯZ�����Q �u�X�bxmcid/xquid ��dvid list
               aFDQuery := TFDQuery.Create(nil);
               with aFDQuery Do begin
                  try
                     Connection := FDConnection;
                     try
                        if theQueueID = '' then
                           theQueueID := '-99';

                        gp_CheckCarLive(carList_id, theQueueID, cxno);
                        SQL.Clear;
                        SQL.Add('Select a.mcid, a.dvid, dvflagNew, dvflagNoSmoke, dvflagRV,'
                            + '       dvflagWheelChair, dvflagPet, dvflagbigpet, dvflagcarcharge,'
                            + '       dvflagbill, dvflagextnew, dvflagnopinang, cxno, a.dvno'
                            + '  from queuetx a left join drv b on a.dvid = b.dvid'
                            + ' where a.mcid = ' + IntToStr(carList_id) +
                            '   and a.qupid = ' + theQueueID +
                            ' and a.qxstatus = 0 order by a.qxdtin');
                        Open;
                        gp_ShowMsg(carList_id, '�]' + cxno + '�^�ƯZ�Ҧ��G' + temp +
                            '�ƯZ�IID:' + theQueueID + ', �W��:' + theQueueName +
                            ' *' + gf_GetQuid(theQueueID) + '*(' + theQueueID +
                            ') ���ƶq:' + IntToStr(RecordCount));
                        GoQueueCarNO := '';

                        while not Eof do begin
                           if CancelPaich = true Then begin
                              ProcessData('B,' + '���_����' + #10);
                              exit;
                           end;
                           asql1 := 'select cxno, qxdtaskdrv from queuetx a' +
                               ' where a.mcid = ' + IntToStr(carList_id) +
                               ' and a.qupid = ' + theQueueID +
                               ' and qxdtout is null and dvno = ' +
                               quotedstr(FieldByName('dvno').AsString) +
                               ' and dvno not in (select bsdvno from blacksheep where mcid = '
                               + IntToStr(carList_id) + ' and bspgphone = ' +
                               quotedstr(cxtel) + ' and bsdvno = ' +
                               quotedstr(FieldByName('dvno').AsString) + ')';

                           lv_Data := gf_Get1Record(carList_id, asql1);

                           if (not varisnull(lv_Data[0])) and
                               (MinutesBetween(Now, lv_Data[1]) < 5) then begin
                              // �ӥq�� ���Q�ݨ� �n�� cxno, �~��U�@�ӥq��
                              Next;
                              Continue;
                           end;

                           la_Term := ls_Term.Split([',']);
                           lb_OK := true;
                           ls_V := '';

                           for li_i := 0 to High(la_Term) do
                              if la_Term[li_i] = 'Y' then
                                 // �n�P�_ �s���B�L�ϡBRV���B�����ȡB���d��
                                 if la_Term[li_i] <> FieldByName(ls_Field[li_i])
                                     .AsString then begin // ���ű���
                                    {
                                      if showDetailMessage.Checked then
                                      begin
                                      for j := 0 to High(ls_Field) do
                                      ls_V := ls_V + ls_Field[j] + ',';

                                      gp_ShowMsg(FieldByName('mcid').AsInteger,
                                      '�q�����š]�s���B�L�ϡBRV���B�����ȡB���d���B�j�d���B�ɹq�B����B�D�`�s�B�L�b�}�^����G' +
                                      ls_Term + '�F' + ls_V);
                                      end;
                                    }
                                    lb_OK := False;
                                    Break;
                                 end;

                           if lb_OK then begin
                              Get0Value_res :=
                                  gf_Get0Value(carList_id,
                                  'select pid from queuetx  where mcid = ' +
                                  FieldByName('mcid').AsString +
                                  '   and qupid = ' + theQueueID +
                                  ' and qxdtout is null and dvid = ' +
                                  FieldByName('dvid').AsString);

                              if Get0Value_res <> null then
                                 queuetx_strpid := VarToStr(Get0Value_res)
                              else begin
                                 Next;
                                 Continue;
                              end;

                              gf_ExecuteSQL(FieldByName('mcid').AsInteger,
                                  'update queuetx set qxdtdrvanswer = NULL, cxno = '
                                  + cxno + '     , qxdtaskdrv = NOW()' +
                                  ' where pid =' + queuetx_strpid);
                              GoQueueCarNO := FieldByName('dvid').AsString;
                              dvno := FieldByName('dvno').AsString;

                              // block black sheep.
                              blockSQL :=
                                  'select count(bsdvno) from blacksheep where mcid = '
                                  + IntToStr(carList_id) + ' and bspgphone = ' +
                                  quotedstr(cxtel) + ' and bsdvno = ' +
                                  quotedstr(dvno);

                              // 2016.3.24, �[�J�ƯZ�I�¦W��L�o�\��
                              lv_Data := gf_Get1Record(carList_id, blockSQL);
                              if (not varisnull(lv_Data[0])) and (lv_Data[0] = 0)
                              then begin
                                 // 2016.3.15 update calltx, ��s cxstep, cxqxdvno
                                 sqlCmd := 'update calltx set cxstep=''QX'', cxqxdvno='''
                                     + dvno + ''' where dvno is null and cxno = '
                                     + cxno;
                                 gf_ExecuteSQL(0, sqlCmd);

                                 gp_ShowMsg(carList_id,
                                     '�]' + cxno + '�^�����x��:' + dvno);
                                 // �o�X�����T�����q��
                                 car_id := pickTaxiByfind(carList_id,
                                     GoQueueCarNO, cxno, cxname, cxtel,
                                     HttpEncode(UTF8Encode('���� ')) + cxfraddr,
                                     cxtoaddr, cxgotime, cxgodis, cxfrlocref);
                                 // ���q������ɡA��q��id��J car_id
                                 temp := '�߰� ' + theQueueName + ' �ƯZ�I �q�� ' +
                                     dvno + ' �X��';
                                 // todo AThread.Connection.IOHandler.

                                 ProcessData('A,' + temp + #10);

                                 // gp_ShowMsg(carList_ID, '�]' + cxno + '�^�e�X�������ȵ��x��:' + dvno);
                                 // ���ݥq���^��,�ˬdsit�ܼ� ��15���h��APP�^���AAPP�b10�����|������
                                 temp := WaitTaxiAns(carList_id, car_id,
                                     theQueueID, cxno, dvno, queuetx_strpid);

                                 // if showDetailMessage.Checked then
                                 // gp_ShowMsg(carList_ID, '�]' + cxno + '�^�x��:' + dvno +
                                 // ' �^�аT��:' + temp);

                                 if temp = '' then // �q���ڵ�
                                 begin
                                    temp := '�q�� ' + dvno + ' ���';
                                    // todo   AThread.Connection.IOHandler.
                                    ProcessData('A,' + temp + #10);
                                 end
                                 else if Copy(temp, 1, 1) = 'P' then begin
                                    if CancelPaich = true Then begin
                                       ProcessData('B,' + '���_����' + #10);
                                       Break;
                                    end;
                                    haveCar := true;
                                    Result := 'Y' + temp;

                                    // todo  AThread.Connection.IOHandler.
                                    ProcessData(Result + #10);
                                    if InsertMsgouttx(StrToInt(car_id),
                                        'R' + cxno + ';') = False then
                                       gp_ShowMsg(carList_id,
                                        '(' + cxno + ')' + '�x��:' + dvno +
                                        ' VTA�^�Хq������g�JMSGOUT����');
                                    Break;
                                 end
                                 else
                                    gp_ShowMsg(carList_id,
                                        '�]' + cxno + '�^Error:(E002338)�������q�T');
                              end
                              else begin
                                 // 2016.05.20 �[�J: �ŦX�¦W���, ��ܦb
                                 // TK ���T���C�W
                                 ProcessData('A,' + '�x��:' + dvno + '���¦W��, �D�ʲ��L' + #10);
//                                 gp_ShowMsg(carList_id,
//                                        '(' + cxno + ')' + '�x��:' + dvno +
//                                        ' VTA�^�Хq������g�JMSGOUT����');
                              end;

                           end;

                           Next;
                        end;

                        if GoQueueCarNO = '' then // �L���i��
                        begin
                           haveCar := False;
                        end;

                        // W-140429 9. �ƯZ�����Q �u�X�bxmcid/xquid ��dvid list
                     except
                        on E: Exception do
                           gp_ShowMsg(carList_id, '�]' + cxno + '�^�X�{���~�G' +
                               E.Message);
                     end;
                  finally
                     Active := False;
                     Free;
                  end;
               end;
            end;
         end; // theQueueID=999999
         // 1:�ƯZ�I���� end
         // 2:�m�мҦ� begin
         if (not haveCar) then // 2:�m�мҦ�
         begin
            if theQueueID = '-1' Then begin
               gp_ShowMsg(carList_id, '�]' + cxno + '�^' + theQueueName);
               // todo AThread.Connection.IOHandler.
               ProcessData('A,' + theQueueName + #10);
            end
            else begin
               gp_ShowMsg(carList_id, '�]' + cxno + '�^�����ƯZ�Ҧ�');
               // todo AThread.Connection.IOHandler.
               ProcessData('A,' + '�ƯZ�I�L���i��' + #10);
            end;
            StepFlag := 'PS�m�мҦ�';
            Result := lp_RunBidMode(1, '�@');

            if Result = 'N' then begin
               // AThread.Connection.IOHandler.Write(result + #10);
               for i := 1 to 3 do begin
                  Sleep(100);
                  // Application.ProcessMessages;
               end;

               Result := lp_RunBidMode(2, '�G');

            end;

         end;
         gp_ShowMsg(carList_id,
             '..............................................................................');
         // 2:�m�мҦ� end

         if Result = 'N' then begin
            // todo AThread.Connection.IOHandler.
            ProcessData(Result + #10);
            gf_ExecuteSQL(carList_id,
                'update calltx set cxdtpgfinish = NOW(), cxdtcsrclick = NOW() where cxno = '
                + cxno);

         end;
      end // end 'C'
      // W-140516 7. ���Q���� �]VTA/�q��APP�^
      // W-140516 8. CSR �e�T���H�q��     ��������                          �o�G����
      else if AnsiSameText(SocketType, 'D') or AnsiSameText(SocketType, 'M') or
          AnsiSameText(SocketType, 'R') then // ��_�ƯZ 14/6/21 jieshu
      begin // �������b�� + ',' + �K�X + ',' + �x�� + ',' + �渹 + '#'
         // �������b�� + ',' + �K�X + ',' + �x�� + ',' + �خ� + '#'
         if AnsiSameText(SocketType, 'D') then
            NowMissionName := '��������'
         else if AnsiSameText(SocketType, 'M') then
            NowMissionName := '�o�G����'
         else
            NowMissionName := '��_�ƯZ';
         StepFlag := 'PS' + NowMissionName;

         gp_ShowMsg(carList_id, 'Socket Data ' + NowMissionName +
             '>>> ' + temp);
         // �ѫʥ]----------

         carList_id := StrToIntDef(getStrBycc(temp, ','), 0);
         paiche_id := getStrBycc(temp, ','); // ������id
         paiche_password := getStrBycc(temp, ','); // �K�X
         car_id := getStrBycc(temp, ',');
         tmpquid := temp; // getStrBycc(temp, #10); // �s�渹  �T�� quid
         res := gf_CheckCSRuser(carList_id, paiche_id, paiche_password);
         // check CSR ID, PW

         if (carList_id = 0) or (car_id = '') or (tmpquid = '') then
            res := NowMissionName + '��ƿ��~�A�������G' + IntToStr(carList_id) + '�A�s��' +
                car_id + '�A�T��(quid)�G' + tmpquid;

         if res = '' then // �S���~�~�@
            if NowMissionName = '��������' then
               res := gf_PaicheCancel(carList_id, car_id, tmpquid)
               // ��Memory�q����������

            else if NowMissionName = '��_�ƯZ' then begin
               if (tmpquid <> '') and (carList_id <> null) then
                  res := gf_RestorePaiBan(carList_id, car_id,
                      gf_GetQupid(carList_id, tmpquid))
               else
                  res := NowMissionName + '��ƿ��~�A�������G' + IntToStr(carList_id) +
                      ' quid:' + tmpquid;

            end;

         if res = '' then begin
            gp_ShowMsg(carList_id, NowMissionName + '���\�]' + tmpquid + '�^�C');
            // WData[1] := Ord('1'); // 0 ����
            Result := 'Z1';
            // jieshu 14/6/6
            if NowMissionName = '��������' then begin
               if paiche_id = '0' then // csid = 0 �N�O���ȦۧU
                  paiche_id := '5' // ���ȦۧU ���� ���� cxsuccess �G= 5
               else
                  paiche_id := '3'; // csr �������� cxsuccess �G= 3

               res := gf_UpdateCallTxCxSuccess(cxno, paiche_id);

               if res <> '' then
                  gp_ShowMsg(carList_id, '��sCxSuccess�X�{���~�G' + res)
               else
                  ProcessData(Result);
            end;
         end
         else begin
            // WData[1] := Ord('0'); // 0 ����
            gp_ShowMsg(carList_id, NowMissionName + '���ѡ]' + res + '�^�I�I');
            temp := res; // �Ȧs���~�P�_�� jieshu 14/6/8
            res := res;

            Result := 'Z0' + res;

            if (NowMissionName = '��������') and
                ((Pos('�S���������渹', temp) > 0) or (Pos('���W�u', temp) > 0)) then
            begin
               if paiche_id = '0' then // csid = 0 �N�O���ȦۧU
                  paiche_id := '5' // ���ȦۧU ���� ���� cxsuccess �G= 5
               else
                  paiche_id := '3'; // csr �������� cxsuccess �G= 3

               res := gf_UpdateCallTxCxSuccess(tmpquid, paiche_id);

               if res <> '' then
                  gp_ShowMsg(carList_id, '��sCxSuccess�X�{���~�G' + res)
               else
                  ProcessData(Result);
            end;
         end;
      end;
      StepFlag := 'PS����';
   except
      on E: Exception do begin
         gp_ShowMsg(carList_id, '�X�{���~�G' + NowMissionName + ' -- ' + E.Message);
      end;
   end;

end;

end.
