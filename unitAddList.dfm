object frmAddlist: TfrmAddlist
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #24120#29992#22320#40670
  ClientHeight = 383
  ClientWidth = 643
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 643
    Height = 38
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alTop
    BevelInner = bvLowered
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #32048#26126#39636
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitWidth = 837
    object Label1: TLabel
      Left = 12
      Top = 11
      Width = 75
      Height = 15
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = #25628#23563#38364#37749#23383
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #26032#32048#26126#39636
      Font.Style = []
      ParentFont = False
    end
    object Edit1: TEdit
      Left = 98
      Top = 8
      Width = 202
      Height = 23
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #32048#26126#39636
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnKeyDown = Edit1KeyDown
    end
    object Button1: TButton
      Left = 308
      Top = 7
      Width = 66
      Height = 24
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Caption = #25628#23563
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #26032#32048#26126#39636
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = Button1Click
    end
    object BitBtn1: TBitBtn
      Left = 520
      Top = 4
      Width = 89
      Height = 31
      Caption = #38364#38281
      Kind = bkClose
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BitBtn1Click
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 38
    Width = 643
    Height = 345
    Align = alClient
    DataSource = ds_psgr_locbk
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    PopupMenu = PopupMenu1
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -16
    TitleFont.Name = #32048#26126#39636
    TitleFont.Style = []
    OnCellClick = DBGrid1CellClick
    Columns = <
      item
        Expanded = False
        FieldName = 'lbloc'
        Title.Caption = #22320#40670
        Width = 300
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lblocref'
        Title.Caption = #20633#35387
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'lbqunm'
        Title.Caption = #25490#29677#31449
        Width = 100
        Visible = True
      end>
  end
  object zq_psgr_locbk: TFDQuery
    Connection = DM1.FDConnection1
    SQL.Strings = (
      'select *, cxfrLoc as_State from calltx limit 10')
    Left = 268
    Top = 164
  end
  object ds_psgr_locbk: TDataSource
    DataSet = zq_psgr_locbk
    Left = 352
    Top = 164
  end
  object PopupMenu1: TPopupMenu
    Left = 468
    Top = 164
    object D1: TMenuItem
      Caption = #21034#38500'(&D)'
      OnClick = D1Click
    end
  end
end
