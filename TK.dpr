program TK;

uses
  Vcl.Forms,
  unitSendCar in 'unitSendCar.pas' {frmSendCar},
  DataModule in 'DataModule.pas' {DM1: TDataModule},
  Paiche in 'Paiche.pas' {fPaiche},
  unitAddList in 'unitAddList.pas' {frmAddlist},
  CDMsg in 'CDMsg.pas' {CDMsgForm},
  SuppLoc in 'SuppLoc.pas' {fSuppLoc},
  block in 'block.pas' {fBlock},
  PSO in 'PSO.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDM1, DM1);
  Application.CreateForm(TfrmSendCar, frmSendCar);
  Application.CreateForm(TfrmAddlist, frmAddlist);
  Application.CreateForm(TfPaiche, fPaiche);
  Application.CreateForm(TfSuppLoc, fSuppLoc);
  Application.CreateForm(TfBlock, fBlock);
  Application.Run;
end.
