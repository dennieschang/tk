//W-140530 2�D	CSR �� �����Q��bug�G�w���z�O�A�j�S�ʆz�O�C�t�~�A�����خ��ب��A�r�Ӥp�C
unit CDMsg;

interface

uses
 Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
 Dialogs, StdCtrls, ExtCtrls, ToolWin, ComCtrls, Buttons, ImgList;

type
 TCDMsgForm = class(TForm)
  PnlMessage: TPanel;
  MmoMessage: TMemo;
  PnlIcon: TPanel;
    ImgIcon: TImage;
    BtnYes: TButton;
    BtnNo: TButton;
    BtnExit: TButton;
  procedure FormShow(Sender: TObject);
  procedure FormClose(Sender: TObject; var Action: TCloseAction);
  procedure BtnYesClick(Sender: TObject);
  procedure BtnNoClick(Sender: TObject);
  procedure BtnExitClick(Sender: TObject);
 private
  { Private declarations }
 public
  { Public declarations }
 end;

var
  CDMsgForm: TCDMsgForm;
  CloseType:TModalResult;
  IconIDs: array[TMsgDlgType] of PChar = (IDI_EXCLAMATION, IDI_HAND,
    IDI_ASTERISK, IDI_QUESTION, nil);
  function gf_ShowMsg(as_Msg : String; as_Title : String = 'CSR'; at_DlgType : TMsgDlgType = mtWarning;
    ai_Flag : Integer = 0; at_MsgColor : TColor = clMaroon; at_Parent : TForm = nil) : Integer;
  //MmoMessage�m��,���[�Ŧ�  pihong 2013.9.11
  function gf_ShowMsgLeft(as_Msg : String; as_Title : String = 'CSR'; at_DlgType : TMsgDlgType = mtInformation;
    ai_Flag : Integer = 0; at_MsgColor : TColor = clMaroon) : Integer;
  //pihong 2013.9.11

implementation

{$R *.dfm}

//gf_ShowMsg('����', Caption, mtWarning, 0);
function gf_ShowMsg(as_Msg : String; as_Title : String = 'CSR'; at_DlgType : TMsgDlgType = mtWarning;
    ai_Flag : Integer = 0; at_MsgColor : TColor = clMaroon; at_Parent : TForm = nil) : Integer;
var
  IconID : PChar;
  MsgForm: TCDMsgForm;
  lt_FS : TFormStyle;
begin
  if at_Parent = nil then
    MsgForm := TCDMsgForm.Create(Application)
  else
  begin
    MsgForm := TCDMsgForm.Create(at_Parent);
    MsgForm.Parent := at_Parent;
  end;

  MsgForm.Caption := as_Title;
  MsgForm.MmoMessage.Text := as_Msg;
  MsgForm.MmoMessage.Font.Color := at_MsgColor;
  IconID := IconIDs[at_DlgType];
  MsgForm.ImgIcon.Picture.Icon.Handle := LoadIcon(0, IconID);
  if MsgForm.MmoMessage.Lines.Count<3 then
    MsgForm.MmoMessage.Text := #13#10+#13#10+as_Msg
  else if MsgForm.MmoMessage.Lines.Count<6 then
    MsgForm.MmoMessage.Text := #13#10+as_Msg;

  if ai_Flag = 0 then begin
   MsgForm.BtnExit.Visible := true;
   MsgForm.BtnYes.Visible  := false;
   MsgForm.BtnNo.Visible   := false;
  end else begin
   MsgForm.BtnExit.Visible := false;
   MsgForm.BtnYes.Visible  := true;
   MsgForm.BtnNo.Visible   := true;
  end;

  Windows.Beep(1000, 100);

  if at_Parent <> nil then
  begin
    lt_FS := at_Parent.FormStyle;
    at_Parent.FormStyle := fsNormal;
  end;
  try
    Result := MsgForm.ShowModal;
  finally
    if at_Parent <> nil then
      at_Parent.FormStyle := lt_FS;
  end;

  FreeAndNil(MsgForm);
end;

//gf_ShowMsgLeft('����', Caption, mtWarning, 0,);   //MmoMessage�m��,���[�Ŧ�  pihong 2013.9.11
function gf_ShowMsgLeft(as_Msg : String; as_Title : String = 'CSR'; at_DlgType : TMsgDlgType = mtInformation;
    ai_Flag : Integer = 0; at_MsgColor : TColor = clMaroon) : Integer;
var
  IconID : PChar;
  MsgForm: TCDMsgForm;
begin
  MsgForm := TCDMsgForm.Create(Application);
  MsgForm.Caption := as_Title;
  MsgForm.MmoMessage.Alignment := taLeftJustify ;
  MsgForm.MmoMessage.Text := as_Msg;
  MsgForm.MmoMessage.Font.Color := at_MsgColor;
  IconID := IconIDs[at_DlgType];
  MsgForm.ImgIcon.Picture.Icon.Handle := LoadIcon(0, IconID);

 if ai_Flag = 0 then begin
  MsgForm.BtnExit.Visible := true;
  MsgForm.BtnYes.Visible  := false;
  MsgForm.BtnNo.Visible   := false;
 end else begin
  MsgForm.BtnExit.Visible := false;
  MsgForm.BtnYes.Visible  := true;
  MsgForm.BtnNo.Visible   := true;
 end;

 Windows.Beep(1000, 100);
 Result := MsgForm.ShowModal;
 FreeAndNil(MsgForm);
end;

procedure TCDMsgForm.FormShow(Sender: TObject);
begin
 CloseType:=mrNo;
 Beep;
end;

procedure TCDMsgForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 ModalResult := CloseType;
 Beep;
end;

procedure TCDMsgForm.BtnYesClick(Sender: TObject);
begin
 CloseType:=mrYes;
 CLOSE;
end;

procedure TCDMsgForm.BtnNoClick(Sender: TObject);
begin
 CloseType:=mrNo;
 CLOSE;
end;

procedure TCDMsgForm.BtnExitClick(Sender: TObject);
begin
 CloseType:=mrCancel;
 CLOSE;
end;

end.
