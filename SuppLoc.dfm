object fSuppLoc: TfSuppLoc
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #35036#20805#22294#36039
  ClientHeight = 371
  ClientWidth = 702
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 16
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 702
    Height = 371
    Align = alClient
    DataSource = ds_SuppLoc
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -16
    TitleFont.Name = #32048#26126#39636
    TitleFont.Style = []
    OnCellClick = DBGrid1CellClick
    Columns = <
      item
        Expanded = False
        FieldName = 'slLoc'
        Title.Caption = #24120#29992#22320#40670
        Width = 663
        Visible = True
      end>
  end
  object zq_SuppLoc: TFDQuery
    Connection = DM1.FDConnection1
    SQL.Strings = (
      'select *, cxfrLoc as_State from calltx limit 10')
    Left = 268
    Top = 164
  end
  object ds_SuppLoc: TDataSource
    DataSet = zq_SuppLoc
    Left = 352
    Top = 164
  end
end
