unit SuppLoc;


interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.DBGrids, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TfSuppLoc = class(TForm)
    zq_SuppLoc: TFDQuery;
    ds_SuppLoc: TDataSource;
    DBGrid1: TDBGrid;
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    gb_In : Boolean;
  end;

var
  fSuppLoc: TfSuppLoc;

implementation

uses datamodule,unitSendCar;

{$R *.dfm}

procedure TfSuppLoc.DBGrid1CellClick(Column: TColumn);
begin
   with frmSendcar do
   begin
     if gb_In then
     begin
       cds_ExtBuffer_ebFrLoc.AsString  := zq_SuppLoc.FieldByName('slLoc').AsString;
//       cds_ExtBuffer_ebFrAddr.AsString := zq_SuppLoc.FieldByName('slAddr').AsString;
       cds_ExtBuffer_ebFrLocRef.AsString := zq_SuppLoc.FieldByName('slLocRef').AsString;
       cds_ExtBuffer_ebFrLat.AsString := zq_SuppLoc.fieldbyname('slLat').AsString;
       cds_ExtBuffer_ebFrLng.AsString := zq_SuppLoc.fieldbyname('slLong').AsString;
     end else
     begin
       cds_ExtBuffer_ebToLoc.AsString  := zq_SuppLoc.FieldByName('slLoc').AsString;
//       cds_ExtBuffer_ebToAddr.AsString := zq_SuppLoc.FieldByName('slAddr').AsString;
       cds_ExtBuffer_ebToLocRef.AsString := zq_SuppLoc.FieldByName('slLocRef').AsString;
       cds_ExtBuffer_ebToLat.AsString := zq_SuppLoc.fieldbyname('slLat').AsString;
       cds_ExtBuffer_ebToLng.AsString := zq_SuppLoc.fieldbyname('slLong').AsString;
     end;
   end; //with frmSendcar do

   Close;
end;

procedure TfSuppLoc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caHide;
end;

end.
