﻿// W-140529 1.Table extbuffer 不再使用，改用 memory。
// W-140530 2．	CSR 按 “派车”bug：已有电话，却说没电话。另外，这个讯息框里，字太小。
// W-140530 3．	按 “派车”如果无车可派，“派车”还要ON
// W-140530 5．	  发送成功的訊息要 update/insert 到 table MSG。
// W-140530 8. 目前查地址有 bug
// W-140530 7．	外线 02，按键上的电话号码，与 下面的号码 不同。
// CSR2 screen layout jieshu 14/6/2 未完工
// CSR2 screen layout jieshu 14/6/2 車輛
// CSR2 screen layout jieshu 14/6/2 不要行車路線
// CSR2 screen layout jieshu 14/6/2 舊的CSR定位
// CSR 2.2.1 Issue 10.	取消派车成功，必须 设定 cxdtcsrclick 的时间。
// jieshu 14/6/7 最近三筆資料修正 改用電話
// 11、12點incall沒出現 逼一聲提醒 jieshu 14/6/8
// jieshu 14/6/16 預約資訊
// 改60秒更新 jieshu 14/6/15
// jieshu 14/6/15 改由日期大到小
// jieshu 14/6/15 地點點擊後，如果是空白，自動放入 mtrcd.mccity
// jieshu 14/6/15 稱謂加到姓名
// pggender tinyint comment '0:女士；1：先生' jieshu 14/6/16
// CSR 2.3 Issues 14 06 18 發布訊息 失敗後，再按 傳送 jieshu 14/6/19
unit unitSendCar;

interface

uses
   Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
   Dialogs, Buttons, ExtCtrls, StdCtrls, ComCtrls, OleCtrls, MSHTML, SHDocVw,
   unitGlobel, PSO, Registry,
   Menus, Grids, DBGrids, IdURI,
   HttpApp, IdGlobal, Vcl.Imaging.pngimage, ThreadSocket, SyncObjs,
   Data.DB, Datasnap.DBClient, Vcl.DBCtrls, Vcl.Mask, System.UITypes, MidasLib,
   ColorButton, Vcl.AppEvnts, Vcl.CheckLst, IdHTTP, IdSSLOpenSSL;

const
   // iMaxArray = 8;
   cNormalColor = clSilver;
   cRingColor = $00FF8000;
   cEditColor = clred; // $008080FF;

Type
   CyrillicString = type AnsiString(950);

type
   TDBGrid = class(DBGrids.TDBGrid)
   protected
      procedure UpdateScrollBar; override;
   end;

   TfrmSendCar = class(TForm)
      // IdTCPClient1: TIdTCPClient;
      cds_ExtBuffer_: TClientDataSet;
      cds_ExtBuffer_ebExtNo: TWordField;
      cds_ExtBuffer_ebPhone: TStringField;
      cds_ExtBuffer_ebPsgrName: TStringField;
      cds_ExtBuffer_ebSex: TStringField;
      cds_ExtBuffer_ebFrLoc: TStringField;
      cds_ExtBuffer_ebFrLocRef: TStringField;
      cds_ExtBuffer_ebFrLng: TStringField;
      cds_ExtBuffer_ebFrLat: TStringField;
      cds_ExtBuffer_ebToLoc: TStringField;
      cds_ExtBuffer_ebToLocRef: TStringField;
      cds_ExtBuffer_ebToLng: TStringField;
      cds_ExtBuffer_ebToLat: TStringField;
      ds_ExtBuffer: TDataSource;
      cds_ExtBuffer_ebInOut: TStringField;
      cds_ExtBuffer_ebPaiche: TBooleanField;
      cds_ExtBuffer_ebTaxi: TWordField;
      cds_ExtBuffer_ebResult: TStringField;
      Panel1: TPanel;
      PageControl1: TPageControl;
      TabSheet1: TTabSheet;
      PanelHistRgion: TPanel;
      PanelHist: TPanel;
      Panel3: TPanel;
      Label12: TLabel;
      Label13: TLabel;
      Label14: TLabel;
      Label15: TLabel;
      btnCancelSendCar: TSpeedButton;
      EditName: TDBEdit;
      editPhone: TDBEdit;
      pgid_Edit: TEdit;
      DBRadioGroup1: TDBRadioGroup;
      ListBoxSendRTMsg: TDBMemo;
      Panel7: TPanel;
      TabSheet2: TTabSheet;
      editLongitudeE: TEdit;
      editLatitudeE: TEdit;
      cds_ExtBuffer_ebCxno: TIntegerField;
      ApplicationEvents1: TApplicationEvents;
      PnlHist1: TPanel;
      btnHist1: TSpeedButton;
      LabelHist1: TLabel;
      btnHistCancel1: TSpeedButton;
      PnlHist2: TPanel;
      btnHist2: TSpeedButton;
      LabelHist2: TLabel;
      btnHistCancel2: TSpeedButton;
      PnlHist3: TPanel;
      btnHist3: TSpeedButton;
      LabelHist3: TLabel;
      btnHistCancel3: TSpeedButton;
      Panel10: TPanel;
      btnFinish: TSpeedButton;
      Label24: TLabel;
      Panel11: TPanel;
      Label26: TLabel;
      btnSendCar: TSpeedButton;
      cmbTAXI_NUM: TDBComboBox;
      editThreads: TEdit;
      lvStatus: TListView;
      ColorButton4: TColorButton;
      Panel4: TPanel;
      DateTimePicker1: TDateTimePicker;
      RadioButton4: TRadioButton;
      RadioButton3: TRadioButton;
      ComboBox1: TComboBox;
      Label28: TLabel;
      ComboBox2: TComboBox;
      Label29: TLabel;
      Label30: TLabel;
      ColorButton5: TColorButton;
      DBMemo1: TDBMemo;
      cds_ExtBuffer_ebdtpgneed: TDateTimeField;
      cds_ExtBuffer_ebDcResult: TStringField;
      ColorButton6: TColorButton;
      cds_ExtBuffer_ebFinish: TBooleanField;
      Panel5: TPanel;
      SpeedButton1: TSpeedButton;
      Label31: TLabel;
      Label25: TLabel;
      ComboBox3: TComboBox;
      Label32: TLabel;
      cds_ExtBuffer_ebDvLng: TStringField;
      cds_ExtBuffer_ebDvLat: TStringField;
      cds_ExtBuffer_ebOkQty: TWordField;
      cds_ExtBuffer_ebCxnoList: TStringField;
      cds_Addr_: TClientDataSet;
      cds_Addr_Blng: TFloatField;
      cds_Addr_Blat: TFloatField;
      cds_Addr_Baddress: TStringField;
      GroupBoxtTEST: TGroupBox;
      EditPgid: TEdit;
      Editcxno: TEdit;
      EditHWND: TEdit;
      ExtNo: TLabel;
      DBCheckBox1: TDBCheckBox;
      Label1: TLabel;
      ComboBox4: TComboBox;
      cds_ExtBuffer_ebdatatodrv: TSmallintField;
      Label2: TLabel;
      Panel2: TPanel;
      GroupBox4: TGroupBox;
      Label16: TLabel;
      Label17: TLabel;
      Label19: TLabel;
      Label20: TLabel;
      SearchFromAddressBtn: TSpeedButton;
      editStartLong: TDBEdit;
      editStartLat: TDBEdit;
      editStartPlace: TDBEdit;
      EditStartPS: TDBEdit;
      CheckBox1: TCheckBox;
      ComboBoxAdrList: TComboBox;
      ComboBox5: TComboBox;
      Panel8: TPanel;
      WebBrowser1: TWebBrowser;
      Panel6: TPanel;
      Image2: TImage;
      Image1: TImage;
      ZoomToTwoPlaceBtn: TButton;
      Panel9: TPanel;
      RadioButton1: TRadioButton;
      RadioButton2: TRadioButton;
      Image3: TImage;
      Image4: TImage;
      GroupBox5: TGroupBox;
      Label5: TLabel;
      Label8: TLabel;
      Label9: TLabel;
      Label10: TLabel;
      SearchToAddressBtn: TSpeedButton;
      editEndLong: TDBEdit;
      editEndLat: TDBEdit;
      editEndPlace: TDBEdit;
      EditendPS: TDBEdit;
      CheckBox2: TCheckBox;
      ComboBox6: TComboBox;
      Timer1: TTimer;
      cds_ExtBuffer_ebDvid: TIntegerField;
      TabSheet3: TTabSheet;
      Label7: TLabel;
      cmbDrvNO: TComboBox;
      ComboBox7: TComboBox;
      Label3: TLabel;
      RadioGroup1: TRadioGroup;
      ColorButton1: TColorButton;
      cbFlag1: TCheckBox;
      cbFlag2: TCheckBox;
      cbFlag3: TCheckBox;
      cbFlag4: TCheckBox;
      cbFlag0: TCheckBox;
      Label4: TLabel;
      ComboBox8: TComboBox;
      cbFlag5: TCheckBox;
      cbFlag6: TCheckBox;
      cbFlag7: TCheckBox;
      Label6: TLabel;
      Edit1: TEdit;
      Panel12: TPanel;
      Label11: TLabel;
      Image5: TImage;
      Image6: TImage;
      Label18: TLabel;
      Label21: TLabel;
      Image7: TImage;
      Image8: TImage;
      Label22: TLabel;
      Label23: TLabel;
      Image9: TImage;
      Image10: TImage;
      Label33: TLabel;
      Label34: TLabel;
      Label35: TLabel;
      Label36: TLabel;
      Label37: TLabel;
      Label38: TLabel;
      ColorButton2: TColorButton;
      Label45: TLabel;
      RadioButton5: TRadioButton;
      RadioButton6: TRadioButton;
      Label46: TLabel;
      Edit2: TEdit;
      ComboBox9: TComboBox;
      Label47: TLabel;
      ComboBox10: TComboBox;
      Label48: TLabel;
      ComboBox11: TComboBox;
      Label49: TLabel;
      Edit3: TEdit;
      Label50: TLabel;
      Label51: TLabel;
      Edit4: TEdit;
      Edit5: TEdit;
      Label52: TLabel;
      Edit6: TEdit;
      Edit7: TEdit;
      Label53: TLabel;
      Label54: TLabel;
      Label55: TLabel;
      Label56: TLabel;
      Label57: TLabel;
      RadioButton7: TRadioButton;
      Edit8: TEdit;
      ComboBox12: TComboBox;
      ComboBox13: TComboBox;
      ComboBox14: TComboBox;
      Edit9: TEdit;
      Label58: TLabel;
      Edit10: TEdit;
      Edit11: TEdit;
      Label59: TLabel;
      Edit12: TEdit;
      Edit13: TEdit;
      RadioButton8: TRadioButton;
      Label60: TLabel;
      sbtCommon2: TSpeedButton;
      Label61: TLabel;
      Label62: TLabel;
      cbFlag8: TCheckBox;
      cbFlag9: TCheckBox;
      ColorButton3: TColorButton;
      Label63: TLabel;
      Label64: TLabel;
      ColorButton7: TColorButton;
      Label27: TLabel;
      Label39: TLabel;
      Label40: TLabel;
      Label41: TLabel;
      Label42: TLabel;
      Shape1: TShape;
      Shape2: TShape;
      Shape3: TShape;
      Shape4: TShape;
      Shape5: TShape;
      Shape6: TShape;
      Label43: TLabel;
      Lcxno1: TLabel;
      Lcxno2: TLabel;
      Lcxno3: TLabel;
      Label44: TLabel;
      ds_car: TDataSource;
      cds_car_: TClientDataSet;
      DBGrid1: TDBGrid;
      cbFlag10: TCheckBox;
      cbFlag11: TCheckBox;
      cbFlag12: TCheckBox;
      cbFlag13: TCheckBox;
      Label65: TLabel;
      Panel13: TPanel;
      btnRePaiNow: TColorButton;
      Panel14: TPanel;
      TabSheet4: TTabSheet;
      Label66: TLabel;
      ComboBox15: TComboBox;
      Label67: TLabel;
      ComboBox16: TComboBox;
      Label68: TLabel;
      ComboBox17: TComboBox;
      Label69: TLabel;
      ComboBox18: TComboBox;
      ColorButton8: TColorButton;
      ColorButton9: TColorButton;
      TimerReFireShowDriverAndCustomer: TTimer;
      Label70: TLabel;
      RadioGroup2: TRadioGroup;
      Label71: TLabel;
      Label72: TLabel;
      ComboBox19: TComboBox;
      ComboBox20: TComboBox;
      Label73: TLabel;
      ComboBox21: TComboBox;
      ComboBox22: TComboBox;
      Label74: TLabel;
      Label75: TLabel;
      Label76: TLabel;
      ComboBox23: TComboBox;
      ComboBox24: TComboBox;
      procedure btnStartMapClick(Sender: TObject);
      procedure FormCreate(Sender: TObject);
      procedure CheckBoxTrafficClick(Sender: TObject);
      procedure CheckBoxStreeViewClick(Sender: TObject);
      procedure btnEndMapClick(Sender: TObject);
      procedure editStartPlaceExit(Sender: TObject);
      procedure btnSendCarClick(Sender: TObject);
      procedure panelPhoneClick(Sender: TObject);
      procedure ZoomToTwoPlaceBtnClick(Sender: TObject);
      procedure btnFinishClick(Sender: TObject);
      procedure btnHistCancel1Click(Sender: TObject);
      procedure btnCancelSendCarClick(Sender: TObject);
      procedure FormClose(Sender: TObject; var Action: TCloseAction);
      procedure WebBrowser1CommandStateChange(ASender: TObject;
          Command: Integer; Enable: WordBool);
      procedure FormKeyPress(Sender: TObject; var Key: Char);
      procedure editStartPlaceEnter(Sender: TObject);
      procedure btnHist1Click(Sender: TObject);
      procedure sbtCommon1Click(Sender: TObject);
      procedure cds_ExtBuffer_AfterInsert(DataSet: TDataSet);
      procedure cds_ExtBuffer_AfterScroll(DataSet: TDataSet);
      procedure RadioButton1Click(Sender: TObject);
      procedure cds_ExtBuffer_ebPaicheValidate(Sender: TField);
      procedure ListBoxSendRTMsgChange(Sender: TObject);
      procedure ApplicationEvents1Exception(Sender: TObject; E: Exception);
      procedure editPhoneChange(Sender: TObject);
      procedure DBRadioGroup1Change(Sender: TObject);
      procedure RadioButton3Click(Sender: TObject);
      procedure DateTimePicker1Change(Sender: TObject);
      procedure ComboBox1KeyPress(Sender: TObject; var Key: Char);
      procedure ColorButton5Click(Sender: TObject);
      procedure ColorButton6Click(Sender: TObject);
      procedure cds_ExtBuffer_ebFinishValidate(Sender: TField);
      procedure ColorButton4Click(Sender: TObject);
      procedure PageControl1DrawTab(Control: TCustomTabControl;
          TabIndex: Integer; const Rect: TRect; Active: Boolean);
      procedure SpeedButton1Click(Sender: TObject);
      procedure EditStartPSEnter(Sender: TObject);
      procedure cds_ExtBuffer_ebFrLocValidate(Sender: TField);
      procedure cds_ExtBuffer_ebToLocValidate(Sender: TField);
      procedure FormDestroy(Sender: TObject);
      procedure ComboBoxAdrListChange(Sender: TObject);
      procedure ComboBoxAdrListCloseUp(Sender: TObject);
      procedure FormResize(Sender: TObject);
      procedure ComboBox5Click(Sender: TObject);
      procedure ComboBox6Click(Sender: TObject);
      procedure FormActivate(Sender: TObject);
      procedure ComboBoxAdrListClick(Sender: TObject);
      procedure WebBrowser1DocumentComplete(ASender: TObject;
          const pDisp: IDispatch; const URL: OleVariant);
      procedure Timer1Timer(Sender: TObject);
      procedure ColorButton1Click(Sender: TObject);
      procedure editEndPlaceExit(Sender: TObject);
      procedure ColorButton2Click(Sender: TObject);
      procedure ComboBox9Change(Sender: TObject);
      procedure ComboBox12Change(Sender: TObject);
      procedure ComboBox10Change(Sender: TObject);
      procedure ComboBox13Change(Sender: TObject);
      procedure ComboBox12Enter(Sender: TObject);
      procedure ComboBox9Enter(Sender: TObject);
      procedure RadioButton8Click(Sender: TObject);
      procedure RadioButton5Click(Sender: TObject);
      procedure ComboBox13Exit(Sender: TObject);
      procedure ComboBox10Exit(Sender: TObject);
      procedure ColorButton3Click(Sender: TObject);
      procedure ComboBox5Enter(Sender: TObject);
      procedure Panel3Exit(Sender: TObject);
      procedure editPhoneEnter(Sender: TObject);
      procedure WebBrowser1ShowScriptError(ASender: TObject;
          const AErrorLine, AErrorCharacter, AErrorMessage, AErrorCode,
          AErrorUrl: OleVariant; var AOut: OleVariant; var AHandled: Boolean);
      procedure btnRePaiNowClick(Sender: TObject);
      procedure WebBrowser1BeforeNavigate2(ASender: TObject;
          const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData,
          Headers: OleVariant; var Cancel: WordBool);
      procedure ColorButton8Click(Sender: TObject);
      procedure ColorButton9Click(Sender: TObject);
      procedure TimerReFireShowDriverAndCustomerTimer(Sender: TObject);
      procedure RadioGroup2Click(Sender: TObject);
      procedure editPhoneExit(Sender: TObject);

   private
      { Private declarations }
      bExit, gb_First, gb_Focus: Boolean; // 程式離開
      bCancelcar, CancelPaich: Boolean; // 取消派車
      // ClientThread:TClientThread;
      HTMLWindow2: IHTMLWindow2;

      /// ///////////////////////////////////////////////////////////
      ///
      // FDefaultCaption   : String;
      fThreads: TList;
      // FClientsConnected : Boolean;
      uiLock: TCriticalSection;
      iMaxConnections, iConnectionsMade: Integer;
      /// ///////////////////////////////////////////////////////////
      iRouteTimes, iRoutedist: Integer; // CXNO派車單號
      ValidCarCount: Integer; // 有效車輛
      // CSR2 screen layout jieshu 14/6/2 車輛
      gd_CarTime: TDateTime;
      gs_LogPath, gs_dcno, gs_AddrNo: String; // jieshu 14/6/14 紀錄Log
      gb_NotEnter, gb_WebOK: Boolean; // jieshu 14/6/20 移Focus不觸發Enter事件
      gt_Queue: TStrings;
      cxnoList: TStringList;

      // get lat, lon from sURL2
      sURLLat: string;
      sURLLon: string;

      procedure CalculateDistances(slng1, slat1, slng2, slat2: string;
          var iTimes, idistant: Integer); // Google服務取得兩地行車時間
      procedure CreatePhoneObject; // 建立電話物件
      procedure ClearEdit; // 清除欄位、地圖
      procedure showPosition; // 把地圖的範圍拉到二個圖標的大小
      procedure writeout(as_AsNo: String = '');
      // W-140529 1.Table extbuffer 不再使用，改用 memory。
      // procedure WriteInBuffer(iEXTNO:integer);//各分機暫存檔
      // procedure WriteOutBuffer;//各分機暫存檔
      procedure DeleteBuffer(iEXTNO: Integer); // 各分機暫存檔刪除
      procedure updatecalltx(iStatus, icxno, icxsuc: Integer);
      procedure preFindAddressByAddress(addr: string);
      function proccessAddressChose(saddresStr: string): string;
      function gf_CheckMapData(as_Addr: String; ab_In: Boolean): String;
      function gotoaddress(sAddr: string; ab_Clear: Boolean): string;
      procedure gp_ValidateInput;
      procedure readini;
      procedure gp_CheckPhone;
      procedure gp_A2_P2N(at_A2, at_A1: TComboBox);
      procedure gp_GetAddrNo(as_Addr: String);
      procedure Do_CloseRtn;
      function WriteUsrtx(mcid: Integer; usid, uxid, uxfield, uxnewvalue,
          uxoldvalue: String): Boolean;
      function EnCode_ebFrLoc: String;
      function GetPlaceAddress(place: String): string;

   const
      ls_Field: Array [0 .. 13] of String = ('dvflagNew', 'dvflagNoSmoke',
          'dvflagRV', 'dvflagWheelChair', 'dvflagPet', 'dvflagbigpet',
          'dvflagcarcharge', 'dvflagbill', 'dvflagextnew', 'dvflagnopinang',
          'dvflagtinymove', 'dvflagstoreshipping', 'dvflagbike', 'dvflagcarry');

      ls_FieldCx: Array [0 .. 13] of String = ('cxflagNew', 'cxflagNoSmoke',
          'cxflagRV', 'cxflagWheelChair', 'cxflagPet', 'cxflagbigpet',
          'cxflagcarcharge', 'cxflagbill', 'cxflagextnew', 'cxflagnopinang',
          'cxflagtinymove', 'cxflagstoreshipping', 'cxflagbike', 'cxflagcarry');

   public
      { Public declarations }
      // 電話物件
      HTMLStr: AnsiString;
      gb_Finish: Boolean;
      gb_WaitingForCancel: Boolean;
      gt_quid: TStrings;
      SocketSendData: TSocketSendData;
      { aPnlPhone: array [0 .. iMaxArray - 1] of TPanel;
        aLabExt: array [0 .. iMaxArray - 1] of TLabel;
        aShpPhone: array [0 .. iMaxArray - 1] of TShape;
        alabPhone: array [0 .. iMaxArray - 1] of TLabel;
        aImgLight: array [0 .. iMaxArray - 1] of TImage; // VTA SOCKET 回報燈號
      }
      /// //////////////////////////////////////////////////////////////////////////////////
      ///
      PSO: TPSO;
      manualFillInCarCount: Integer;

      procedure writein(var at_SSD: TSocketSendData; quid: String;
          as_AsNo: String = ''; ab_calltx: Boolean = True);
      procedure StartThreads(iState: Integer; at_SSD: TSocketSendData);
      procedure StopThreads;
      function gf_CancelPaiche(as_Cxno, as_dvid, as_Phone,
          as_dvno: String): Boolean;
      procedure gp_UpdateCsrClick(as_Cxno: String);
      // CSR2 screen layout jieshu 14/6/2 車輛
      procedure gp_ShowCarInfo;
      procedure gp_ShowDefer; // jieshu 14/6/15 預約
      procedure gp_ExecPaiche(var at_SSD: TSocketSendData);
      procedure ShowAddMessage(iEXTNO: Integer; sMsg: string);
      procedure gp_WriteToLog(as_Msg: String);
      procedure lp_AddCar(as_dvno: String; ar_min: String);

      procedure queryAddressReplaceClick;
   end;

var
   frmSendCar: TfrmSendCar;

implementation

uses ActiveX, datamodule, unitAddList, CDMsg, iniFiles,
   Paiche, SuppLoc, block, Math;

{$R *.dfm}

const
   // CSR2 screen layout jieshu 14/6/2 舊的CSR定位
   HTMLStr1: AnsiString = '<html>                               ' +
       '<head>                                                  ' +
   // '<meta http-equiv="X-UA-Compatible" content="IE=edge"/>  ' +
       '<meta name="viewport" content="initial-scale=1.0, user-scalable=yes" /> '
       + '<script type="text/javascript" src="http://maps.google.com/maps/api/js?AIzaSyAKXofW6wr5eM1EtiYkX-OJKYzkuJgm2vo&v=3.24&sensor=false"></script> '
       + '<script type="text/javascript">                       ' +
       '      var Blng="";                                      ' +
       '      var Blat="";                                      ' +
       '      var Baddresss="";                                 ' +
       '      var Aaddresss=" ";                                ' +
       '      var geocoder;                                     ' +
       '      var map;                                          ' +
       '      var trafficLayer;                                 ' +
       '      var bikeLayer;                                    ' +
       '      var markersArray = [];                            ' +
       '      var Btime="1";                                    ' +
       '      var Bdistant="1";                                 ' +
       '      var Btimevalue=0;                                 ' +
       '      var Bdistantvalue=0;                              ' +
       '      var Bhtml="1";                                    ' +
       '      var haveClick=0;                                  ' +

       '  function initialize() { ' +
       '      geocoder = new google.maps.Geocoder();';

   HTMLStr2: AnsiString = '    var myOptions = {                ' +
       '      zoom: 15,                                         ' +
       '      center: latlng,                                   ' +
       '      scaleControl: true,                               ' +
       '      scaleControlOptions: {                            ' +
       '           position: google.maps.ControlPosition.BOTTOM_RIGHT' +
       '      },                                                ' +
       '      mapTypeId: google.maps.MapTypeId.ROADMAP          ' +
       '    };                                                  ' +
       '    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions); '
       + '  trafficLayer = new google.maps.TrafficLayer();' +
       '    bikeLayer = new google.maps.BicyclingLayer();' +
       '    map.set("streetViewControl", false);' +
   // '    google.maps.event.addListener(map, "click", ' +
   // '         function(event) {' +
   // '             document.getElementById("LatValue").value = event.latLng.lat(); '
   // + '           document.getElementById("LngValue").value = event.latLng.lng(); '
   // '                         codeLatLngAddredd(event.latLng.lat(),event.latLng.lng());' +
   // '                         haveClick = 1;  ' +
   // '                         ClearMarkers();' +
   // '         } ' +
   // '   );'
       '  } ' + '' +

       '  function zoomToSpan(Lat1, Lang1,Lat2, Lang2) { ' +
       '     var southWest = new google.maps.LatLng(Lat1, Lang1);  ' +
       '     var northEast = new google.maps.LatLng(Lat2, Lang2); ' +
       '     var bounds = new google.maps.LatLngBounds(southWest, northEast);   '
       + '     map.fitBounds(bounds);  ' + '  }' + '' +

       '  function setCenter(){ ' +
       '     var bounds = new google.maps.LatLngBounds(); ' +
       '     if (markersArray) {                                   ' +
       '        for (i in markersArray) {                          ' +
       '           bounds.extend(markersArray[i].position);        ' +
       '        }                                                  ' +
       '     } ' +

       '     map.fitBounds(bounds);                                ' +

       '     if (map.getZoom() > 15){' +
       '        map.setZoom(15);                                   ' +
       '     }                                                     ' +
       '  }                                                       ' +

       '  function prcodeAddress(address) {                       ' +
       '     if (geocoder) {' +
       '        geocoder.geocode( { address: address}, function(results, status) { '
       + '             if (status == google.maps.GeocoderStatus.OK) {' +

       '                  Blng =results[0].geometry.location.lng();' +
       '                  Blat =results[0].geometry.location.lat();' +
       '                  Baddresss =results[0].formatted_address;' +
       '               }                                          ' +
       '        });                                               ' +
       '    }                                                     ' +
       '  }                                                       ' +

       '  function codeAddress(address) {                         ' +
       '      if (geocoder) {' +
       '         geocoder.geocode( { "address": address, "region": "tw"}, function(results, status) { '
       + '           if (status == google.maps.GeocoderStatus.OK) {' +
       '                map.setCenter(results[0].geometry.location);' +
       '                Blng = "";                                 ' +
       '                Blat = "";                                 ' +
       '                Baddresss = "";                            ' +
       '                for (var i = 0; i < results.length; i++){' +
       '                    Blng += results[i].geometry.location.lng() + ",";' +
       '                    Blat += results[i].geometry.location.lat() + ",";' +
       '                    Baddresss += results[i].formatted_address + ",";' +
       '                }                                          ' +
       '             }                                             ' +
       '             else {                                        ' +
       '                Baddresss = status                         ' +
       '             }                                             ' +
       '         });                                               ' +
       '    }                                                      ' +
       '  }                                                        ' +

       '  function codeLatLngAddredd(Lat, Lang) {                  ' +
       '     return;                                               ' +
       '     var latlng = new google.maps.LatLng(Lat,Lang);        ' +
       '     if (geocoder) {                                       ' +
       '        geocoder.geocode( { location:latlng}, function(results, status) { '
       + '          if (status == google.maps.GeocoderStatus.OK) {' +
       '                Aaddresss =results[0].formatted_address;' +
       '            }                                              ' +
       '        });                                                ' +
       '    }                                                      ' +
       '  }                                                        ' +

       '  function codeLatLngonly(Lat, Lang) { ' +
       '     var latlng = new google.maps.LatLng(Lat,Lang);' +
       '     map.setCenter(latlng);                                ' +
       '  }                                                        ' +

       '  function codeLatLng(Lat, Lang) { ' +
       '     var latlng = new google.maps.LatLng(Lat,Lang);        ' +
       '     map.setCenter(latlng);                                ' +
       '     PutMarker(Lat, Lang, Lat+","+Lang);                   ' +
       '     if (geocoder) {' +
       '          geocoder.geocode( { location:latlng}, function(results, status) { '
       + '            if (status == google.maps.GeocoderStatus.OK) {' +
       '                  Baddresss =results[0].formatted_address;' +
       '              }                                                 ' +
       '              else {' +
       '              }                                                 ' +
       '          });                                                   ' +
       '    }                                                           ' +
       '  }                                                             ' +

       '  function GotoLatLng(Lat, Lang) { ' +
       '      var latlng = new google.maps.LatLng(Lat,Lang);' +
       '      map.setCenter(latlng);                             ' +
       '      PutMarker(Lat, Lang, Lat+","+Lang);' +
       '  }                                                      ' +

       '  function GotoLatLngonly(Lat, Lang) { ' +
       '       var latlng = new google.maps.LatLng(Lat,Lang);' +
       '       map.setCenter(latlng);' +
       '  }                                                      ' +

       '  function geocodePosition(newPos) {                     ' +
       '    var lat = newPos.lat();                              ' +
       '    var lng = newPos.lng();                              ' +
       '    window.location = "dragend:///?lat=" + lat + "&lng=" + lng;' +
       '  }                                                        ' +

       '  function PutMarkerIco(Lat, Lang,msg,marktype) { ' +
       '    var latlng = new google.maps.LatLng(Lat,Lang);' +
       '    var marker = new google.maps.Marker({' +
       '                   position: latlng, ' +
       '                   draggable: true,                ' +
       '                   animation: google.maps.Animation.DROP,' +
       '                   map: map,                       ' +
       '                   title: ""+msg' + '  });' +

       '    google.maps.event.addListener(marker, "dragend", ' +
       '            function(){                              ' +
       '                 geocodePosition(marker.getPosition());' +
       '            });                                      ' +

       '    map.setCenter(latlng);' + ' markersArray.push(marker); ' +
       '    if (marktype == 1) {' +
       '        marker.setIcon("file:///{ImagePath}icontt.png");' +
       '        marker.draggable = false;   ' +
       '    }                                                 ' +
       '    else if (marktype == 2){' +
       '        marker.setIcon("file:///{ImagePath}cir_red.png");' +
       '        marker.draggable = false;   ' +
       '    }                                                 ' +
       '    else if (marktype == 3){' +
       '        marker.setIcon("file:///{ImagePath}blueico.png");' +
       '        marker.draggable = false;   ' +
       '    }                                                 ' +
       '    else if (marktype == 4){' +
       '        marker.setIcon("file:///{ImagePath}yellowico.png");' +
       '        marker.draggable = false;   ' +
       '    }                                                 ' +
       '    else if (marktype == 5){' +
       '        marker.setIcon("file:///{ImagePath}red2ico.png");' +
       '        marker.draggable = false;   ' +
       '    }                                                 ' +
       '    else if (marktype == 6){' +
       '        marker.setIcon("file:///{ImagePath}redico.png");' +
   // '        marker.draggable = false;   ' +
       '    }                                                 ' +
       '    else if (marktype == 7){' +
       '        marker.setIcon("file:///{ImagePath}greenico.png");' +
       '        marker.draggable = false;   ' +
       '    }                                                 ' +
       '    else if (marktype == 8){' +
       '        marker.setIcon("file:///{ImagePath}blue2ico.png");' +
       '        marker.draggable = false;   ' +
       '    }                                                 ' +
       '  }                                                   ' +

       '  function ClearMarkers() {                           ' +
       '     if (markersArray) {                              ' +
       '        for (i in markersArray) {                     ' +
       '            markersArray[i].setMap(null);             ' +
       '        }                                             ' +
       '     }                                                ' +

       '     markersArray = [];                               ' +
       '  }                                                   ' +

       '  function PutMarker(Lat, Lang, Msg) { ' +
       '     var latlng = new google.maps.LatLng(Lat,Lang);' +
       '     bounds.extend(latlng);                           ' +
       '     var marker = new google.maps.Marker({            ' +
       '         position: latlng,                            ' +
       '         map: map,' +
       '         title: Msg+" ("+Lat+","+Lang+")"             ' +
       '     });                                              ' +
       '     markersArray.push(marker);                       ' +
       '  }                                                   ' +

       ' function calculateDistances(lng1,lat1,lng2,lat2) {   ' +
       '    var origin = new google.maps.LatLng(lat1, lng1);   ' +
       '    var destination = new google.maps.LatLng(lat2,lng2); ' +
       '    var service = new google.maps.DistanceMatrixService();   ' +
       '    service.getDistanceMatrix(                         ' +
       '    {                                                 ' +
       '      origins: [origin],                              ' +
       '      destinations: [destination],                    ' +
       '      travelMode: google.maps.TravelMode.DRIVING,     ' +
       '      unitSystem: google.maps.UnitSystem.METRIC,      ' +
       '      avoidHighways: false,                           ' +
       '      avoidTolls: false                               ' +
       '    }, callback);                                     ' +
       '  }                                                   ' +
       '                                                      ' +
       ' function callback(response, status) {                 ' +
       '    if (status != google.maps.DistanceMatrixStatus.OK) {' +
       '       Btime="1";                                     ' +
       '       Bdistant=""; ' +
       '    } else {                                          ' +
       '      var results = response.rows[0].elements;        ' +
       '      Bdistant= results[0].distance.text;             ' +
       '      Btime  = results[0].duration.text;              ' +
       '      Bdistantvalue= results[0].distance.value;       ' +
       '      Btimevalue  = results[0].duration.value;        ' +
       '    }                                                 ' +
       ' }                                                    ' +

       '  function TrafficOn()   { trafficLayer.setMap(map); }' + '' +
       '  function TrafficOff()  { trafficLayer.setMap(null); }' +
       '  function BicyclingOn() { bikeLayer.setMap(map); }' + '' +
       '  function BicyclingOff(){ bikeLayer.setMap(null);}' + '' +
       '  function StreetViewOn() { map.set("streetViewControl", true); }' +
       '  function StreetViewOff() { map.set("streetViewControl", false); }' +
       '</script>                                             ' +
       '</head>                                               ' +
       '<body topmargin="0" leftmargin="0" onload="initialize()"> ' +
       '  <div id="map_canvas" style="width:100%; height:100%"></div> ' +
       '  <div id="latlong"> ' + ' <input type="hidden" id="LatValue" >' +
       '  <input type="hidden" id="LngValue" >' + ' </div>    ' +
       '</body>                                               ' + '</html> ';

   /// /////////////////////////////////////////////////////////////////////////////////////////
   /// /////////////////////////////////////////////////////////////////////////////////////////
   /// //////////////////////////////////////////////////////////////////////////////////////////////
procedure TfrmSendCar.ShowAddMessage(iEXTNO: Integer; sMsg: string);
var
   s: string;
begin
   exit; // 20151208 QT, TK 的 CSR log 完成刪除，改用 “操作日誌”
   s := formatdatetime('mmdd hh:nn:ss', now) + '(' + LoginUser.sUserID + ') ';

   if iEXTNO > 0 then
      s := s + '外線 ' + formatfloat('00', iEXTNO) + ' ';

   s := s + sMsg;

   if sLanguage = 'zh-cn' then
      s := GBCht2Chs(s);

   if pos(#10, s) = 0 then
      s := s + #10;
   gp_WriteToLog(s);
end;

// jieshu 14/4/28
function gf_GetFileVersion(as_FileName: String): String;
{ by Steve Schafer }
var
   VerInfoSize: DWORD;
   VerInfo: Pointer;
   VerValueSize: DWORD;
   VerValue: PVSFixedFileInfo;
   Dummy: DWORD;
   V1, V2, V3, V4: Word;
begin
   VerInfoSize := GetFileVersionInfoSize(PChar(as_FileName), Dummy);

   if VerInfoSize = 0 then // 沒有版本資訊
   begin
      Result := '';
      exit;
   end;

   GetMem(VerInfo, VerInfoSize);
   GetFileVersionInfo(PChar(as_FileName), 0, VerInfoSize, VerInfo);
   VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);

   with VerValue^ do begin
      V1 := dwFileVersionMS shr 16; // Major Version
      V2 := dwFileVersionMS and $FFFF; // Minor Version
      V3 := dwFileVersionLS shr 16; // Release
      V4 := dwFileVersionLS and $FFFF; // Build Number
   end;

   FreeMem(VerInfo, VerInfoSize);
   Result := IntToStr(V1) + '.' + IntToStr(V2) + '.' + IntToStr(V3) + '.' +
       IntToStr(V4);
end;

procedure TfrmSendCar.ClearEdit; // 清除欄位、地圖
var
   i: Integer;
begin
   btnSendCar.flat := True;
   sbtCommon2.Enabled := false;

   for i := 0 to GroupBoxtTEST.ControlCount - 1 do
      if GroupBoxtTEST.Controls[i] is TEdit then
         TEdit(GroupBoxtTEST.Controls[i]).Text := '';

   for i := 0 to Panel3.ControlCount - 1 do
      if Panel3.Controls[i] is TEdit then
         TEdit(Panel3.Controls[i]).Text := '';

   for i := 0 to GroupBox4.ControlCount - 1 do
      if GroupBox4.Controls[i] is TEdit then
         TEdit(GroupBox4.Controls[i]).Text := '';

   for i := 0 to GroupBox5.ControlCount - 1 do
      if GroupBox5.Controls[i] is TEdit then
         TEdit(GroupBox5.Controls[i]).Text := '';

   // ListBoxSendRTMsg.Items.Clear;

   // 三筆歷史資料隱藏
   btnHistCancel1.Visible := false;
   for i := 0 to 2 do
      TPanel(PanelHist.Controls[i]).Visible := false;

   // editPhone.SetFocus;
   // 清除地圖圖標
   try
      HTMLWindow2.execScript('ClearMarkers()', 'JavaScript');
   except
      on E: Exception do
         Self.ShowAddMessage(StrToIntDef(ExtNo.Caption, 0),
             '呼叫地圖出現錯誤：' + E.Message);
   end;
end;

procedure TfrmSendCar.showPosition; // 把地圖的範圍拉到二個圖標的大小
// var
// ilat1, ilng1, ilat2, ilng2, ite: double;
begin
   // 取得上下左右座標
   // W-140529 1.Table extbuffer 不再使用，改用 memory。
   { ilat1 := strtofloatdef(editstartlat.Text, 0);
     ilng1 := strtofloatdef(editstartlong.Text, 0);
     ilat2 := strtofloatdef(editendlat.Text, 0);
     ilng2 := strtofloatdef(editendlong.Text, 0);
   }{ ilat1 := strtofloatdef(cds_ExtBuffer_ebFrLat.asstring, 0);
     ilng1 := strtofloatdef(cds_ExtBuffer_ebFrLng.asstring, 0);

     if (cds_ExtBuffer_ebDvLat.asstring = '') or
     (cds_ExtBuffer_ebDvLat.asstring = '0') then
     begin
     ilat2 := strtofloatdef(cds_ExtBuffer_ebToLat.asstring, 0);
     ilng2 := strtofloatdef(cds_ExtBuffer_ebToLng.asstring, 0);
     end
     else
     begin
     ilat2 := cds_ExtBuffer_ebDvLat.AsFloat;
     ilng2 := cds_ExtBuffer_ebDvLng.AsFloat;
     end;
     // 標定左上角與右下角
     if ilat1 < ilat2 then
     begin
     ite := ilat2;
     ilat2 := ilat1;
     ilat1 := ite;
     end;

     if ilng1 < ilng2 then
     begin
     ite := ilng2;
     ilng2 := ilng1;
     ilng1 := ite;
     end;
   } try
      // Google服務
      // HTMLWindow2.execScript(Format('zoomToSpan(%s,%s,%s,%s)', [FloatToStr(ilat2),
      // FloatToStr(ilng2), FloatToStr(ilat1), FloatToStr(ilng1)]), 'JavaScript');
      HTMLWindow2.execScript('setCenter();', 'JavaScript');
   except
      on E: Exception do
         Self.ShowAddMessage(StrToIntDef(ExtNo.Caption, 0),
             '呼叫地圖出現錯誤：' + E.Message);
   end;

end;

// Google服務取得兩地行車時間
procedure TfrmSendCar.CalculateDistances(slng1, slat1, slng2, slat2: string;
    var iTimes, idistant: Integer);
var
   i: Integer;
   stime, sDist: string;
begin
   iTimes := 0;
   idistant := 0;
   if ((strtofloatdef(slng1, 0) = 0) or (strtofloatdef(slat1, 0) = 0)) then
      exit;

   try
      try
         screen.Cursor := crHourGlass;
         WebBrowser1.OleObject.Document.script.Btime := '';
         WebBrowser1.OleObject.Document.script.Btimevalue := 0;

         HTMLWindow2.execScript(Format('calculateDistances(%s,%s,%s,%s)',
             [slng1, slat1, slng2, slat2]), 'JavaScript');
         i := 0;

         while i <= 100 do begin
            if WebBrowser1.OleObject.Document.script.Btime <> '' then
               Break;
            if i = 50 then
               HTMLWindow2.execScript(Format('calculateDistances(%s,%s,%s,%s)',
                   [slng1, slat1, slng2, slat2]), 'JavaScript');

            inc(i);
            Sleep(200);
            application.ProcessMessages;
         end;

         if (WebBrowser1.OleObject.Document.script.Btimevalue = 0) then begin
            ShowMessageDlag('查無資料，無法計算!');
            // exit;
         end;

         stime := WebBrowser1.OleObject.Document.script.Btime;
         sDist := WebBrowser1.OleObject.Document.script.Bdistant;
         iTimes := WebBrowser1.OleObject.Document.script.Btimevalue;
         idistant := WebBrowser1.OleObject.Document.script.Bdistantvalue;

         // Memo1.Lines.Add('乘車時間:' + Btime + '--' + FloatToStr(itimes));
         // Memo1.Lines.Add('途經距離:' + Bdis + '--' + FloatToStr(idistant));
         { tt := '';
           for i := 1 to Length(Btime) do
           begin
           if Btime[i] = ' ' then Break;
           tt := tt + Btime[i];
           end;
           gtime := strtofloatdef(tt, 0);
           tt := '';
           for i := 1 to Length(Bdis) do
           begin
           if Bdis[i] = ' ' then Break;
           tt := tt + Bdis[i];
           end;
           gdis := strtofloatdef(tt, 0); }

      finally
         screen.Cursor := crDefault;
      end;
   except
      on E: Exception do begin
         Self.ShowAddMessage(StrToIntDef(ExtNo.Caption, 0),
             '計算行車時間錯誤：' + E.Message);
         ShowMessageDlag('錯誤!無法計算!');
      end;
   end;
end;

procedure TfrmSendCar.cds_ExtBuffer_AfterInsert(DataSet: TDataSet);
begin
   cds_ExtBuffer_ebTaxi.AsInteger := 1;
   cds_ExtBuffer_ebOkQty.AsInteger := 0;
   cds_ExtBuffer_ebCxnoList.asstring := '';
   cds_ExtBuffer_ebPaiche.AsBoolean := True;
   cds_ExtBuffer_ebInOut.asstring := 'I';
   cds_ExtBuffer_ebFinish.AsBoolean := false;
   cds_ExtBuffer_ebSex.asstring := '1';
   cds_ExtBuffer_ebdtpgneed.AsDateTime := now;
   cds_ExtBuffer_ebdatatodrv.AsInteger := 1;

   if MotorCade.mcticketcity = '1' then
      cds_ExtBuffer_ebFrLoc.asstring := MotorCade.smccity;
end;

procedure TfrmSendCar.cds_ExtBuffer_AfterScroll(DataSet: TDataSet);
var
   li_h, li_m, li_s, li_ms: Word;
   ld_need: TDateTime;
begin
   if AnsiSameText(cds_ExtBuffer_ebInOut.asstring, 'I') then
      RadioButton1.Checked := True
   else
      RadioButton2.Checked := True;

   btnSendCar.Enabled := cds_ExtBuffer_ebPaiche.AsBoolean;
   // btnFinish.Enabled := cds_ExtBuffer_ebFinish.AsBoolean;

   if cds_ExtBuffer_ebdtpgneed.IsNull then
      ld_need := now
   else
      ld_need := cds_ExtBuffer_ebdtpgneed.AsDateTime;

   DateTimePicker1.Date := ld_need;
   DecodeTime(ld_need, li_h, li_m, li_s, li_ms);

   if li_h <= 12 then
      ComboBox1.Text := IntToStr(li_h)
   else
      ComboBox1.Text := IntToStr(li_h - 12);

   ComboBox2.Text := IntToStr(li_m);

   if li_h < 12 then
      RadioButton3.Checked := True
   else
      RadioButton4.Checked := True;
end;

// W-140530 3．	按 “派车”如果无车可派，“派车”还要ON
procedure TfrmSendCar.cds_ExtBuffer_ebFinishValidate(Sender: TField);
begin
   // btnFinish.Enabled := cds_ExtBuffer_ebFinish.AsBoolean;
end;

procedure TfrmSendCar.cds_ExtBuffer_ebFrLocValidate(Sender: TField);
begin
   // if not(cds_ExtBuffer_.State in [dsInsert, dsEdit]) then
   // cds_ExtBuffer_.Edit;

   // cds_ExtBuffer_ebFrLng.asstring := '';
   // cds_ExtBuffer_ebFrLat.asstring := '';
   // cds_ExtBuffer_ebFrAddr.asstring := '';
end;

procedure TfrmSendCar.cds_ExtBuffer_ebPaicheValidate(Sender: TField);
begin
   btnSendCar.Enabled := cds_ExtBuffer_ebPaiche.AsBoolean;

   if btnSendCar.Enabled then begin
      editStartPlace.ReadOnly := false;
      editEndPlace.ReadOnly := false;
   end
   else begin
      editStartPlace.ReadOnly := True;
      editEndPlace.ReadOnly := True;
   end;
end;

procedure TfrmSendCar.cds_ExtBuffer_ebToLocValidate(Sender: TField);
begin
   // if not(cds_ExtBuffer_.State in [dsInsert, dsEdit]) then
   // cds_ExtBuffer_.Edit;

   cds_ExtBuffer_ebToLng.asstring := '';
   cds_ExtBuffer_ebToLat.asstring := '';
   // cds_ExtBuffer_ebToAddr.asstring := '';
end;

procedure TfrmSendCar.writeout(as_AsNo: String = '');
var
   iRecord, i: Integer; // , j
   sTemp: string;
   // iIndex: Integer;
   // NewItem: TMenuItem;
begin
   // W-140529 1.Table extbuffer 不再使用，改用 memory。
   // 檢查乘客資料表
   Self.Panel14.Visible := false;
   with dm1 do begin
      qyPsgr.Close;
      qyPsgr.SQL.clear;
      qyPsgr.SQL.Text := 'select * from psgr where mcid=' +
          IntToStr(MotorCade.iID) +
      // W-140529 1.Table extbuffer 不再使用，改用 memory。
      // ' and pgphone = '+quotedstr(editPhone.text);
          ' and pgphone = ' + quotedstr(cds_ExtBuffer_ebPhone.asstring) +
          ' and asno = ' + quotedstr(as_AsNo);
      qyPsgr.open;

      for i := 1 to 3 do
         PanelHist.FindChildControl('PnlHist' + IntToStr(i)).Visible := false;

      if not qyPsgr.Eof then begin
         Label38.Visible := false;
         ColorButton2.Enabled := True;
         EditPgid.Text := qyPsgr.fieldbyname('pgid').asstring;
         // W-140529 1.Table extbuffer 不再使用，改用 memory。
         if not(cds_ExtBuffer_.State in [dsInsert, dsEdit]) then
            cds_ExtBuffer_.Edit;
         // editName.Text  :=qyPsgr.fieldbyname('pgnm').asstring;
         cds_ExtBuffer_ebPsgrName.asstring :=
             qyPsgr.fieldbyname('pgnm').asstring;
         // 性別 pggender tinyint comment '0:女士；1：先生' jieshu 14/6/16
         if not qyPsgr.fieldbyname('pggender').IsNull then
            cds_ExtBuffer_ebSex.AsInteger := qyPsgr.fieldbyname('pggender')
                .AsInteger;
         // 給司機
         if not qyPsgr.fieldbyname('pgDataToDrv').IsNull then
            cds_ExtBuffer_ebdatatodrv.AsInteger :=
                qyPsgr.fieldbyname('pgDataToDrv').AsInteger;
         /// ////////////////////////////////////////////////////////////////////////////////
         /// 子表
         if as_AsNo = '' then begin
            qypsgr_locbk.Close;
            qypsgr_locbk.SQL.Text :=
                'select a.pgnm, b.lbloc, b.lblocref, b.lbquid, c.qunm, b.lblong, b.lblat, b.pgid'
                + '  from psgr a join psgr_locbk b on a.pgid = b.pgid' +
                '  left join queue c on a.mcid = c.mcid and b.lbquid =c.quid' +
                ' where a.mcid = ' + IntToStr(MotorCade.iID) +
                '   and a.pgphone = ' +
                quotedstr(cds_ExtBuffer_ebPhone.asstring) +
                ' order by lbdtlast desc limit 1';
            // qypsgr_locbk.SQL.Text := 'select *' + '  from psgr_locbk where pgid = '
            // + EditPgid.Text + ' order by lbdtlast desc';
            qypsgr_locbk.open;
            // 關聯表先防呆
            if qypsgr_locbk.fieldbyname('pgid').asstring = EditPgid.Text then
            begin
               iRecord := qypsgr_locbk.recordcount;
               if iRecord < 1 then begin
                  // 客戶的 locbk 只有一筆以下，隱藏“常用地點”鍵。
                  // sbtCommon1.Enabled := false;
                  sbtCommon2.Enabled := false;

                  if not(cds_ExtBuffer_.State in [dsInsert, dsEdit]) then
                     cds_ExtBuffer_.Edit;

               end
               else // iRecord >1
               begin
                  if iRecord = 1 then begin
                     cds_ExtBuffer_ebFrLoc.asstring :=
                         qypsgr_locbk.fieldbyname('lbloc').asstring;
                     cds_ExtBuffer_ebFrLocRef.asstring :=
                         qypsgr_locbk.fieldbyname('lblocref').asstring;
                     cds_ExtBuffer_ebFrLng.asstring :=
                         qypsgr_locbk.fieldbyname('lblong').asstring;
                     cds_ExtBuffer_ebFrLat.asstring :=
                         qypsgr_locbk.fieldbyname('lblat').asstring;
                     RadioButton6.Checked := True;

                     if (qypsgr_locbk.fieldbyname('lbquid').asstring = '0') or
                         (qypsgr_locbk.fieldbyname('lbquid').asstring = '') then
                     begin
                        Label45.Caption := '排班站：無';
                        Label45.Font.Color := clBlack;
                        ComboBox4.ItemIndex := 0;
                     end
                     else begin
                        Label45.Font.Color := clred;
                        ComboBox4.ItemIndex :=
                            gt_quid.IndexOf(qypsgr_locbk.fieldbyname('lbquid')
                            .asstring);
                        Label45.Caption := '排班站：' + ComboBox4.Text;
                     end;

                     editLatitudeE.Text :=
                         formatfloat('0.00000',
                         strtofloat(cds_ExtBuffer_ebFrLat.asstring));
                     editLongitudeE.Text :=
                         formatfloat('0.00000',
                         strtofloat(cds_ExtBuffer_ebFrLng.asstring));
                     Label65.Caption := '經度：' + editLongitudeE.Text + '，緯度：' +
                         editLatitudeE.Text;
                     Self.Panel14.Visible := True;
                  end;
                  // sbtCommon1.Enabled := True;
                  sbtCommon2.Enabled := True;
               end; // if iRecord <=1  then  else
            end; // if qypsgr_locbk.fieldbyname('pgid').asstring=qyPsgr.fieldbyname('pgid').asstring then

            /// /////////////////////////////////////////////////////////////////////////////////////////
            // 取出歷史資料
            if trim(EditPgid.Text) = '' then // 沒乘客資料
               exit;

            // 列出 該電話最近的三筆派車成功/失敗 （不包含取消的） calltx
            // 如果最近的一筆，乘客還未接到，則出現紅色 X，按下去可以取消該派車。（VTA，司機APP 要配合修改）
            // 如果最近的一筆，乘客還未接到(CXSTATUS=0,CXDT<>NULL,CXPICKUP = NULL)，則出現紅色 X，按下去可以取消該派車。
            qyCalltxHist.Close;
            qyCalltxHist.SQL.clear;
            // DATE_FORMAT(cxdtcreate,'+quotedstr('%m/%d %H:%i')+') AS CDT
            // jieshu 14/6/7 最近三筆資料修正 改用電話
            // ' where pgid =' + EditPgid.Text + ' and mcid =' +
            qyCalltxHist.SQL.Text := 'SELECT * FROM calltx ' + ' where mcid =' +
                IntToStr(MotorCade.iID) + ' and cxpgPhone = ' +
                quotedstr(cds_ExtBuffer_ebPhone.asstring) + ' and asno = ''''' +
                ' and (cxsuccess=0 or cxsuccess=1) ' +
                ' order by cxdtcreate desc limit 3';

            qyCalltxHist.open;
            i := 1;

            while not qyCalltxHist.Eof do begin
               sTemp := formatdatetime('mm/dd hh:nn',
                   qyCalltxHist.fieldbyname('cxdtcreate').AsDateTime) + ' ' +
                   qyCalltxHist.fieldbyname('cxfrloc').asstring + '，司機 ' +
                   qyCalltxHist.fieldbyname('dvno').asstring + '，';

               if qyCalltxHist.fieldbyname('cxdtpickup').IsNull then
                  sTemp := sTemp + '未載客'
               else
                  sTemp := sTemp + '載客 ' + formatdatetime('hh:nn',
                      qyCalltxHist.fieldbyname('cxdtpickup').AsDateTime);

               if qyCalltxHist.fieldbyname('cxfrlocref').asstring <> '' then
                  sTemp := sTemp + '(' + qyCalltxHist.fieldbyname('cxfrlocref')
                      .asstring + ')';

               case i of // 用FindComponent找不到
                  1: begin
                        PnlHist1.Visible := True;
                        LabelHist1.Caption := sTemp;
                        btnHist1.Hint := qyCalltxHist.fieldbyname
                            ('cxno').asstring;
                        btnHistCancel1.Hint :=
                            qyCalltxHist.fieldbyname('cxno').asstring;

                        if (qyCalltxHist.fieldbyname('dvno').asstring <> '') and
                            (qyCalltxHist.fieldbyname('cxdtpickup').IsNull) and
                            not qyCalltxHist.fieldbyname('cxdtconfirm').IsNull
                        then
                           btnHistCancel1.Visible := True
                        else
                           btnHistCancel1.Visible := false;
                     end;
                  2: begin
                        PnlHist2.Visible := True;
                        LabelHist2.Caption := sTemp;
                        btnHist2.Hint := qyCalltxHist.fieldbyname
                            ('cxno').asstring;
                        btnHistCancel2.Hint :=
                            qyCalltxHist.fieldbyname('cxno').asstring;

                        if (qyCalltxHist.fieldbyname('dvno').asstring <> '') and
                            (qyCalltxHist.fieldbyname('cxdtpickup').IsNull) and
                            not qyCalltxHist.fieldbyname('cxdtconfirm').IsNull
                        then
                           btnHistCancel2.Visible := True
                        else
                           btnHistCancel2.Visible := false;
                     end;
                  3: begin
                        PnlHist3.Visible := True;
                        LabelHist3.Caption := sTemp;
                        btnHist3.Hint := qyCalltxHist.fieldbyname
                            ('cxno').asstring;
                        btnHistCancel3.Hint :=
                            qyCalltxHist.fieldbyname('cxno').asstring;

                        if (qyCalltxHist.fieldbyname('dvno').asstring <> '') and
                            (qyCalltxHist.fieldbyname('cxdtpickup').IsNull) and
                            not qyCalltxHist.fieldbyname('cxdtconfirm').IsNull
                        then
                           btnHistCancel3.Visible := True
                        else
                           btnHistCancel3.Visible := false;
                     end;
               end;
               inc(i);
               qyCalltxHist.next;
            end;
            qyCalltxHist.first;
         end;
      end // if qyPsgr.locate('pgphone',cds_ExtBuffer_ebPhone.AsString,[]) then
      else begin
         Label38.Visible := True;
         ColorButton2.Enabled := false;
      end;

   end; // with dm1 do
end;

procedure TfrmSendCar.ZoomToTwoPlaceBtnClick(Sender: TObject);
var
   aStream: TMemoryStream;
begin
   if cds_ExtBuffer_ebDvid.AsInteger <> 0 then begin
      dm1.qyDrv.Close;
      dm1.qyDrv.SQL.Text := 'select dvlong, dvlat from drv' + ' where dvid = ' +
          cds_ExtBuffer_ebDvid.asstring;
      dm1.qyDrv.open();
      cds_ExtBuffer_.ReadOnly := false;
      application.ProcessMessages;
      try
         cds_ExtBuffer_.Edit;
         cds_ExtBuffer_ebDvLat.AsFloat :=
             dm1.qyDrv.fieldbyname('dvlat').AsFloat;
         cds_ExtBuffer_ebDvLng.AsFloat :=
             dm1.qyDrv.fieldbyname('dvlong').AsFloat;
         // cds_ExtBuffer_.Post;
      finally
         if AnsiPos(ExtNo.Caption, '已派,再派') > 0 then
            cds_ExtBuffer_.ReadOnly := True;
      end;
   end;

   gb_Finish := True;
   // 清除圖標
   if Self.Panel14.Visible then begin
      Self.Panel14.Visible := false;

      if not Assigned(HTMLWindow2) then begin
         if Assigned(WebBrowser1.Document) then begin
            gb_WebOK := false;
            aStream := TMemoryStream.Create;
            try
               aStream.WriteBuffer(Pointer(HTMLStr)^, Length(HTMLStr));
               aStream.Seek(0, soFromBeginning);
               (WebBrowser1.Document as IPersistStreamInit)
                   .Load(TStreamAdapter.Create(aStream));
            finally
               aStream.Free;
            end;
         end;
         HTMLWindow2 := (WebBrowser1.Document as IHTMLDocument2).parentWindow;
      end;

      Self.TimerReFireShowDriverAndCustomer.Enabled := True;
      exit;
   end;

   HTMLWindow2.execScript('ClearMarkers()', 'JavaScript');

   // jieshu 14/6/30 增加司機
   // cds_ExtBuffer_ebDvLat.asstring := '22.65216';
   // cds_ExtBuffer_ebDvLng.asstring := '120.28458';
   if (cds_ExtBuffer_ebDvLat.asstring <> '0') and
       (cds_ExtBuffer_ebDvLng.asstring <> '') then
      HTMLWindow2.execScript(Format('PutMarkerIco(%s,%s,%s,%d,%s)',
          [cds_ExtBuffer_ebDvLat.asstring, cds_ExtBuffer_ebDvLng.asstring,
          quotedstr('司機點'), 2, quotedstr(sImagePath)]), 'JavaScript');

   // 如果有出發座標標出圖標
   // W-140529 1.Table extbuffer 不再使用，改用 memory。
   // if (strtofloatdef(editstartlat.Text, 0) = 0 ) or (strtofloatdef(editstartlong.Text, 0) = 0)then
   if (strtofloatdef(cds_ExtBuffer_ebFrLat.asstring, 0) <> 0) or
       (strtofloatdef(cds_ExtBuffer_ebFrLng.asstring, 0) <> 0) then
      HTMLWindow2.execScript(Format('PutMarkerIco(%s,%s,%s,%d,%s)',
          // W-140529 1.Table extbuffer 不再使用，改用 memory。
          // [editstartlat.Text, editstartlong.Text, QuotedStr('上車點'), 6,QuotedStr(sImagePath)]),'JavaScript');
          [cds_ExtBuffer_ebFrLat.asstring, cds_ExtBuffer_ebFrLng.asstring,
          quotedstr('上車點'), 6, quotedstr(sImagePath)]), 'JavaScript');

   if (ParamStr(1) = '已派') and (dm1.qyCalltx.fieldbyname('cxcfmlong').AsFloat
       <> 0) then
      HTMLWindow2.execScript(Format('PutMarkerIco(%s,%s,%s,%d,%s)',
          [dm1.qyCalltx.fieldbyname('cxcfmlat').asstring,
          dm1.qyCalltx.fieldbyname('cxcfmlong').asstring, quotedstr('司機接單'), 3,
          quotedstr(sImagePath)]), 'JavaScript');

   if (ParamStr(1) = '已派') and (dm1.qyCalltx.fieldbyname('cxinlong').AsFloat
       <> 0) then
      HTMLWindow2.execScript(Format('PutMarkerIco(%s,%s,%s,%d,%s)',
          [dm1.qyCalltx.fieldbyname('cxinlat').asstring,
          dm1.qyCalltx.fieldbyname('cxinlong').asstring, quotedstr('到達定點'), 4,
          quotedstr(sImagePath)]), 'JavaScript');

   if (ParamStr(1) = '已派') and (dm1.qyCalltx.fieldbyname('cxonlong').AsFloat
       <> 0) then
      HTMLWindow2.execScript(Format('PutMarkerIco(%s,%s,%s,%d,%s)',
          [dm1.qyCalltx.fieldbyname('cxonlat').asstring,
          dm1.qyCalltx.fieldbyname('cxonlong').asstring, quotedstr('接到乘客'), 5,
          quotedstr(sImagePath)]), 'JavaScript');

   if (ParamStr(1) = '已派') and (dm1.qyCalltx.fieldbyname('cxoflong').AsFloat
       <> 0) then
      HTMLWindow2.execScript(Format('PutMarkerIco(%s,%s,%s,%d,%s)',
          [dm1.qyCalltx.fieldbyname('cxoflat').asstring,
          dm1.qyCalltx.fieldbyname('cxoflong').asstring, quotedstr('完成服務'), 8,
          quotedstr(sImagePath)]), 'JavaScript');

   // 如果有目的座標標出圖標
   // W-140529 1.Table extbuffer 不再使用，改用 memory。
   // if (strtofloatdef(editendlat.Text, 0) = 0) or (strtofloatdef(editendlong.Text, 0) = 0 ) then
   if (strtofloatdef(cds_ExtBuffer_ebToLat.asstring, 0) <> 0) and
       (strtofloatdef(cds_ExtBuffer_ebToLng.asstring, 0) <> 0) then
      // W-140529 1.Table extbuffer 不再使用，改用 memory。
      // HTMLWindow2.execScript(Format('PutMarkerIco(%s,%s,%s,%d,%s)',[editendlat.Text, editendlong.Text, QuotedStr('下車點'), 7,QuotedStr(sImagePath)]),
      HTMLWindow2.execScript(Format('PutMarkerIco(%s,%s,%s,%d,%s)',
          [cds_ExtBuffer_ebToLat.asstring, cds_ExtBuffer_ebToLng.asstring,
          quotedstr('下車點'), 7, quotedstr(sImagePath)]), 'JavaScript');

   showPosition;
end;

procedure TfrmSendCar.writein(var at_SSD: TSocketSendData; quid: String;
    as_AsNo: String = ''; ab_calltx: Boolean = True);
var
   i: Integer;
   reqCount: Integer;

begin
   //
   // 處理乘客資料
   // 1.同一車隊有沒相同電話號碼
   // dm1.zconMySQL.Disconnect;
   with dm1.qy1 do begin
      Close;
      SQL.clear;
      // W-140529 1.Table extbuffer 不再使用，改用 memory。
      // sql.Text:='select pgid from psgr where pgphone='+quotedstr(editphone.text)+
      SQL.Text := 'select pgid from psgr where mcid = ' +
          IntToStr(MotorCade.iID) + ' and pgphone = ' + quotedstr(at_SSD.sPhone)
          + ' and asno = ' + quotedstr(as_AsNo);
      open;
      if Fields[0].asstring = '' then begin
         EditPgid.Text := '';
         Close;
         SQL.clear;
         SQL.Text :=
             'INSERT INTO PSGR(PGDTCREATE,MCID,PGNM,PGPHONE,pggender,pgdatatodrv) VALUES('
             + 'SYSDATE(),' + IntToStr(MotorCade.iID) + ',' +
             quotedstr(at_SSD.sName) + ',' + quotedstr(at_SSD.sPhone) + ',' +
             at_SSD.sSex + ',' + IntToStr(at_SSD.iDataToDrv) + ')';
         try
            EXECSQL;
            // 取出pgid
            Close;
            SQL.clear;
            SQL.Text := 'select last_insert_id() ';
            open;
            EditPgid.Text := Fields[0].asstring;
            at_SSD.ipgid := Fields[0].AsInteger;
            Close;
         except
            on E: Exception do begin
               Self.ShowAddMessage(at_SSD.iEXTNO, '新增PSGR錯誤：' + E.Message);
               MessageDlg(E.Message, mtError, [mbOK], 0);
               exit;
            end;
         end;
      end
      else // if fields[0].AsString = '' then
      begin
         EditPgid.Text := Fields[0].asstring;
         Close;
         SQL.clear;
         SQL.Text := 'Update PSGR set PGNM=' + quotedstr(at_SSD.sName) +
         // pggender tinyint comment '0:女士；1：先生' jieshu 14/6/16
             ', pggender = ' + at_SSD.sSex + ', pgdatatodrv = ' +
             IntToStr(at_SSD.iDataToDrv) + ' WHERE PGID=' + EditPgid.Text;
         try
            EXECSQL;
         except
            on E: Exception do begin
               Self.ShowAddMessage(at_SSD.iEXTNO, '更新PSGR錯誤：' + E.Message);
               MessageDlg(E.Message, mtError, [mbOK], 0);
               exit;
            end;
         end;
      end;

   end; // with dm1.qy1 do
   // 3. 計算乘客出發地至目的地的時間 派車共用搬來這裡
   if (at_SSD.icxfrlng <> 0) and (at_SSD.icxfrlat <> 0) and
       (at_SSD.icxtolng <> 0) and (at_SSD.icxtolat <> 0) then
      CalculateDistances(FloatToStr(at_SSD.icxfrlng),
          FloatToStr(at_SSD.icxfrlat), FloatToStr(at_SSD.icxtolng),
          FloatToStr(at_SSD.icxtolat), iRouteTimes, iRoutedist);

   at_SSD.icxgotime := iRouteTimes;
   at_SSD.icxgodis := iRoutedist;
   /// /////////////////////////////////////////////////////////////////////////////////////////////////////////////
   // 2.先處理上下車常用地址
   dm1.gp_Set_psgr_locbk(at_SSD.ipgid, strtoint(quid), at_SSD.scxfrPlace, '',
       at_SSD.scxfrPS, at_SSD.icxfrlng, at_SSD.icxfrlat);

   /// /////////////////////////////////////////////////////////////////////////////////////////////////////////////
   // 3.寫入派車表(派車單)
   if ab_calltx then begin
      cxnoList.clear;
      for reqCount := 0 to strtoint(Self.cmbTAXI_NUM.Text) - 1 do begin
         with dm1.qyCalltx do begin
            open;
            append;
            fieldbyname('pgid').asstring := EditPgid.Text;
            fieldbyname('cxfrloc').asstring := at_SSD.scxfrPlace;
            fieldbyname('cxdtcreate').AsDateTime := now;
            // fieldbyname('cxfraddr').asstring := at_SSD.scxfraddr;
            fieldbyname('cxfrlocref').asstring := at_SSD.scxfrPS;
            fieldbyname('cxfrlong').AsFloat := at_SSD.icxfrlng;
            fieldbyname('cxfrlat').AsFloat := at_SSD.icxfrlat;
            fieldbyname('cxtoloc').asstring := at_SSD.scxtoPlace;
            // fieldbyname('cxtoaddr').asstring := at_SSD.scxtoaddr;
            fieldbyname('cxtolocref').asstring := at_SSD.scxtoPS;
            fieldbyname('cxtolong').AsFloat := at_SSD.icxtolng;
            fieldbyname('cxtolat').AsFloat := at_SSD.icxtolat;
            fieldbyname('csid').asstring := LoginUser.sUserID;
            fieldbyname('mcid').AsInteger := MotorCade.iID;
            // CSR2 screen layout jieshu 14/6/2 未完工
            fieldbyname('cxpgphone').asstring := at_SSD.sPhone;
            fieldbyname('cxflagon').AsInteger := 0;
            fieldbyname('quid').asstring := quid;

            if at_SSD.sDvno <> '' then begin
               fieldbyname('dvno').asstring := at_SSD.sDvno;
               fieldbyname('dvid').AsInteger := at_SSD.idvid;
               fieldbyname('cxtimetopickup').AsInteger := at_SSD.itimetopickup;
            end;

            for i := 0 to high(ls_FieldCx) do
               if TCheckBox(GroupBox4.FindChildControl('cbFlag' + IntToStr(i))).Checked
               then begin
                  fieldbyname('cxflagon').AsInteger := 1;
                  fieldbyname(ls_FieldCx[i]).asstring := 'Y';
               end
               else
                  fieldbyname(ls_FieldCx[i]).asstring := 'N';

            if at_SSD.idcno <> 0 then
               fieldbyname('dcno').AsInteger := at_SSD.idcno;

            if ParamStr(1) = '再派' then begin
               fieldbyname('cxoutlineno').AsInteger := 0;
               fieldbyname('cxsuccess').AsInteger := 1;
               fieldbyname('rcno').asstring := ParamStr(3);
               fieldbyname('asno').asstring := as_AsNo;
            end
            else
               fieldbyname('cxoutlineno').AsInteger := at_SSD.iEXTNO;
            // fieldbyname('cxsuccess').Asinteger:=0;

            { close;sql.clear;
              sql.text:='INSERT INTO calltx '+
              '(pgid,cxdtcreate,cxfrloc,cxfraddr,cxfrlocref,cxfrlong,cxfrlat,cxtoloc,cxtoaddr,cxtolocref,cxtolong,cxtolat,csid,mcid,cxsuccess) VALUES('+editpgid.Text+',sysdate(),'+
              quotedstr(cds_ExtBuffer_ebFrLoc.AsString)+','+quotedstr(cds_ExtBuffer_ebFrAddr.AsString)+','+quotedstr(cds_ExtBuffer_ebFrLocRef.AsString)+','+quotedstr(cds_ExtBuffer_ebFrLng.AsString)+','+quotedstr(cds_ExtBuffer_ebFrLat.AsString)+','+
              quotedstr(cds_ExtBuffer_ebToLoc.AsString)+','+quotedstr(cds_ExtBuffer_ebToAddr.AsString)+','+quotedstr(cds_ExtBuffer_ebToLocRef.AsString)+','+quotedstr(cds_ExtBuffer_ebToLng.AsString)+','+quotedstr(cds_ExtBuffer_ebToLat.AsString)+','+
              quotedstr(LoginUser.sUserID)+','+inttostr(motorcade.iID)+',0 )'; }

            try
               Post;

               with dm1.qy1 do begin
                  Close;
                  SQL.clear;
                  SQL.Text := 'select last_insert_id() ';
                  open;
                  Editcxno.Text := IntToStr(Fields[0].AsInteger);
                  cxnoList.Add(IntToStr(Fields[0].AsInteger));
                  at_SSD.icxno := Fields[0].AsInteger;
                  Close;
               end;
               Close;
               open;
            except
               on E: Exception do
                  Self.ShowAddMessage(at_SSD.iEXTNO, '新增Calltx錯誤：' + E.Message);
            end;
         end;
      end;

      WriteUsrtx(at_SSD.iMotorCadeID, LoginUser.sUserID,
          '一般派車，車輛數:' + cmbTAXI_NUM.Text, '客戶電話:' + at_SSD.sPhone,
          '上車地點:' + at_SSD.scxfrPlace, '');
   end;
end;

function TfrmSendCar.WriteUsrtx(mcid: Integer; usid, uxid, uxfield, uxnewvalue,
    uxoldvalue: String): Boolean;
var
   asql: String;
begin
   asql := 'Insert into usrtx(mcid,usid,uxdt,uxid,uxfield,uxnewvalue,uxoldvalue) '
       + 'values(' + IntToStr(mcid) + ',' + quotedstr(usid) + ',Now()' + ',' +
       quotedstr(uxid) + ',' + quotedstr(uxfield) + ',' + quotedstr(uxnewvalue)
       + ',' + quotedstr(uxoldvalue) + ')';
   dm1.gp_ExecuteSQL(asql);
end;

procedure TfrmSendCar.updatecalltx(iStatus, icxno, icxsuc: Integer);
begin
   with dm1.qyCalltx do begin
      try
         if iStatus = 1 then // 新增狀態須重開
            Close;

         open;
         if locate('cxno', icxno, []) then begin
            Edit;
            fieldbyname('cxsuccess').AsInteger := icxsuc;
            Post;
         end;
      except
         on E: Exception do begin
            Self.ShowAddMessage(StrToIntDef(ExtNo.Caption, 0),
                '派車單狀態更新錯誤：' + E.Message);
            ShowMessageDlag('派車單狀態更新失敗!');
         end;
      end;

   end;
end;

procedure TfrmSendCar.DateTimePicker1Change(Sender: TObject);
var
   li_h: Word;
begin
   li_h := StrToIntDef(ComboBox1.Text, 0);

   if RadioButton4.Checked and (li_h < 12) then
      li_h := li_h + 12;

   cds_ExtBuffer_.Edit;
   try
      cds_ExtBuffer_ebdtpgneed.AsDateTime :=
          StrToDateTime(formatdatetime('yyyy/mm/dd', DateTimePicker1.Date) + ' '
          + IntToStr(li_h) + ':' + ComboBox2.Text);
      // cds_ExtBuffer_ebFrLocRef.AsString
      // := '預約' + FormatDateTime('hh點nn分', cds_ExtBuffer_ebdtPgNeed.AsDateTime) +  '用車';
   except
      on E: Exception do begin
         Self.ShowAddMessage(StrToIntDef(ExtNo.Caption, 0),
             '日期格式錯誤：' + E.Message);
         gf_ShowMsg('日期格式錯誤：' + E.Message, 'CSR', mtWarning, 0, 128, Self);
      end;
   end;
end;

procedure TfrmSendCar.DBRadioGroup1Change(Sender: TObject);
begin
   if cds_ExtBuffer_ebPsgrName.asstring = '' then
      exit;
   // jieshu 14/6/15 稱謂加到姓名
   if (pos('先生', cds_ExtBuffer_ebPsgrName.asstring) < 1) and
       (pos('女士', cds_ExtBuffer_ebPsgrName.asstring) < 1) and
       (DBRadioGroup1.ItemIndex >= 0) then begin
      cds_ExtBuffer_.Edit;
      cds_ExtBuffer_ebPsgrName.asstring := cds_ExtBuffer_ebPsgrName.asstring +
          DBRadioGroup1.Items[DBRadioGroup1.ItemIndex];
   end;
end;

procedure TfrmSendCar.DeleteBuffer(iEXTNO: Integer); // 各分機暫存檔刪除
var
   li_i: Integer;
begin
   { with dm1.qyExtBuffer do
     begin
     if locate('ebEXTNO',iEXTNO,[]) then
     delete;
     end;
   }
   if cds_ExtBuffer_.locate('ebExtNo', iEXTNO, []) then begin
      cds_ExtBuffer_.Edit;

      for li_i := 0 to cds_ExtBuffer_.FieldCount - 1 do
         if cds_ExtBuffer_.Fields[li_i] is TNumericField then
            cds_ExtBuffer_.Fields[li_i].AsInteger := 0
         else if cds_ExtBuffer_.Fields[li_i] is TBooleanField then
            cds_ExtBuffer_.Fields[li_i].AsBoolean := True
         else
            cds_ExtBuffer_.Fields[li_i].asstring := '';

      cds_ExtBuffer_AfterInsert(cds_ExtBuffer_);
   end
   else
      cds_ExtBuffer_.append;

   cds_ExtBuffer_ebExtNo.AsInteger := iEXTNO;
end;

procedure TfrmSendCar.WebBrowser1BeforeNavigate2(ASender: TObject;
    const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData,
    Headers: OleVariant; var Cancel: WordBool);
var
   latlng: String;
   lat, lng: string;
begin
   if pos('dragend://', URL) > 0 then begin
      // ShowMessage(URL);
      latlng := URL;
      Fetch(latlng, '?');
      lat := Fetch(latlng, '&');
      lng := latlng;

      Fetch(lat, '=');
      Fetch(lng, '=');

      cds_ExtBuffer_ebFrLat.asstring := lat;
      cds_ExtBuffer_ebFrLng.asstring := lng;

      editLatitudeE.Text := formatfloat('0.00000',
          strtofloat(cds_ExtBuffer_ebFrLat.asstring));
      editLongitudeE.Text := formatfloat('0.00000',
          strtofloat(cds_ExtBuffer_ebFrLng.asstring));
      Label65.Caption := '經度：' + editLongitudeE.Text + '，緯度：' +
          editLatitudeE.Text;

      Cancel := True;
   end;
end;

procedure TfrmSendCar.WebBrowser1CommandStateChange(ASender: TObject;
    Command: Integer; Enable: WordBool); // WebBrowser控件的狀態事件監聽
var
   ADocument: IHTMLDocument2;
   ABody: IHTMLElement2;
   slat, slng, stempStr: string;
   bneedtoRePaint: Boolean;
   // iLookTime: Integer;
   // rlat,rlng:real;
   /// /////////////////////////////////////////////////////////////////////////////////////////////
   function GetIdValue(const id: string): string;
   var
      Tag: IHTMLElement;
      TagsList: IHTMLElementCollection;
      iIndex: Integer;
   begin
      Result := '';
      TagsList := ABody.getElementsByTagName('input');
      for iIndex := 0 to TagsList.Length - 1 do begin
         Tag := TagsList.item(iIndex, EmptyParam) as IHTMLElement;
         if CompareText(Tag.id, id) = 0 then
            Result := Tag.getAttribute('value', 0);
      end;
   end;
/// /////////////////////////////////////////////////////////////////////////////////////////////
   function SearchAddressbyGoogle(frlat, frlong: string): string;
   var
      tt: Integer;
      // checkStr: string;
   begin
      // WebBrowser1.OleObject.Document.script.Aaddresss := '';
      Result := '';
      HTMLWindow2.execScript(Format('codeLatLngAddredd(%s,%s)', [frlat, frlong]
          ), 'JavaScript');
      tt := 0;
      while Result = '' do begin // 使用連續判斷地址是否己經更新方式去檢查Google服務是否己經完成
         if tt > 25 then // 等待25秒
         begin
            Result := '找不到該地址';
            exit; // 等待時間太長還是要退出的
         end;
         tt := tt + 1;
         Sleep(1000); // Sleep
         application.ProcessMessages; // 避免顯示停沚
         Result := WebBrowser1.OleObject.Document.script.Aaddresss;
      end;
      /// /////////////////////////////////////////////////////////////////////////////////////////////
   end;

begin
   bneedtoRePaint := false;
   if TOleEnum(Command) <> CSC_UPDATECOMMANDS then
      exit;

   ADocument := WebBrowser1.Document as IHTMLDocument2;
   if not Assigned(ADocument) then
      exit;

   if not Supports(ADocument.body, IHTMLElement2, ABody) then
      exit;
   // 取得點擊位置的座標
   slat := GetIdValue('LatValue');
   slng := GetIdValue('LngValue');

   // 由Google服務取得點擊位置的地址---由於Google服務的時間差要重覆要求
   if (slat <> '') and (slng <> '') and
       ((slat <> editLatitudeE.Text) or (slng <> editLongitudeE.Text)) and
       (WebBrowser1.OleObject.Document.script.haveClick = 1) then begin
      editLatitudeE.Text := formatfloat('0.00000', strtofloat(slat));
      editLongitudeE.Text := formatfloat('0.00000', strtofloat(slng));
      Label65.Caption := '經度：' + editLongitudeE.Text + '，緯度：' +
          editLatitudeE.Text;
      Panel13.Caption := '已經有經緯度';
      // 出發地點的RadioButton 及 Google地圖被點擊
      // W-140529 1.Table extbuffer 不再使用，改用 memory。
      // if RBfrom.Checked and (WebBrowser1.OleObject.Document.script.haveClick = 1) then
      WebBrowser1.OleObject.Document.script.haveClick := 0;
      // 座標轉成字串
      if not(cds_ExtBuffer_.State in [dsInsert, dsEdit]) then
         cds_ExtBuffer_.Edit;

      if AnsiSameText(cds_ExtBuffer_ebInOut.asstring, 'I') then begin
         cds_ExtBuffer_ebFrLng.asstring := editLongitudeE.Text;
         cds_ExtBuffer_ebFrLat.asstring := editLatitudeE.Text;
      end
      else begin // 目的地點的RadioButton 及 Google地圖被點擊
         cds_ExtBuffer_ebToLat.asstring := editLatitudeE.Text;
         cds_ExtBuffer_ebToLng.asstring := editLongitudeE.Text;
      end;

      bneedtoRePaint := True;
      screen.Cursor := crHourGlass;
      try
         application.ProcessMessages;
         HTMLWindow2.execScript(Format('codeLatLngAddredd(%s,%s)', [slat, slng]
             ), 'JavaScript');
         Sleep(500);
         application.ProcessMessages;
         stempStr := SearchAddressbyGoogle(slat, slng);

         if stempStr = '' then
            application.MessageBox(PChar(stempStr), PChar(Self.Caption), MB_OK)
         else begin
            if not(cds_ExtBuffer_.State in [dsInsert, dsEdit]) then
               cds_ExtBuffer_.Edit;
            // cds_ExtBuffer_ebFrAddr.asstring := proccessAddressChose(stempStr);
            // jieshu 14/6/3 地點 = 地址
            if AnsiSameText(cds_ExtBuffer_ebInOut.asstring, 'I') then begin
               cds_ExtBuffer_ebFrLoc.OnValidate := nil;
               try
                  cds_ExtBuffer_ebFrLoc.asstring :=
                      proccessAddressChose(stempStr);
               finally
                  cds_ExtBuffer_ebFrLoc.OnValidate :=
                      cds_ExtBuffer_ebFrLocValidate;
               end;
            end
            else begin
               cds_ExtBuffer_ebToLoc.OnValidate := nil;
               try
                  cds_ExtBuffer_ebToLoc.asstring :=
                      proccessAddressChose(stempStr);
               finally
                  cds_ExtBuffer_ebToLoc.OnValidate :=
                      cds_ExtBuffer_ebToLocValidate;
               end;
            end;
         end;
      finally
         screen.Cursor := crDefault;
      end;
   end;
   // WebBrowser1.OleObject.Document.script.Aaddresss := '';

   // 有成功的新位置點擊時要重畫圖標
   if bneedtoRePaint = True then begin
      HTMLWindow2.execScript('ClearMarkers()', 'JavaScript');
      if ((strtofloatdef(cds_ExtBuffer_ebFrLat.asstring, 0) <> 0) and
          (strtofloatdef(cds_ExtBuffer_ebFrLng.asstring, 0) <> 0)) then begin
         HTMLWindow2.execScript(Format('PutMarkerIco(%s,%s,%s,%d,%s)',
             [cds_ExtBuffer_ebFrLat.asstring, cds_ExtBuffer_ebFrLng.asstring,
             quotedstr('上車點'), 6, quotedstr(sImagePath)]), 'JavaScript');
      end;
      if ((strtofloatdef(cds_ExtBuffer_ebToLat.asstring, 0) <> 0) and
          (strtofloatdef(cds_ExtBuffer_ebToLng.asstring, 0) <> 0)) then begin
         HTMLWindow2.execScript(Format('PutMarkerIco(%s,%s,%s,%d,%s)',
             [cds_ExtBuffer_ebToLat.asstring, cds_ExtBuffer_ebToLng.asstring,
             quotedstr('下車點'), 7, quotedstr(sImagePath)]), 'JavaScript');
      end;

      // WebBrowser1.Destroy;
   end;
end;

procedure TfrmSendCar.WebBrowser1DocumentComplete(ASender: TObject;
    const pDisp: IDispatch; const URL: OleVariant);
begin
   gb_WebOK := True;
end;

procedure TfrmSendCar.WebBrowser1ShowScriptError(ASender: TObject;
    const AErrorLine, AErrorCharacter, AErrorMessage, AErrorCode,
    AErrorUrl: OleVariant; var AOut: OleVariant; var AHandled: Boolean);
begin
   if LowerCase(AErrorMessage) = 'script error' then
      AHandled := True;
   // ScriptErrorAction := eaContinue;
end;

procedure TfrmSendCar.ColorButton1Click(Sender: TObject);
var
   sdvid, quid: string;
   ipos: Integer;
   sTemp: unicodestring;
begin
   cds_ExtBuffer_.ReadOnly := false;
   try
      if RadioGroup1.ItemIndex = 1 then begin
         if gf_ShowMsg('確定無車可派？', 'CSR', mtConfirmation, 1, 128, Self) = mrNo
         then
            exit
         else begin // 無車可派
            dm1.q_Recall.Edit;
            dm1.q_Recall.fieldbyname('rcresult').AsInteger := 1;
            dm1.q_Recall.fieldbyname('rccsidexec').asstring :=
                LoginUser.sUserID;
            dm1.q_Recall.fieldbyname('rcdtfinish').AsDateTime := now;
            dm1.q_Recall.Post;
            Close;
         end;
      end
      else begin
         if StrToIntDef(ComboBox7.Text, -1) = -1 then begin
            gf_ShowMsg('分鐘數不是有效數字。', 'CSR', mtWarning, 0, 128, Self);
            exit;
         end;

         ipos := pos(' ', cmbDrvNO.Text) - 1;

         if ipos > 0 then
            sTemp := Copy(cmbDrvNO.Text, 1, ipos)
         else
            sTemp := cmbDrvNO.Text;

         dm1.qyDrv.Close;
         dm1.qyDrv.open;

         if dm1.qyDrv.locate('dvno', sTemp, []) then
            sdvid := dm1.qyDrv.fieldbyname('dvid').asstring
         else
            sdvid := '';

         if sdvid = '' then begin
            gf_ShowMsg('查無司機台號！(' + sTemp + ')', 'CSR', mtWarning, 0,
                128, Self);
            exit;
         end;

         writeout(dm1.q_Recall.fieldbyname('asno').asstring); // 不查歷史、常用地址
         SocketSendData.sLoginID := LoginUser.sUserID;
         SocketSendData.sPassword := LoginUser.sPassword;
         SocketSendData.iMotorCadeID := MotorCade.iID;
         SocketSendData.iEXTNO := StrToIntDef(ExtNo.Caption, 1);
         SocketSendData.ipgid := StrToIntDef(EditPgid.Text, 0);
         SocketSendData.sName := cds_ExtBuffer_ebPsgrName.asstring;
         SocketSendData.sPhone := cds_ExtBuffer_ebPhone.asstring;
         SocketSendData.scxface := '';
         SocketSendData.scxfrPlace := cds_ExtBuffer_ebFrLoc.asstring;
         // SocketSendData.scxfraddr := cds_ExtBuffer_ebFrAddr.asstring;
         SocketSendData.scxfrPS := cds_ExtBuffer_ebFrLocRef.asstring;
         SocketSendData.icxfrlng := strtofloat(cds_ExtBuffer_ebFrLng.asstring);
         SocketSendData.icxfrlat := strtofloat(cds_ExtBuffer_ebFrLat.asstring);
         SocketSendData.scxtoPlace := cds_ExtBuffer_ebToLoc.asstring;
         // SocketSendData.scxtoaddr := cds_ExtBuffer_ebToAddr.asstring;
         SocketSendData.scxtoPS := cds_ExtBuffer_ebToLocRef.asstring;
         SocketSendData.icxtolng :=
             strtofloatdef(cds_ExtBuffer_ebToLng.asstring, 0);
         SocketSendData.icxtolat :=
             strtofloatdef(cds_ExtBuffer_ebToLat.asstring, 0);
         // jieshu 14/6/17 共用派車用
         SocketSendData.sSex := cds_ExtBuffer_ebSex.asstring;
         SocketSendData.iDataToDrv := cds_ExtBuffer_ebdatatodrv.AsInteger;

         if ComboBox8.ItemIndex < 1 then begin
            SocketSendData.sQunm := '';
            SocketSendData.iQupid := 0;
            quid := '0';
            SocketSendData.iquid := 0;
         end
         else begin
            SocketSendData.sQunm := ComboBox8.Items[ComboBox8.ItemIndex];
            SocketSendData.iQupid := strtoint(gt_Queue[ComboBox8.ItemIndex]);
            quid := gt_quid[ComboBox8.ItemIndex];
            SocketSendData.iquid := strtoint(quid);
         end;

         cds_ExtBuffer_ebPaiche.AsBoolean := false;
         SocketSendData.sDvno := sTemp;
         SocketSendData.idvid := StrToIntDef(sdvid, 0);
         SocketSendData.itimetopickup := StrToIntDef(ComboBox7.Text, 0);
         // 1.寫入table (生成派車單)
         writein(SocketSendData, quid, dm1.q_Recall.fieldbyname('asno')
             .asstring);

         gp_UpdateCsrClick(IntToStr(SocketSendData.icxno));

         dm1.q_Recall.Edit;
         dm1.q_Recall.fieldbyname('cxno').AsInteger := SocketSendData.icxno;
         dm1.q_Recall.fieldbyname('dvno').asstring := sTemp;
         dm1.q_Recall.fieldbyname('cxtimetopickup').asstring := ComboBox7.Text;
         dm1.q_Recall.fieldbyname('rcresult').AsInteger := 1;
         dm1.q_Recall.fieldbyname('rccsidexec').asstring := LoginUser.sUserID;
         dm1.q_Recall.fieldbyname('rcdtfinish').AsDateTime := now;
         dm1.q_Recall.Post;
         Close;
      end;
   finally
      cds_ExtBuffer_.ReadOnly := True;
   end;
end;

procedure TfrmSendCar.ColorButton2Click(Sender: TObject);
begin
   if cds_ExtBuffer_.fieldbyname('ebPhone').asstring = '' then begin
      gf_ShowMsg('請輸入電話號碼');
      exit;
   end;

   dm1.zqTemp.Close;
   dm1.zqTemp.SQL.Text := 'select mcid from psgr where mcid = ' +
       IntToStr(MotorCade.iID) + '   and pgphone = ' +
       quotedstr(cds_ExtBuffer_.fieldbyname('ebPhone').asstring);
   dm1.zqTemp.open();

   if dm1.zqTemp.IsEmpty then begin
      gf_ShowMsg('新電話不能設定黑名單');
      exit;
   end;

   fBlock.Label1.Caption := '客戶電話 ' + cds_ExtBuffer_.fieldbyname('ebPhone')
       .asstring + ' 黑名單';
   fBlock.ShowModal;
end;

procedure TfrmSendCar.ColorButton3Click(Sender: TObject);
var
   i: Integer;
   // for saving HTML document.
   // PersistStream: IPersistStreamInit;
   // Stream: IStream;
   // FileStream: TFileStream;
begin
   // if not Assigned(Self.WebBrowser1.Document) then begin
   // ShowMessage('Document not loaded!');
   // exit;
   // end;
   //
   // PersistStream := WebBrowser1.Document as IPersistStreamInit;
   // FileStream := TFileStream.Create('d:\test.htm', fmCreate);
   // try
   // Stream := TStreamAdapter.Create(FileStream, soReference) as IStream;
   // if Failed(PersistStream.Save(Stream, True)) then
   // ShowMessage('SaveAs HTML fail!');
   // finally
   // FileStream.Free;
   // end;
   // exit;
   dm1.zqTemp.Close;
   dm1.zqTemp.SQL.Text := 'select count(*) from drv where mcid = ' +
       IntToStr(MotorCade.iID) + ' and dvdtreport >= Now() - 500 ';

   for i := 0 to high(ls_Field) do
      if TCheckBox(GroupBox4.FindChildControl('cbFlag' + IntToStr(i))).Checked
      then
         dm1.zqTemp.SQL.Text := dm1.zqTemp.SQL.Text + '   and ' + ls_Field[i] +
             ' = ''Y''';

   dm1.zqTemp.open();
   Label63.Caption := '全部： ' + dm1.zqTemp.Fields[0].asstring + ' 部';
   dm1.zqTemp.Close;
   dm1.zqTemp.SQL.Text := dm1.zqTemp.SQL.Text + ' and dvstatus = 1';
   dm1.zqTemp.open();
   ValidCarCount := dm1.zqTemp.Fields[0].AsInteger;
   Label64.Caption := '空車： ' + dm1.zqTemp.Fields[0].asstring + ' 部';
end;

procedure TfrmSendCar.ColorButton4Click(Sender: TObject);
begin
   // 檢查輸入欄位
   if (cds_ExtBuffer_ebPhone.asstring = '') then begin
      // W-140530 2．	CSR 按 “派车”bug：已有电话，却说没电话。另外，这个讯息框里，字太小。
      if editPhone.Text = '' then begin
         ShowMessageDlag('請輸入乘客電話!');
         exit;
      end
      else begin
         cds_ExtBuffer_.Edit;
         cds_ExtBuffer_ebPhone.asstring := editPhone.Text;
      end;
   end;
   Self.ShowAddMessage(0, '執行更多派車紀錄。');
   fPaiche.ShowModal;
end;

procedure TfrmSendCar.ColorButton5Click(Sender: TObject);
begin
   gp_ValidateInput;
   gp_CheckPhone;
   DateTimePicker1Change(Sender);
   cds_ExtBuffer_.Edit;
   // 檢查輸入欄位
   if (cds_ExtBuffer_ebPhone.asstring = '') then begin
      // W-140530 2．	CSR 按 “派车”bug：已有电话，却说没电话。另外，这个讯息框里，字太小。
      if editPhone.Text = '' then begin
         ShowMessageDlag('請輸入乘客電話!');
         exit;
      end
      else
         cds_ExtBuffer_ebPhone.asstring := editPhone.Text;
   end;

   if (cds_ExtBuffer_ebFrLoc.asstring = '') then begin
      ShowMessageDlag('請輸入上車位置!');
      exit;
   end;

   if (cds_ExtBuffer_ebFrLng.asstring = '') then begin
      ShowMessageDlag('沒有坐標,請先按定位!');
      exit;
   end;

   { if (cds_ExtBuffer_ebFrAddr.asstring = '') then
     begin
     ShowMessageDlag('沒有地址,請再確認!');
     Exit;
     end;
   }
   if cds_ExtBuffer_ebdtpgneed.AsDateTime <= now then begin
      ShowMessageDlag('乘客用車時間 ' + formatdatetime('mm/dd hh:nn',
          cds_ExtBuffer_ebdtpgneed.AsDateTime) + '，現在時間 ' +
          formatdatetime('mm/dd hh:nn', now) + ', 用車時間早於現在時間');
      exit;
   end;
   { if cds_ExtBuffer_ebFrLng.AsString = '' then
     btnStartMapClick(Sender);

     if (cds_ExtBuffer_ebToLng.AsString = '') and (cds_ExtBuffer_ebToLoc.AsString <> '') then
     btnEndMapClick(Sender);
   }
   dm1.zqTemp.Close;
   dm1.zqTemp.SQL.Text := 'update defercall' + '   set dccsidcreate = ' +
       quotedstr(LoginUser.sUserID) + '     , dcdtcreate = :a1' +
       '     , dcoutlineno = ' + cds_ExtBuffer_ebExtNo.asstring +
       '     , dcpgname = :a0' + '     , dcfrloc = ' +
       quotedstr(cds_ExtBuffer_ebFrLoc.asstring) +
   // '     , dcfraddr = ' + quotedstr(cds_ExtBuffer_ebFrAddr.asstring) +
       '     , dcfrlocref = ' + quotedstr(cds_ExtBuffer_ebFrLocRef.asstring) +
       '     , dcfrlong = ' + cds_ExtBuffer_ebFrLng.asstring +
       '     , dcfrlat = ' + cds_ExtBuffer_ebFrLat.asstring +
       '     , dctoloc = :a3' +
   // '     , dctoaddr = :a4' +
       '     , dctolocref = :a5' + '     , dctolong = :a6' +
       '     , dctolat = :a7' + '     , dcpggender = :a8' + '     , dcpremin = '
       + ComboBox3.Text + ' where mcid = ' + IntToStr(MotorCade.iID) +
       '   and dcpgphone = ' + quotedstr(cds_ExtBuffer_ebPhone.asstring) +
       '   and dcdtpgneed = :a2';
   dm1.zqTemp.ParamByName('a1').AsDateTime := now;
   dm1.zqTemp.ParamByName('a0').asstring := cds_ExtBuffer_ebPsgrName.asstring;
   dm1.zqTemp.ParamByName('a2').AsDateTime :=
       cds_ExtBuffer_ebdtpgneed.AsDateTime;
   dm1.zqTemp.ParamByName('a3').Value := cds_ExtBuffer_ebToLoc.Value;
   // dm1.zqTemp.ParamByName('a4').Value := cds_ExtBuffer_ebToAddr.Value;
   dm1.zqTemp.ParamByName('a5').Value := cds_ExtBuffer_ebToLocRef.Value;
   dm1.zqTemp.ParamByName('a8').Value := cds_ExtBuffer_ebSex.Value;

   if cds_ExtBuffer_ebToLng.asstring = '' then
      dm1.zqTemp.ParamByName('a6').Value := null
   else
      dm1.zqTemp.ParamByName('a6').Value := cds_ExtBuffer_ebToLng.Value;

   if cds_ExtBuffer_ebToLat.asstring = '' then
      dm1.zqTemp.ParamByName('a7').Value := null
   else
      dm1.zqTemp.ParamByName('a7').Value := cds_ExtBuffer_ebToLat.Value;

   dm1.zqTemp.ParamByName('a6').DataType := ftFloat;
   dm1.zqTemp.ParamByName('a7').DataType := ftFloat;
   ColorButton5.Enabled := false;
   screen.Cursor := crHourGlass;
   try
      try
         dm1.zqTemp.EXECSQL;

         if dm1.zqTemp.RowsAffected = 0 then // 更新不到
         begin
            dm1.zqTemp.Close;
            dm1.zqTemp.SQL.Text :=
                'insert into defercall(mcid, dccsidcreate, dcdtcreate, dcoutlineno, dcpgphone,'
                + '    dcpgname, dcdtpgneed, dcfrloc, dcfrlocref, dcfrlong, dcfrlat,'
                + '    dctoloc, dctolocref, dctolong, dctolat, dcpggender, dcpremin)'
                + '  Values(' + IntToStr(MotorCade.iID) + ', ' +
                quotedstr(LoginUser.sUserID) + ', :a1, ' +
                cds_ExtBuffer_ebExtNo.asstring + ', ' +
                quotedstr(cds_ExtBuffer_ebPhone.asstring) + ', :a0, :a2, ' +
                quotedstr(cds_ExtBuffer_ebFrLoc.asstring) + ', ' +
            // quotedstr(cds_ExtBuffer_ebFrAddr.asstring) + ', ' +
                quotedstr(cds_ExtBuffer_ebFrLocRef.asstring) + ', ' +
                cds_ExtBuffer_ebFrLng.asstring + ', ' +
                cds_ExtBuffer_ebFrLat.asstring + ', :a3, :a5, :a6, :a7, :a8, ' +
                ComboBox3.Text + ')';
            dm1.zqTemp.ParamByName('a1').AsDateTime := now;
            dm1.zqTemp.ParamByName('a0').asstring :=
                cds_ExtBuffer_ebPsgrName.asstring;
            dm1.zqTemp.ParamByName('a2').AsDateTime :=
                cds_ExtBuffer_ebdtpgneed.AsDateTime;
            dm1.zqTemp.ParamByName('a3').Value := cds_ExtBuffer_ebToLoc.Value;
            // dm1.zqTemp.ParamByName('a4').Value := cds_ExtBuffer_ebToAddr.Value;
            dm1.zqTemp.ParamByName('a5').Value :=
                cds_ExtBuffer_ebToLocRef.Value;
            dm1.zqTemp.ParamByName('a8').Value := cds_ExtBuffer_ebSex.Value;

            if cds_ExtBuffer_ebToLng.asstring = '' then
               dm1.zqTemp.ParamByName('a6').Value := null
            else
               dm1.zqTemp.ParamByName('a6').Value :=
                   cds_ExtBuffer_ebToLng.Value;

            if cds_ExtBuffer_ebToLat.asstring = '' then
               dm1.zqTemp.ParamByName('a7').Value := null
            else
               dm1.zqTemp.ParamByName('a7').Value :=
                   cds_ExtBuffer_ebToLat.Value;

            dm1.zqTemp.EXECSQL;
         end;

         cds_ExtBuffer_ebDcResult.asstring := cds_ExtBuffer_ebDcResult.asstring
             + '乘客姓名：' + cds_ExtBuffer_ebPsgrName.asstring + '，電話：' +
             cds_ExtBuffer_ebPhone.asstring + '，時間：' +
             formatdatetime('yyyy/mm/dd hh:nn',
             cds_ExtBuffer_ebdtpgneed.AsDateTime) + ' 用車，預約成功';
         ShowAddMessage(StrToIntDef(ExtNo.Caption, 0),
             '乘客姓名：' + cds_ExtBuffer_ebPsgrName.asstring + '，電話：' +
             cds_ExtBuffer_ebPhone.asstring + '，時間：' +
             formatdatetime('yyyy/mm/dd hh:nn',
             cds_ExtBuffer_ebdtpgneed.AsDateTime) + ' 用車，預約成功');
      except
         on E: Exception do begin
            cds_ExtBuffer_.Edit;
            cds_ExtBuffer_ebDcResult.asstring :=
                cds_ExtBuffer_ebDcResult.asstring + '新增預約出現錯誤：' + E.Message;
            ShowAddMessage(StrToIntDef(ExtNo.Caption, 0),
                '新增預約出現錯誤：' + E.Message);
            gf_ShowMsg('新增預約出現錯誤：' + E.Message, 'CSR', mtWarning, 0, 128, Self);
         end;
      end;
   finally
      ColorButton5.Enabled := True;
      screen.Cursor := crDefault; // 遊標復原
   end;
end;

procedure TfrmSendCar.ColorButton6Click(Sender: TObject);
// var
// s: string;
begin
   Self.ColorButton6.Enabled := false;
   try
      gp_CheckPhone;
      ColorButton6.Tag := 1; // 表有按過了
      if cds_ExtBuffer_ebPhone.asstring = '' then
         exit;
      writeout;
   finally
      Self.ColorButton6.Enabled := True;
   end;
end;

procedure TfrmSendCar.ColorButton8Click(Sender: TObject);
var
   i, idx: Integer;
   reqCount, theQueueCount: Integer;
   at_SSD: TSocketSendData;
   quid, datetime: String;
   as_AsNo, drvno, theQueueID, theQueueName, temp: String;
   ab_calltx: Boolean;
   carCount: Integer;
   resultIdx: Integer;
begin
   for carCount := 1 to manualFillInCarCount do begin
      case carCount of
         1: begin
               if (Self.ComboBox16.ItemIndex = -1) and
                   (Self.ComboBox18.ItemIndex > 0) then begin
                  ShowMessageDlag('請選擇司機台號');
                  exit;
               end;
            end;
         2: begin
               if (Self.ComboBox19.ItemIndex = -1) and
                   (Self.ComboBox23.ItemIndex > 0) then begin
                  ShowMessageDlag('請選擇司機台號');
                  exit;
               end;
            end;
         3: begin
               if (Self.ComboBox20.ItemIndex = -1) and
                   (Self.ComboBox24.ItemIndex > 0) then begin
                  ShowMessageDlag('請選擇司機台號');
                  exit;
               end;
            end;
      end;

      if (cds_ExtBuffer_ebPhone.asstring = '') then begin
         // W-140530 2．	CSR 按 “派车”bug：已有电话，却说没电话。另外，这个讯息框里，字太小。
         if editPhone.Text = '' then begin
            ShowMessageDlag('請輸入乘客電話!');
            exit;
         end
         else begin
            cds_ExtBuffer_.Edit;
            cds_ExtBuffer_ebPhone.asstring := editPhone.Text;
         end;
      end;

      // cds_ExtBuffer_ebFrLng.asstring := Self.sURLLon;
      // cds_ExtBuffer_ebToLat.asstring := Self.sURLLat;
      if (cds_ExtBuffer_ebFrLng.asstring = '') or
          (cds_ExtBuffer_ebFrLat.asstring = '') then begin
         ShowMessageDlag('請先確認地點!');
         exit;
      end;

      as_AsNo := '';
      ab_calltx := True;

      at_SSD.sLoginID := LoginUser.sUserID;
      at_SSD.sPassword := LoginUser.sPassword;
      at_SSD.iMotorCadeID := MotorCade.iID;
      at_SSD.iEXTNO := StrToIntDef(ExtNo.Caption, 1);
      at_SSD.ipgid := StrToIntDef(EditPgid.Text, 0);
      at_SSD.sName := cds_ExtBuffer_ebPsgrName.asstring;
      at_SSD.sPhone := cds_ExtBuffer_ebPhone.asstring;
      at_SSD.scxface := '';
      at_SSD.scxfrPlace := cds_ExtBuffer_ebFrLoc.asstring;
      // at_SSD.scxfraddr := cds_ExtBuffer_ebFrAddr.asstring;
      at_SSD.scxfrPS := cds_ExtBuffer_ebFrLocRef.asstring;
      at_SSD.icxfrlng := strtofloat(cds_ExtBuffer_ebFrLng.asstring);
      at_SSD.icxfrlat := strtofloat(cds_ExtBuffer_ebFrLat.asstring);
      at_SSD.scxtoPlace := cds_ExtBuffer_ebToLoc.asstring;
      // at_SSD.scxtoaddr := cds_ExtBuffer_ebToAddr.asstring;
      at_SSD.scxtoPS := cds_ExtBuffer_ebToLocRef.asstring;
      at_SSD.icxtolng := strtofloatdef(cds_ExtBuffer_ebToLng.asstring, 0);
      at_SSD.icxtolat := strtofloatdef(cds_ExtBuffer_ebToLat.asstring, 0);
      // jieshu 14/6/17 共用派車用
      at_SSD.sSex := cds_ExtBuffer_ebSex.asstring;
      at_SSD.iDataToDrv := cds_ExtBuffer_ebdatatodrv.AsInteger;

      if Self.ComboBox15.ItemIndex > 0 then begin
         at_SSD.sQunm := ComboBox15.Items[ComboBox15.ItemIndex];
         at_SSD.iQupid := strtoint(gt_Queue[ComboBox15.ItemIndex + 1]);
         quid := gt_quid[ComboBox15.ItemIndex + 1];
         at_SSD.iquid := strtoint(quid);
      end
      else begin
         at_SSD.sQunm := '';
         at_SSD.iQupid := 0;
         quid := gt_quid[ComboBox15.ItemIndex + 1];
         at_SSD.iquid := 0;

         // call PSO.GetQueueId
         PSO := TPSO.Create;
         try
            PSO.Connection := dm1.FDConnection1;
            PSO.mcid := MotorCade.iID;
            try
               theQueueID := PSO.search_qterritory(at_SSD.iMotorCadeID,
                   at_SSD.icxfrlng, at_SSD.icxfrlat, theQueueName);
            except
               on E: Exception do
                  ShowMessageDlag('search_qterritory出現錯誤：' + E.Message);
            end;

            if theQueueID = '' then begin
               theQueueID := PSO.GetQueueID(at_SSD.iMotorCadeID,
                   at_SSD.icxfrlng, at_SSD.icxfrlat, theQueueName,
                   theQueueCount); // 找出最近的排班點
               temp := '『最近』';
            end;

            idx := gt_Queue.IndexOf(theQueueID);
            if idx > -1 then begin
               quid := gt_quid[idx];
               at_SSD.iquid := strtoint(quid);
            end;
            at_SSD.iQupid := strtoint(theQueueID);
            at_SSD.sQunm := theQueueName;
         finally
            PSO.Free;
         end;

      end;

      if cds_car_.Active then
         cds_car_.EmptyDataSet
      else
         cds_car_.CreateDataSet;

      //
      // 處理乘客資料
      // 1.同一車隊有沒相同電話號碼
      // dm1.zconMySQL.Disconnect;
      with dm1.qy1 do begin
         Close;
         SQL.clear;
         // W-140529 1.Table extbuffer 不再使用，改用 memory。
         // sql.Text:='select pgid from psgr where pgphone='+quotedstr(editphone.text)+
         SQL.Text := 'select pgid from psgr where mcid = ' +
             IntToStr(MotorCade.iID) + ' and pgphone = ' +
             quotedstr(at_SSD.sPhone) + ' and asno = ' + quotedstr(as_AsNo);
         open;
         if Fields[0].asstring = '' then begin
            EditPgid.Text := '';
            Close;
            SQL.clear;
            SQL.Text :=
                'INSERT INTO PSGR(PGDTCREATE,MCID,PGNM,PGPHONE,pggender,pgdatatodrv) VALUES('
                + 'SYSDATE(),' + IntToStr(MotorCade.iID) + ',' +
                quotedstr(at_SSD.sName) + ',' + quotedstr(at_SSD.sPhone) + ',' +
                at_SSD.sSex + ',' + IntToStr(at_SSD.iDataToDrv) + ')';
            try
               EXECSQL;
               // 取出pgid
               Close;
               SQL.clear;
               SQL.Text := 'select last_insert_id() ';
               open;
               EditPgid.Text := Fields[0].asstring;
               at_SSD.ipgid := Fields[0].AsInteger;
               Close;
            except
               on E: Exception do begin
                  Self.ShowAddMessage(at_SSD.iEXTNO, '新增PSGR錯誤：' + E.Message);
                  MessageDlg(E.Message, mtError, [mbOK], 0);
                  exit;
               end;
            end;
         end
         else // if fields[0].AsString = '' then
         begin
            EditPgid.Text := Fields[0].asstring;
            Close;
            SQL.clear;
            SQL.Text := 'Update PSGR set PGNM=' + quotedstr(at_SSD.sName) +
            // pggender tinyint comment '0:女士；1：先生' jieshu 14/6/16
                ', pggender = ' + at_SSD.sSex + ', pgdatatodrv = ' +
                IntToStr(at_SSD.iDataToDrv) + ' WHERE PGID=' + EditPgid.Text;
            try
               EXECSQL;
            except
               on E: Exception do begin
                  Self.ShowAddMessage(at_SSD.iEXTNO, '更新PSGR錯誤：' + E.Message);
                  MessageDlg(E.Message, mtError, [mbOK], 0);
                  exit;
               end;
            end;
         end;

      end; // with dm1.qy1 do
      // 3. 計算乘客出發地至目的地的時間 派車共用搬來這裡
      if (at_SSD.icxfrlng <> 0) and (at_SSD.icxfrlat <> 0) and
          (at_SSD.icxtolng <> 0) and (at_SSD.icxtolat <> 0) then
         CalculateDistances(FloatToStr(at_SSD.icxfrlng),
             FloatToStr(at_SSD.icxfrlat), FloatToStr(at_SSD.icxtolng),
             FloatToStr(at_SSD.icxtolat), iRouteTimes, iRoutedist);

      at_SSD.icxgotime := iRouteTimes;
      at_SSD.icxgodis := iRoutedist;
      /// /////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // 2.先處理上下車常用地址
      dm1.gp_Set_psgr_locbk(at_SSD.ipgid, strtoint(quid), at_SSD.scxfrPlace, '',
          at_SSD.scxfrPS, at_SSD.icxfrlng, at_SSD.icxfrlat);

      /// /////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // 3.寫入派車表(派車單)
      if ab_calltx then begin
         cxnoList.clear;
         // for reqCount := 0 to strtoint(Self.cmbTAXI_NUM.Text) - 1 do begin
         with dm1.qyCalltx do begin
            open;
            append;
            fieldbyname('pgid').asstring := EditPgid.Text;
            fieldbyname('cxfrloc').asstring := at_SSD.scxfrPlace;
            fieldbyname('cxdtcreate').AsDateTime := now;
            // fieldbyname('cxfraddr').asstring := at_SSD.scxfraddr;
            fieldbyname('cxfrlocref').asstring := at_SSD.scxfrPS;
            fieldbyname('cxfrlong').AsFloat := at_SSD.icxfrlng;
            fieldbyname('cxfrlat').AsFloat := at_SSD.icxfrlat;
            fieldbyname('cxtoloc').asstring := at_SSD.scxtoPlace;
            // fieldbyname('cxtoaddr').asstring := at_SSD.scxtoaddr;
            fieldbyname('cxtolocref').asstring := at_SSD.scxtoPS;
            fieldbyname('cxtolong').AsFloat := at_SSD.icxtolng;
            fieldbyname('cxtolat').AsFloat := at_SSD.icxtolat;
            fieldbyname('csid').asstring := LoginUser.sUserID;
            fieldbyname('mcid').AsInteger := MotorCade.iID;
            // CSR2 screen layout jieshu 14/6/2 未完工
            fieldbyname('cxpgphone').asstring := at_SSD.sPhone;
            fieldbyname('cxflagon').AsInteger := 0;
            fieldbyname('quid').asstring := quid;

            fieldbyname('cxdtcsrclick').AsDateTime := now;

            if at_SSD.sDvno <> '' then begin
               fieldbyname('dvno').asstring := at_SSD.sDvno;
               fieldbyname('dvid').AsInteger := at_SSD.idvid;
               fieldbyname('cxtimetopickup').AsInteger := at_SSD.itimetopickup;
            end;

            for i := 0 to high(ls_FieldCx) do
               if TCheckBox(GroupBox4.FindChildControl('cbFlag' + IntToStr(i))).Checked
               then begin
                  fieldbyname('cxflagon').AsInteger := 1;
                  fieldbyname(ls_FieldCx[i]).asstring := 'Y';
               end
               else
                  fieldbyname(ls_FieldCx[i]).asstring := 'N';

            if at_SSD.idcno <> 0 then
               fieldbyname('dcno').AsInteger := at_SSD.idcno;

            fieldbyname('cxoutlineno').AsInteger := at_SSD.iEXTNO;

            case carCount of
               1: begin
                     drvno := Self.ComboBox16.Text;
                     resultIdx := Self.ComboBox18.ItemIndex;
                     fieldbyname('cxtimetopickup').asstring :=
                         Self.ComboBox17.Text;
                  end;
               2: begin
                     drvno := Self.ComboBox19.Text;
                     resultIdx := Self.ComboBox23.ItemIndex;
                     fieldbyname('cxtimetopickup').asstring :=
                         Self.ComboBox21.Text;
                  end;
               3: begin
                     drvno := Self.ComboBox20.Text;
                     resultIdx := Self.ComboBox24.ItemIndex;
                     fieldbyname('cxtimetopickup').asstring :=
                         Self.ComboBox22.Text;
                  end;
            end;

            drvno := Fetch(drvno, ' ');
            fieldbyname('dvno').asstring := drvno;

            case resultIdx of
               0: begin
                     // 不成功, cxsucess = 0, dvno = @@@
                     fieldbyname('cxsuccess').AsInteger := 0;
                     // fieldbyname('dvno').asstring := '@@@';
                  end;
               1: begin
                     // 成功, cxsucess = 1, dvno 要有
                     fieldbyname('cxsuccess').AsInteger := 1;
                  end;
               2: begin
                     // 不見乘客, cxsucess = 9, dvno 要有
                     fieldbyname('cxsuccess').AsInteger := 9;
                  end;
            end;

            { close;sql.clear;
              sql.text:='INSERT INTO calltx '+
              '(pgid,cxdtcreate,cxfrloc,cxfraddr,cxfrlocref,cxfrlong,cxfrlat,cxtoloc,cxtoaddr,cxtolocref,cxtolong,cxtolat,csid,mcid,cxsuccess) VALUES('+editpgid.Text+',sysdate(),'+
              quotedstr(cds_ExtBuffer_ebFrLoc.AsString)+','+quotedstr(cds_ExtBuffer_ebFrAddr.AsString)+','+quotedstr(cds_ExtBuffer_ebFrLocRef.AsString)+','+quotedstr(cds_ExtBuffer_ebFrLng.AsString)+','+quotedstr(cds_ExtBuffer_ebFrLat.AsString)+','+
              quotedstr(cds_ExtBuffer_ebToLoc.AsString)+','+quotedstr(cds_ExtBuffer_ebToAddr.AsString)+','+quotedstr(cds_ExtBuffer_ebToLocRef.AsString)+','+quotedstr(cds_ExtBuffer_ebToLng.AsString)+','+quotedstr(cds_ExtBuffer_ebToLat.AsString)+','+
              quotedstr(LoginUser.sUserID)+','+inttostr(motorcade.iID)+',0 )'; }

            try
               Post;

               with dm1.qy1 do begin
                  Close;
                  SQL.clear;
                  SQL.Text := 'select last_insert_id() ';
                  open;
                  Editcxno.Text := IntToStr(Fields[0].AsInteger);
                  cxnoList.Add(IntToStr(Fields[0].AsInteger));
                  at_SSD.icxno := Fields[0].AsInteger;
                  Close;
               end;
               Close;
               open;

               ShowMessageDlag('新增人工補單完成');

               if carCount = Self.manualFillInCarCount then
                  application.Terminate;
            except
               on E: Exception do
                  Self.ShowAddMessage(at_SSD.iEXTNO, '新增Calltx錯誤：' + E.Message);
            end;
         end;
         // end;

         // WriteUsrtx(at_SSD.iMotorCadeID, LoginUser.sUserID,
         // '一般派車，車輛數:' + cmbTAXI_NUM.Text, '客戶電話:' + at_SSD.sPhone,
         // '上車地點:' + at_SSD.scxfrPlace, '');
      end;
   end;
end;

procedure TfrmSendCar.ColorButton9Click(Sender: TObject);
var
   theQueueID, theQueueName, temp, quid: string;
   theQueueCount, idx: Integer;
begin
   // 計算排班點
   // call PSO.GetQueueId
   if ((cds_ExtBuffer_ebFrLng.asstring = '') or
       (cds_ExtBuffer_ebFrLat.asstring = '')) then begin
      ShowMessageDlag('地址未填寫, 請查明後再行計算');
   end
   else begin
      PSO := TPSO.Create;
      try
         PSO.Connection := dm1.FDConnection1;
         PSO.mcid := MotorCade.iID;
         try
            theQueueID := PSO.search_qterritory(MotorCade.iID,
                strtofloat(cds_ExtBuffer_ebFrLng.asstring),
                strtofloat(cds_ExtBuffer_ebFrLat.asstring), theQueueName);
         except
            on E: Exception do
               ShowMessageDlag('search_qterritory出現錯誤：' + E.Message);
         end;

         if theQueueID = '' then begin
            theQueueID := PSO.GetQueueID(MotorCade.iID,
                strtofloat(cds_ExtBuffer_ebFrLng.asstring),
                strtofloat(cds_ExtBuffer_ebFrLat.asstring), theQueueName,
                theQueueCount); // 找出最近的排班點
            // temp := '『最近』';
         end;

         if theQueueID <> '' then begin
            idx := gt_Queue.IndexOf(theQueueID);
            if idx > -1 then begin
               quid := gt_quid[idx];
               // at_SSD.iquid := strtoint(quid);
            end;
            // at_SSD.iQupid := strtoint(theQueueID);
            Self.Label45.Caption := '排班站：' + theQueueName;
            ComboBox15.ItemIndex := ComboBox15.Items.IndexOf(theQueueName);
         end;
      finally
         PSO.Free;
      end;
   end;
end;

procedure TfrmSendCar.ComboBox10Change(Sender: TObject);
begin
   dm1.zqTemp.Close;
   dm1.zqTemp.SQL.Text := 'select a3 from addr3 where a1 = ' +
       quotedstr(ComboBox9.Text);

   if ComboBox10.Text <> '' then
      dm1.zqTemp.SQL.Text := dm1.zqTemp.SQL.Text + ' and a2 = ' +
          quotedstr(ComboBox10.Text);

   dm1.zqTemp.SQL.Text := dm1.zqTemp.SQL.Text + ' order by a3';
   dm1.zqTemp.open();
   ComboBox11.Items.clear;
   ComboBox11.Text := '';
   // ComboBox11.Items.Add('');

   while not dm1.zqTemp.Eof do begin
      ComboBox11.Items.Add(dm1.zqTemp.Fields[0].asstring);
      dm1.zqTemp.next;
   end;
end;

procedure TfrmSendCar.ComboBox10Exit(Sender: TObject);
begin
   gp_A2_P2N(ComboBox10, ComboBox9);
end;

procedure TfrmSendCar.ComboBox12Change(Sender: TObject);
begin
   dm1.zqTemp.Close;
   dm1.zqTemp.SQL.Text := 'select a2 from addr2x where mcid = ' +
       IntToStr(MotorCade.iID) + '   and a1 = ' + quotedstr(ComboBox12.Text) +
       ' order by pri';
   dm1.zqTemp.open();

   if dm1.zqTemp.recordcount = 0 then begin
      dm1.zqTemp.Close;
      dm1.zqTemp.SQL.Text := 'select a2 from addr2 where a1 = ' +
          quotedstr(ComboBox12.Text) + ' order by a2';
      dm1.zqTemp.open();
   end;

   ComboBox13.Items.clear;
   ComboBox14.Items.clear;
   ComboBox13.Text := '';
   ComboBox14.Text := '';
   ComboBox13.Items.Add('');

   while not dm1.zqTemp.Eof do begin
      ComboBox13.Items.Add(dm1.zqTemp.Fields[0].asstring);
      dm1.zqTemp.next;
   end;

   ComboBox13Change(ComboBox13);
end;

procedure TfrmSendCar.ComboBox12Enter(Sender: TObject);
begin
   RadioButton7.Checked := True;
end;

procedure TfrmSendCar.ComboBox13Change(Sender: TObject);
begin
   dm1.zqTemp.Close;
   dm1.zqTemp.SQL.Text := 'select a3 from addr3 where a1 = ' +
       quotedstr(ComboBox12.Text);

   if ComboBox13.Text <> '' then
      dm1.zqTemp.SQL.Text := dm1.zqTemp.SQL.Text + ' and a2 = ' +
          quotedstr(ComboBox13.Text);

   dm1.zqTemp.SQL.Text := dm1.zqTemp.SQL.Text + ' order by a3';
   dm1.zqTemp.open();
   ComboBox14.Items.clear;
   ComboBox14.Text := '';
   // ComboBox14.Items.Add('');

   while not dm1.zqTemp.Eof do begin
      ComboBox14.Items.Add(dm1.zqTemp.Fields[0].asstring);
      dm1.zqTemp.next;
   end;
end;

procedure TfrmSendCar.ComboBox13Exit(Sender: TObject);
begin
   gp_A2_P2N(ComboBox13, ComboBox12);
end;

procedure TfrmSendCar.ComboBox1KeyPress(Sender: TObject; var Key: Char);
begin
   if not CharInSet(Key, ['0' .. '9', Chr(VK_BACK)]) then begin
      Key := #0;
      Abort;
   end;
end;

procedure TfrmSendCar.ComboBox5Click(Sender: TObject);
var
   li_i: Integer;
begin
   li_i := editStartPlace.SelStart;
   cds_ExtBuffer_.Edit;
   cds_ExtBuffer_ebFrLoc.asstring := Copy(editStartPlace.Text, 1, li_i) +
       ComboBox5.Text + Copy(editStartPlace.Text, li_i + 1, MaxInt);
   editStartPlace.SelStart := li_i + Length(ComboBox5.Text);
end;

procedure TfrmSendCar.ComboBox5Enter(Sender: TObject);
begin
   gb_Focus := false;
   RadioButton6.Checked := True;
end;

procedure TfrmSendCar.ComboBox6Click(Sender: TObject);
var
   li_i: Integer;
begin
   li_i := editEndPlace.SelStart;
   cds_ExtBuffer_.Edit;
   cds_ExtBuffer_ebToLoc.asstring := Copy(editEndPlace.Text, 1, li_i) +
       ComboBox6.Text + Copy(editEndPlace.Text, li_i + 1, MaxInt);
   editEndPlace.SelStart := li_i + Length(ComboBox6.Text);
end;

procedure TfrmSendCar.ComboBox9Change(Sender: TObject);
begin
   dm1.zqTemp.Close;
   dm1.zqTemp.SQL.Text := 'select a2 from addr2x where mcid = ' +
       IntToStr(MotorCade.iID) + '   and a1 = ' + quotedstr(ComboBox9.Text) +
       ' order by pri';
   dm1.zqTemp.open();

   if dm1.zqTemp.recordcount = 0 then begin
      dm1.zqTemp.Close;
      dm1.zqTemp.SQL.Text := 'select a2 from addr2 where a1 = ' +
          quotedstr(ComboBox9.Text) + ' order by a2';
      dm1.zqTemp.open();
   end;

   ComboBox10.Items.clear;
   ComboBox11.Items.clear;
   ComboBox10.Text := '';
   ComboBox11.Text := '';
   ComboBox10.Items.Add('');

   while not dm1.zqTemp.Eof do begin
      ComboBox10.Items.Add(dm1.zqTemp.Fields[0].asstring);
      dm1.zqTemp.next;
   end;

   ComboBox10Change(ComboBox10);
end;

procedure TfrmSendCar.ComboBox9Enter(Sender: TObject);
begin
   RadioButton5.Checked := True;
end;

procedure TfrmSendCar.ComboBoxAdrListChange(Sender: TObject);
begin
   cds_ExtBuffer_.Edit;

   if cds_Addr_.locate('Baddress', ComboBoxAdrList.Text, []) then begin
      if RadioButton1.Checked then begin
         cds_ExtBuffer_ebFrLoc.asstring :=
             proccessAddressChose(cds_Addr_Baddress.asstring);
         cds_ExtBuffer_ebFrLat.asstring := cds_Addr_Blat.asstring;
         cds_ExtBuffer_ebFrLng.asstring := cds_Addr_Blng.asstring;
      end
      else begin
         cds_ExtBuffer_ebToLoc.asstring :=
             proccessAddressChose(cds_Addr_Baddress.asstring);
         cds_ExtBuffer_ebToLat.asstring := cds_Addr_Blat.asstring;
         cds_ExtBuffer_ebToLng.asstring := cds_Addr_Blng.asstring;
      end;

      ZoomToTwoPlaceBtnClick(ZoomToTwoPlaceBtn);
   end;
end;

procedure TfrmSendCar.ComboBoxAdrListClick(Sender: TObject);
begin
   { cds_ExtBuffer_.Edit;

     if RadioButton1.Checked then
     begin
     cds_ExtBuffer_ebFrLoc.asstring := cds_Addr_Baddress.asstring;
     cds_ExtBuffer_ebFrLat.asstring := cds_Addr_Blat.asstring;
     cds_ExtBuffer_ebFrLng.asstring := cds_Addr_Blng.asstring;
     end
     else
     begin
     cds_ExtBuffer_ebToLoc.asstring := cds_Addr_Baddress.asstring;
     cds_ExtBuffer_ebToLat.asstring := cds_Addr_Blat.asstring;
     cds_ExtBuffer_ebToLng.asstring := cds_Addr_Blng.asstring;
     end;
   } end;

procedure TfrmSendCar.ComboBoxAdrListCloseUp(Sender: TObject);
begin
   ComboBoxAdrList.Visible := false;
end;

procedure TfrmSendCar.CreatePhoneObject; // 建立電話物件
begin
end;

procedure TfrmSendCar.editEndPlaceExit(Sender: TObject);
begin
   preFindAddressByAddress(cds_ExtBuffer_ebToLoc.asstring);
   // JavaScript反饋時間太慢==先預找一次
   application.ProcessMessages;
   Sleep(100);
end;

procedure TfrmSendCar.editPhoneChange(Sender: TObject);
begin
   if editPhone.Text = '' then begin
      // sbtCommon1.Enabled := false;
      sbtCommon2.Enabled := false;
   end
   else begin
      // sbtCommon1.Enabled := True;
      sbtCommon2.Enabled := True;
   end;
end;

procedure TfrmSendCar.editPhoneEnter(Sender: TObject);
begin
   ColorButton6.Tag := 0; // 表確認鍵未按過了
end;

procedure TfrmSendCar.editPhoneExit(Sender: TObject);
begin
   if Self.ColorButton6.Tag = 0 then
      Self.ColorButton6Click(nil);
end;

procedure TfrmSendCar.editStartPlaceEnter(Sender: TObject);
begin
   if gb_NotEnter then
      exit;

   { if cds_ExtBuffer_ebPhone.asstring = '' then
     begin
     ShowMessageDlag('請先輸入上方電話欄位！');
     Abort;
     end;
   }
   if not(cds_ExtBuffer_.State in [dsInsert, dsEdit]) and
       (AnsiPos(ExtNo.Caption, '已派,再派') < 1) then
      cds_ExtBuffer_.Edit;

   // jieshu 14/6/15 地點點擊後，如果是空白，自動放入 mtrcd.mccity
   if Sender = editStartPlace then begin
      RadioButton1.Checked := True;
      RadioButton6.Checked := True;

      // if cds_ExtBuffer_ebFrLoc.asstring = '' then
      // begin
      editStartPlace.SelStart := Length(MotorCade.smccity);
      // keybd_event(VK_END, VK_END, 0, 0); // 改成按 Tab 鍵
      // end;
   end
   else begin
      RadioButton2.Checked := True;
      RadioButton8.Checked := True;
   end;
end;

procedure TfrmSendCar.editStartPlaceExit(Sender: TObject);
begin
   preFindAddressByAddress(cds_ExtBuffer_ebFrLoc.asstring);
   // JavaScript反饋時間太慢==先預找一次
   application.ProcessMessages;
   Sleep(100);
end;

procedure TfrmSendCar.EditStartPSEnter(Sender: TObject);
begin
   { if cds_ExtBuffer_ebPhone.asstring = '' then
     begin
     ShowMessageDlag('請先輸入上方電話欄位！');
     Abort;
     end;
   } end;

// 找地址（預找）
procedure TfrmSendCar.preFindAddressByAddress(addr: string);
var
   address: string; // , chi
   // tt: Integer;
   aStream: TMemoryStream;
begin
   if not gb_WebOK then
      exit;

   address := addr;
   address := StringReplace(StringReplace(trim(address), #13, ' ',
       [rfReplaceAll]), #10, ' ', [rfReplaceAll]);

   if address = '' then
      exit;

   if not Assigned(HTMLWindow2) then begin
      if Assigned(WebBrowser1.Document) then begin
         gb_WebOK := false;
         aStream := TMemoryStream.Create;
         try
            aStream.WriteBuffer(Pointer(HTMLStr)^, Length(HTMLStr));
            aStream.Seek(0, soFromBeginning);
            (WebBrowser1.Document as IPersistStreamInit)
                .Load(TStreamAdapter.Create(aStream));
         finally
            aStream.Free;
         end;
      end;
      HTMLWindow2 := (WebBrowser1.Document as IHTMLDocument2).parentWindow;
   end;

   HTMLWindow2.execScript(Format('prcodeAddress(%s)', [quotedstr(address)]),
       'JavaScript');
   application.ProcessMessages;
end;

function TfrmSendCar.gotoaddress(sAddr: string; ab_Clear: Boolean): string;
var
   saddress, sResultaddr: string;
   i: Integer;

   procedure gp_GetAddr;
   var
      ls_Addr, ls_lng, ls_lat: String;
   begin
      if Self.sURLLat = '' then begin

         sResultaddr := WebBrowser1.OleObject.Document.script.Baddresss;
         { if pos(MotorCade.smccity, sResultaddr) > 0 then
           begin
         }
         editLongitudeE.Text := WebBrowser1.OleObject.Document.script.Blng;
         editLatitudeE.Text := WebBrowser1.OleObject.Document.script.Blat;
         { end
           else
           sResultaddr := '';
         }
      end
      else begin
         editLongitudeE.Text := Self.sURLLon;
         editLatitudeE.Text := Self.sURLLat;
      end;

      if AnsiPos(',', sResultaddr) > 0 then begin
         ls_Addr := sResultaddr;
         ls_lng := editLongitudeE.Text;
         ls_lat := editLatitudeE.Text;
         ComboBoxAdrList.Items.clear;

         if cds_Addr_.Active then
            cds_Addr_.EmptyDataSet
         else
            cds_Addr_.CreateDataSet;

         repeat
            cds_Addr_.append;
            cds_Addr_Baddress.asstring :=
                Copy(ls_Addr, 1, AnsiPos(',', ls_Addr) - 1);

            if pos('號', cds_Addr_Baddress.asstring) < 1 then
               cds_Addr_Baddress.asstring := cds_Addr_Baddress.asstring +
                   gs_AddrNo;

            cds_Addr_Blng.AsFloat :=
                strtofloatdef(Copy(ls_lng, 1, AnsiPos(',', ls_lng) - 1), 0);
            cds_Addr_Blat.AsFloat :=
                strtofloatdef(Copy(ls_lat, 1, AnsiPos(',', ls_lat) - 1), 0);
            Delete(ls_Addr, 1, AnsiPos(',', ls_Addr));
            Delete(ls_lng, 1, AnsiPos(',', ls_lng));
            Delete(ls_lat, 1, AnsiPos(',', ls_lat));
            ComboBoxAdrList.Items.Add(cds_Addr_Baddress.asstring);
            cds_Addr_.CheckBrowseMode;
         until AnsiPos(',', ls_Addr) < 1;
         { if RadioButton1.Checked then
           fAddrChoice.gb_In := true
           else
           fAddrChoice.gb_In := false;
         }
         if cds_Addr_.recordcount > 1 then begin
            // fAddrChoice.ShowModal;
            if RadioButton1.Checked then begin
               ComboBoxAdrList.Parent := GroupBox4;
               ComboBoxAdrList.Width := editStartPlace.Width;
               ComboBoxAdrList.left := editStartPlace.left;
               ComboBoxAdrList.top := editStartPlace.top +
                   editStartPlace.height;
            end
            else begin
               ComboBoxAdrList.Parent := GroupBox5;
               ComboBoxAdrList.Width := editEndPlace.Width;
               ComboBoxAdrList.left := editEndPlace.left;
               ComboBoxAdrList.top := editEndPlace.top + editEndPlace.height;
            end;

            ComboBoxAdrList.Visible := True;
            ComboBoxAdrList.ItemIndex := 0;
            ComboBoxAdrList.DroppedDown := True;
            // ComboBoxAdrListChange(ComboBoxAdrList); 2015.3.22 不預選
            while ComboBoxAdrList.DroppedDown do begin
               Sleep(200);
               application.ProcessMessages;
            end;

            if RadioButton1.Checked then begin
               // sResultaddr := cds_ExtBuffer_ebFrAddr.AsString;
               editLongitudeE.Text := cds_ExtBuffer_ebFrLng.asstring;
               editLatitudeE.Text := cds_ExtBuffer_ebFrLat.asstring;
               RadioButton6.Checked := True;
            end
            else begin
               // sResultaddr := cds_ExtBuffer_ebToAddr.AsString;
               editLongitudeE.Text := cds_ExtBuffer_ebToLng.asstring;
               editLatitudeE.Text := cds_ExtBuffer_ebToLat.asstring;
               RadioButton8.Checked := True;
            end;
         end
         else begin
            // sResultaddr := fAddrChoice.cds_Addr_Baddress.AsString;
            editLongitudeE.Text := cds_Addr_Blng.asstring;
            editLatitudeE.Text := cds_Addr_Blat.asstring;
         end;
      end;
   end;

begin
   Result := '';

   editLongitudeE.Text := '';
   editLatitudeE.Text := '';

   if ab_Clear then
      HTMLWindow2.execScript('ClearMarkers()', 'JavaScript');

   WebBrowser1.OleObject.Document.script.Baddresss := '';
   WebBrowser1.OleObject.Document.script.Blng := '';
   WebBrowser1.OleObject.Document.script.Blat := '';

   saddress := sAddr;

   saddress := StringReplace(StringReplace(trim(saddress), #13, ' ',
       [rfReplaceAll]), #10, ' ', [rfReplaceAll]);

   saddress := GetPlaceAddress(saddress);
   if saddress = '' then begin
      if pos('飯店', sAddr) > 0 then begin
         saddress := StringReplace(sAddr, '飯店', '大飯店', [rfReplaceAll]);

         saddress := GetPlaceAddress(saddress);
      end
      else begin
         saddress := sAddr;
      end;
   end;

   HTMLWindow2.execScript(Format('codeAddress(%s)', [quotedstr(saddress)]),
       'JavaScript');

   if saddress = '' then begin
      gp_GetAddr;

      i := 0;
      while i < 11 do begin
         if (sResultaddr = '') then begin
            // 1秒後沒回應再執行一次詢問
            if i = 5 then
               HTMLWindow2.execScript(Format('codeAddress(%s)',
                   [quotedstr(saddress)]), 'JavaScript');

            gp_GetAddr;
         end
         else
            Break;

         inc(i);
         application.ProcessMessages;
         Sleep(200);
      end;
   end
   else begin
      sResultaddr := saddress;
      gp_GetAddr;
   end;

   saddress := proccessAddressChose(sResultaddr);
   Result := saddress;
end;

function TfrmSendCar.GetPlaceAddress(place: String): string;
const
   sUrl1: string =
       'https://maps.googleapis.com/maps/api/place/textsearch/xml?key=AIzaSyAKXofW6wr5eM1EtiYkX-OJKYzkuJgm2vo&language=zh-TW&query=';
   // 'https://maps.googleapis.com/maps/api/place/queryautocomplete/xml?key=AIzaSyAKXofW6wr5eM1EtiYkX-OJKYzkuJgm2vo&language=zh-TW&input=';
   sUrl2: string =
       'https://maps.googleapis.com/maps/api/geocode/xml?key=AIzaSyAKXofW6wr5eM1EtiYkX-OJKYzkuJgm2vo&language=zh-TW&place_id=';
var
   sResult: String;
   firstPlaceID: String; // 以第一個找到的 Place id 為例繼續搜尋
   lHTTP: TIdHTTP;
   sPos, ePos: Integer;
begin
   lHTTP := TIdHTTP.Create(nil);

   Self.sURLLat := '';
   Self.sURLLon := '';
   try
      lHTTP.IOHandler := TIdSSLIOHandlerSocketOpenSSL.Create(lHTTP);
      lHTTP.HandleRedirects := True;

      // sResult := lHTTP.Get(sUrl1 + HttpEncode(UTF8Encode(place)));
      sResult := lHTTP.Get(sUrl1 + TIdURI.ParamsEncode(place,
          IndyTextEncoding_UTF8));
      if pos('<status>OK</status>', sResult) = 0 Then begin
         Result := '';
         exit;
      end;

      Result := '';
      sPos := pos('<formatted_address>', sResult);
      ePos := pos('</formatted_address>', sResult);

      if ePos > 0 then begin
         sPos := pos('<formatted_address>', sResult);
         ePos := pos('</formatted_address>', sResult);
         Result := Copy(sResult, sPos + 19, ePos - sPos - 19);

         sPos := pos('<lat>', sResult);
         ePos := pos('</lat>', sResult);
         Self.sURLLat := Copy(sResult, sPos + 5, ePos - sPos - 5);

         sPos := pos('<lng>', sResult);
         ePos := pos('</lng>', sResult);
         Self.sURLLon := Copy(sResult, sPos + 5, ePos - sPos - 5);

         WebBrowser1.OleObject.Document.script.Blng := Self.sURLLon;
         WebBrowser1.OleObject.Document.script.Blat := Self.sURLLat;

         cds_ExtBuffer_ebFrLng.asstring := Self.sURLLon;
         cds_ExtBuffer_ebToLat.asstring := Self.sURLLat;

         if pos('號', Result) = 0 then
            Result := '';
         // if Length(Result) < 3 then
         // Result := '';
      end;

      if Result = '' then begin
         sPos := pos('<place_id>', sResult);
         ePos := pos('</place_id>', sResult);
         if ePos > sPos then begin
            firstPlaceID := Copy(sResult, sPos + 10, ePos - sPos - 10);
            sResult := lHTTP.Get(sUrl2 + firstPlaceID);
            sPos := pos('<formatted_address>', sResult);
            ePos := pos('</formatted_address>', sResult);
            Result := Copy(sResult, sPos + 19, ePos - sPos - 19);
            if Result = '' then
               Result := place;

            sPos := pos('<lat>', sResult);
            ePos := pos('</lat>', sResult);
            Self.sURLLat := Copy(sResult, sPos + 5, ePos - sPos - 5);

            sPos := pos('<lng>', sResult);
            ePos := pos('</lng>', sResult);
            Self.sURLLon := Copy(sResult, sPos + 5, ePos - sPos - 5);
         end
      end;
   finally
      lHTTP.Free;
   end;
end;

function TfrmSendCar.gf_CancelPaiche(as_Cxno, as_dvid, as_Phone,
    as_dvno: String): Boolean;
var
   sTemp: String;
   i, iBufferSize: Integer;
begin
   Result := false;
   // 取消派车	"D"+2 Byte "車隊號"+"	派車員帳號 + ',' + "密碼" + ',' + "車ID" + ',' + "單號" + '#'				60
   { aTempdata[0] := byte('D');
     aTempdata[1] := MotorCade.iID div (high(byte) + 1); // 車隊
     aTempdata[2] := MotorCade.iID mod (high(byte) + 1); // 車隊
   }  // 帳號                         //密碼（需10碼不足前面補零）
   sTemp := 'D' + IntToStr(MotorCade.iID) + ',' + LoginUser.sUserID + ',' +
       LoginUser.sPassword + ',' + as_dvid + ',' + as_Cxno;

   SocketSendData.iMotorCadeID := MotorCade.iID;
   SocketSendData.sLoginID := LoginUser.sUserID;
   SocketSendData.sPassword := LoginUser.sPassword;
   SocketSendData.idvid := strtoint(as_dvid);
   SocketSendData.icxno := strtoint(as_Cxno);

   WriteUsrtx(MotorCade.iID, LoginUser.sUserID, '取消一般派車', '客戶電話:' + as_Phone,
       '上車地點:' + dm1.qyCalltxHist.fieldbyname('cxfrloc').asstring,
       '司機台號 = ' + as_dvno);
   Self.ShowAddMessage(0, '(' + as_Phone + ')送出派車取消訊息(' + sTemp + ')。');
   StartThreads(200, SocketSendData); // state=200 發送派車取消需求

end;

function TfrmSendCar.gf_CheckMapData(as_Addr: String; ab_In: Boolean): String;
begin
   // dm1.zconMySQL.Disconnect;
   fSuppLoc.zq_SuppLoc.Close;
   fSuppLoc.zq_SuppLoc.SQL.Text := 'select slLoc, slLocRef, slLong, slLat' +
       '  from supploc where mcid = ' + IntToStr(MotorCade.iID) +
       '   and (   slLoc like ' + quotedstr('%' + as_Addr + '%')
   // + '        or slAddr like ' + QuotedStr('%' + as_Addr + '%')
       + '        or slLocRef like ' + quotedstr('%' + as_Addr + '%') + ')';
   fSuppLoc.zq_SuppLoc.open;

   if fSuppLoc.zq_SuppLoc.recordcount = 1 then begin
      // Result := fSuppLoc.zq_SuppLoc.FieldByName('slAddr').AsString;
      editLongitudeE.Text := fSuppLoc.zq_SuppLoc.fieldbyname('slLong').asstring;
      editLatitudeE.Text := fSuppLoc.zq_SuppLoc.fieldbyname('slLat').asstring;
      cds_ExtBuffer_.Edit;
      cds_ExtBuffer_ebFrLocRef.asstring := fSuppLoc.zq_SuppLoc.fieldbyname
          ('slLocRef').asstring;
      cds_ExtBuffer_ebFrLoc.asstring := fSuppLoc.zq_SuppLoc.fieldbyname
          ('slLoc').asstring;
   end
   else if fSuppLoc.zq_SuppLoc.recordcount > 1 then begin
      fSuppLoc.gb_In := ab_In;
      fSuppLoc.ShowModal;

      if ab_In then begin
         // Result := cds_ExtBuffer_ebFrAddr.AsString;
         editLongitudeE.Text := cds_ExtBuffer_ebFrLng.asstring;
         editLatitudeE.Text := cds_ExtBuffer_ebFrLat.asstring;
      end
      else begin
         // Result := cds_ExtBuffer_ebToAddr.AsString;
         editLongitudeE.Text := cds_ExtBuffer_ebToLng.asstring;
         editLatitudeE.Text := cds_ExtBuffer_ebToLat.asstring;
      end;
   end
   else
      Result := '';
end;

// CSR2 screen layout jieshu 14/6/2 車輛
procedure TfrmSendCar.gp_A2_P2N(at_A2, at_A1: TComboBox);
begin
   if StrToIntDef(at_A2.Text, -1) = -1 then
      exit; // 不是數字

   dm1.zqTemp.Close;
   dm1.zqTemp.SQL.Text := 'select a2 from addr2x where mcid = ' +
       IntToStr(MotorCade.iID) + '   and a1 = ' + quotedstr(at_A1.Text) +
       ' and pri = ' + at_A2.Text;
   dm1.zqTemp.open();

   if dm1.zqTemp.recordcount > 0 then
      at_A2.Text := dm1.zqTemp.fieldbyname('A2').asstring;
end;

procedure TfrmSendCar.gp_CheckPhone;
var
   li_i: Integer;
   ls_P: String;
begin
   ls_P := cds_ExtBuffer_ebPhone.asstring;

   if (Copy(ls_P, 1, 1) = '+') then begin
      if (ls_P.Length <= 14) then begin
         if StrToInt64Def(Copy(ls_P, 2, 13), -1) = -1 then begin
            gf_ShowMsg('電話號碼錯誤，必須都是數字', 'CSR', mtWarning, 0, 128, Self);
            editPhone.SetFocus;
            Abort;
         end
         else
            exit;
      end
      else begin
         gf_ShowMsg('+號開頭最多14碼！', 'CSR', mtWarning, 0, 128, Self);
         editPhone.SetFocus;
         Abort;
      end;
   end;

   if not((Length(ls_P) = 9) or (Length(ls_P) = 10) or (Length(ls_P) = 11) or
       (Length(ls_P) = 1)) or (ls_P[1] <> '0') then begin
      gf_ShowMsg('電話號碼錯誤，長度必須是9或10或11，且第一碼必須是0', 'CSR', mtWarning, 0,
          128, Self);
      editPhone.SetFocus;
      Abort;
   end;

   for li_i := 1 to Length(ls_P) do
      if StrToIntDef(ls_P[li_i], -1) = -1 then begin
         gf_ShowMsg('電話號碼錯誤，必須都是數字', 'CSR', mtWarning, 0, 128, Self);
         editPhone.SetFocus;
         Abort;
      end;
end;

procedure TfrmSendCar.gp_ExecPaiche(var at_SSD: TSocketSendData);
begin
end;

procedure TfrmSendCar.gp_GetAddrNo(as_Addr: String);
var
   li_i: Integer;
   tmpStr: String;
begin
   if AnsiPos('''', as_Addr) > 0 then begin
      ShowMessageDlag('地點不可以含 引號');
      Abort;
   end;

   gs_AddrNo := '';
   tmpStr := as_Addr;

   repeat
      if (pos('號', tmpStr) > 0) then
         gs_AddrNo := gs_AddrNo + Fetch(tmpStr, '號') + '號'
      else
         gs_AddrNo := gs_AddrNo + Fetch(tmpStr, '號');
   until pos('號', tmpStr) = 0;
   // if pos('號', as_Addr) > 0 then begin
   // gs_AddrNo := as_Addr;
   //
   // gs_AddrNo := Fetch(gs_AddrNo, '號') + '號';
   // for li_i := Length(as_Addr) downto 1 do begin
   // if as_Addr[li_i] = '號' then
   // gs_AddrNo := '號';
   //
   // if (gs_AddrNo <> '') and (as_Addr[li_i] <> '號') then
   // if (StrToIntDef(as_Addr[li_i], -1) = -1) then
   // Break
   // else
   // gs_AddrNo := as_Addr[li_i] + gs_AddrNo;
   // end;
   // end;
end;

procedure TfrmSendCar.gp_ShowCarInfo;
begin
end;

procedure TfrmSendCar.gp_ShowDefer;
begin
end;

// CSR2 screen layout jieshu 14/6/2 未完工
procedure TfrmSendCar.gp_UpdateCsrClick(as_Cxno: String);
begin
   dm1.zqTemp.Close;
   dm1.zqTemp.SQL.Text := 'Update calltx Set cxdtcsrclick = :a1 where mcid = ' +
       IntToStr(MotorCade.iID) + '   and csid = ' + quotedstr(LoginUser.sUserID)
       + '   and cxno = ' + as_Cxno;
   dm1.zqTemp.ParamByName('a1').AsDateTime := now;
   dm1.zqTemp.EXECSQL;
end;

procedure TfrmSendCar.gp_ValidateInput;
begin
   gb_NotEnter := True;
   try
      { 判別在離開前有無輸入資料，如無Delphi會自動清除此筆Insert row }
      SelectNext(ActiveControl, True, True); // 第二個參數 True 移動焦點到下一個物件
      // 第三個參數是要不要檢查下一個元件的 TabStop
      SelectNext(ActiveControl, false, True); // 第二個參數 False 移動焦點到上一個物件

      if not(cds_ExtBuffer_.State in [dsInsert, dsEdit]) then
         cds_ExtBuffer_.Edit;

      cds_ExtBuffer_ebPhone.asstring := editPhone.Text;
   finally
      gb_NotEnter := false;
   end;
end;

procedure TfrmSendCar.gp_WriteToLog(as_Msg: String);
var
   LogFile: String;
   Stream: TFileStream;
   CyrillicText: CyrillicString;
begin

   if as_Msg = '' then
      exit;
   CyrillicText := as_Msg;

   LogFile := 'CSR' + formatdatetime('yymmdd', Date) + '.log';
   LogFile := gs_LogPath + LogFile;
   if not FileExists(LogFile) then
      Stream := TFileStream.Create(LogFile, fmCreate or fmShareDenyWrite)
   else
      Stream := TFileStream.Create(LogFile, fmOpenReadWrite or
          fmShareDenyWrite);

   with Stream do
      try
         Seek(0, soFromEnd);
         WriteBuffer(CyrillicText[1], Length(CyrillicText));
      finally
         Free;
      end;
end;

procedure TfrmSendCar.ListBoxSendRTMsgChange(Sender: TObject);
begin
   ListBoxSendRTMsg.SelStart := ListBoxSendRTMsg.GetTextLen;
   ListBoxSendRTMsg.SelLength := 0;
   ListBoxSendRTMsg.ScrollBy(0, ListBoxSendRTMsg.Lines.Count);
   ListBoxSendRTMsg.Refresh;
end;

procedure TfrmSendCar.PageControl1DrawTab(Control: TCustomTabControl;
    TabIndex: Integer; const Rect: TRect; Active: Boolean);
var
   page: TTabSheet;
begin
   page := Self.PageControl1.Pages[TabIndex];

   if ExtNo.Caption <> '再派' then begin
      case TabIndex of
         0: begin
               Control.Canvas.Brush.Color := clgreen;
               Control.Canvas.Font.Color := clWhite;
            end;
         1:
            Control.Canvas.Brush.Color := clYellow;
      else begin
            Control.Canvas.Brush.Color := clBtnFace;
         end;
      end;
   end
   else begin
      Control.Canvas.Brush.Color := clred;
      Control.Canvas.Font.Color := clWhite;
   end;

   if page.TabVisible then begin
      Control.Canvas.TextOut(Rect.left + 5, Rect.top + 3, page.Caption);
      page.Brush.Color := Control.Canvas.Brush.Color;
   end;
end;

procedure TfrmSendCar.Panel3Exit(Sender: TObject);
begin
   if ColorButton6.Tag = 0 then
      ColorButton6Click(Sender);

end;

procedure TfrmSendCar.panelPhoneClick(Sender: TObject);
begin

end;

// 截取地址及判斷是否有在中國或台灣
function TfrmSendCar.proccessAddressChose(saddresStr: string): string;
var
   chi: string;
begin
   Result := saddresStr;
   // 2014/5/14 user要求比照舊版截斷地址
   // 判斷是否在台灣或中國境內來區分是否找到地址
   // 因為Google服務無論addresStr入什麼內容找查地址都會有回覆答案
   { if (pos('台灣', saddresStr) = 0) and (pos('人民', saddresStr) = 0) then
     begin
     ShowMessageDlag('查無此地址！');
     result := '';
     Exit;
     end;        jieshu 14/6/19 這個先 mark 吧
   }
   // 截取有效的地址內容-市與縣之前不需要
   chi := '市';
   if pos(chi, saddresStr) = 0 then
      chi := '縣';

   Result := Copy(saddresStr, pos(chi, saddresStr) + 1,
       Length(saddresStr) - pos(chi, saddresStr));

end;

procedure TfrmSendCar.queryAddressReplaceClick;
var
   // sJSFn: string ;
   originalInput, stempAddress, sTemp, tmpStr, tmpAddr: string;
   aStream: TMemoryStream;
   lb_First: Boolean;
begin
   Self.Panel14.Visible := false;
   if Assigned(WebBrowser1.Document) then begin
      gb_WebOK := false;
      aStream := TMemoryStream.Create;
      try
         aStream.WriteBuffer(Pointer(HTMLStr)^, Length(HTMLStr));
         aStream.Seek(0, soFromBeginning);
         (WebBrowser1.Document as IPersistStreamInit)
             .Load(TStreamAdapter.Create(aStream));
      finally
         aStream.Free;
      end;
      HTMLWindow2 := (WebBrowser1.Document as IHTMLDocument2).parentWindow;
      lb_First := True;
   end
   else
      lb_First := false;

   if RadioButton5.Checked then begin
      if Edit4.Text = '' then begin
         ShowMessageDlag('請輸入門牌號');
         Edit4.SetFocus;
         Abort;
      end;

      originalInput := editStartPlace.Text;
      cds_ExtBuffer_ebFrLoc.asstring := EnCode_ebFrLoc();
      editStartPlace.Text := EnCode_ebFrLoc();
   end
   else begin
      originalInput := editStartPlace.Text;
      tmpStr := editStartPlace.Text;
      tmpAddr := '';
      repeat
         if pos('號', tmpStr) > 0 then
            tmpAddr := tmpAddr + Fetch(tmpStr, '號') + '號'
         else
            tmpAddr := tmpAddr + Fetch(tmpStr, '號');
      until pos('號', tmpStr) = 0;
      cds_ExtBuffer_ebFrLoc.asstring := tmpAddr; // editStartPlace.Text;
   end;

   gp_GetAddrNo(cds_ExtBuffer_ebFrLoc.asstring);

   if editStartPlace = ActiveControl then
      editStartPlaceExit(nil);

   ShowAddMessage(StrToIntDef(ExtNo.Caption, 0), '執行上車地點定位');
   RadioButton1.Checked := True;

   try
      screen.Cursor := crHourGlass;

      repeat
         Sleep(100);
         application.ProcessMessages;
      until gb_WebOK;

      // if not lb_First then
      // HTMLWindow2.execScript('ClearMarkers()', 'JavaScript'); // 清除圖標
      // W-140530 8. 目前查地址有 bug
      if (trim(cds_ExtBuffer_ebFrLoc.asstring) = '') then
         // and (trim(cds_ExtBuffer_ebFrAddr.asstring) = '') then
         exit;

      // if (cds_ExtBuffer_ebFrLoc.asstring <> '') then
      sTemp := trim(cds_ExtBuffer_ebFrLoc.asstring);
      editLongitudeE.Text := '';

      if CheckBox1.Checked then
         gf_CheckMapData(sTemp, True); // 補充圖資 jieshu 14/7/11
      // if (cds_ExtBuffer_ebFrAddr.asstring <> '') then
      // sTemp := trim(cds_ExtBuffer_ebFrAddr.asstring);
      if editLongitudeE.Text = '' then // 補充圖資 jieshu 14/7/11
         stempAddress := gotoaddress(sTemp, not lb_First);

      if not(cds_ExtBuffer_.State in [dsInsert, dsEdit]) then
         cds_ExtBuffer_.Edit;

      if trim(editLongitudeE.Text) = '' then // 找不到時....
      begin
         cds_ExtBuffer_ebFrLng.asstring := '';
         cds_ExtBuffer_ebFrLat.asstring := '';
         ShowMessageDlag('地點不明確 或 錯誤 !');
      end
      else begin
         // 因為有時查找的地點會在Google的回覆字串中，要再次處理
         { if pos(cds_ExtBuffer_ebFrLoc.asstring, stempAddress) > 0 then
           cds_ExtBuffer_ebFrAddr.asstring :=
           Copy(stempAddress, 1, pos(cds_ExtBuffer_ebFrLoc.asstring,
           stempAddress) - 1)
           else 相同 也 去掉。這部份的程式碼，麻煩你  mark 起來 jieshu 14/6/21
         } // cds_ExtBuffer_ebFrAddr.asstring := trim(stempAddress);

         cds_ExtBuffer_ebFrLng.asstring := editLongitudeE.Text;
         cds_ExtBuffer_ebFrLat.asstring := editLatitudeE.Text;
         Label65.Caption := '經度：' + editLongitudeE.Text + '，緯度：' +
             editLatitudeE.Text;
      end;

      // 如果有出發座標》標出圖標
      if (strtofloatdef(cds_ExtBuffer_ebFrLat.asstring, 0) > 0) and
          (strtofloatdef(cds_ExtBuffer_ebFrLng.asstring, 0) > 0) then begin
         HTMLWindow2.execScript(Format('PutMarkerIco(%s,%s,%s,%d,%s)',
             [cds_ExtBuffer_ebFrLat.asstring, cds_ExtBuffer_ebFrLng.asstring,
             quotedstr('上車點'), 6, quotedstr(sImagePath)]), 'JavaScript');

         showPosition; // 把地圖的範圍拉到二個圖標的大小
      end;
   finally
      // cds_ExtBuffer_ebFrLoc.asstring := originalInput;
      // editStartPlace.Text := originalInput;
      screen.Cursor := crDefault;
   end;
end;

procedure TfrmSendCar.RadioButton1Click(Sender: TObject);
begin
   if not(cds_ExtBuffer_.State in [dsInsert, dsEdit]) and
       (AnsiPos(ExtNo.Caption, '已派,再派') < 1) then
      cds_ExtBuffer_.Edit;

   if RadioButton1.Checked then begin
      if (AnsiPos(ExtNo.Caption, '已派,再派') < 1) then
         cds_ExtBuffer_ebInOut.asstring := 'I';

      GroupBox4.Visible := True;
      GroupBox5.Visible := false;
      Image1.Visible := True;
      Image2.Visible := false;
      // editStartPlace.SetFocus;
      editStartPlace.SelStart := Length(editStartPlace.Text);
   end
   else begin
      if (AnsiPos(ExtNo.Caption, '已派,再派') < 1) then
         cds_ExtBuffer_ebInOut.asstring := 'O';

      GroupBox4.Visible := false;
      GroupBox5.Visible := True;
      Image2.Visible := True;
      Image1.Visible := false;

      if (cds_ExtBuffer_ebToLoc.asstring = '') and
          (AnsiPos(ExtNo.Caption, '已派,再派') < 1) and
          (MotorCade.mcticketcity = '1') then
         cds_ExtBuffer_ebToLoc.asstring := MotorCade.smccity;

      // editEndPlace.SetFocus;
      editEndPlace.SelStart := Length(editEndPlace.Text);
   end;
end;

procedure TfrmSendCar.RadioButton3Click(Sender: TObject);
begin
   if RadioButton3.Checked then
      ComboBox1.Items.CommaText := '0,1,2,3,4,5,6,7,8,9,10,11'
   else
      ComboBox1.Items.CommaText := '12,1,2,3,4,5,6,7,8,9,10,11';

   DateTimePicker1Change(Sender);
end;

procedure TfrmSendCar.RadioButton5Click(Sender: TObject);
begin
   if RadioButton6.Checked then begin
      editStartPlace.Font.Color := clred;
      RadioButton6.OnClick := nil;

      if gb_Focus then
         editStartPlace.SetFocus
      else
         gb_Focus := True;

      editStartPlace.SelLength := 0;
      editStartPlace.SelStart := Length(cds_ExtBuffer_ebFrLoc.asstring);
      if (editStartPlace.Text = '') then
         editStartPlace.Text := EnCode_ebFrLoc();

      RadioButton6.OnClick := RadioButton5Click;
   end
   else
      editStartPlace.Font.Color := clGray;
end;

procedure TfrmSendCar.RadioButton8Click(Sender: TObject);
begin
   if RadioButton8.Checked then begin
      editEndPlace.Font.Color := clred;
      RadioButton8.OnClick := nil;
      editEndPlace.SetFocus;
      editEndPlace.SelLength := 0;
      editEndPlace.SelStart := Length(cds_ExtBuffer_ebToLoc.asstring);
      RadioButton8.OnClick := RadioButton8Click;
   end
   else
      editEndPlace.Font.Color := clGray;
end;

procedure TfrmSendCar.RadioGroup2Click(Sender: TObject);
begin
   manualFillInCarCount := Self.RadioGroup2.ItemIndex + 1;

   case Self.RadioGroup2.ItemIndex of
      0: begin
            Self.Label71.Enabled := false;
            Self.ComboBox19.Enabled := false;
            Self.Label73.Enabled := false;
            Self.ComboBox21.Enabled := false;
            Self.Label75.Enabled := false;
            Self.ComboBox23.Enabled := false;

            Self.Label72.Enabled := false;
            Self.ComboBox20.Enabled := false;
            Self.Label74.Enabled := false;
            Self.ComboBox22.Enabled := false;
            Self.Label76.Enabled := false;
            Self.ComboBox24.Enabled := false;
         end;
      1: begin
            Self.Label71.Enabled := True;
            Self.ComboBox19.Enabled := True;
            Self.Label73.Enabled := True;
            Self.ComboBox21.Enabled := True;
            Self.Label75.Enabled := True;
            Self.ComboBox23.Enabled := True;

            Self.Label72.Enabled := false;
            Self.ComboBox20.Enabled := false;
            Self.Label74.Enabled := false;
            Self.ComboBox22.Enabled := false;
            Self.Label76.Enabled := false;
            Self.ComboBox24.Enabled := false;
         end;
      2: begin
            Self.Label71.Enabled := True;
            Self.ComboBox19.Enabled := True;
            Self.Label73.Enabled := True;
            Self.ComboBox21.Enabled := True;
            Self.Label75.Enabled := True;
            Self.ComboBox23.Enabled := True;

            Self.Label72.Enabled := True;
            Self.ComboBox20.Enabled := True;
            Self.Label74.Enabled := True;
            Self.ComboBox22.Enabled := True;
            Self.Label76.Enabled := True;
            Self.ComboBox24.Enabled := True;
         end;
   end;
end;

procedure TfrmSendCar.readini;
var
   aStream: TMemoryStream;
   sStartPosition: string;
   // CSR2 screen layout jieshu 14/6/2 舊的CSR定位
   ls_Path: String;
   // ls_tmp : String;
   Fini: Tinifile;
begin

   sFilePath := ExtractFilePath(ParamStr(0));
   Fini := Tinifile.Create(sFilePath + System_INI_FileName);
   try
      MotorCade.iID := strtoint(Fini.ReadString('CSR', 'mcid', ''));
      LoginUser.sUserID := Fini.ReadString('CSR', 'loginName', '');

      with SocketData do begin
         iRemortPort := Fini.Readinteger('CSR', 'VTA_CSRPort', 3881);
         sLocalPort := Fini.ReadString('CSR', 'VTA_TaxiPort', '3880');
         iTimeout := Fini.Readinteger('CSR', 'VTA_WaitTimeout', 30);
      end;

      if Copy(LoginUser.sUserID, 1, 1) = '*' then begin
         dm1.FDConnection1.Params.Values['Server'] := dm1.gs_TestIP;
         SocketData.sRemortIP := dm1.gs_TestIP;
         // IdTCPClient1.Host := dm1.gs_TestIP;
      end
      else
         SocketData.sRemortIP := dm1.FDConnection1.Params.Values['Server'];

      /// 1.先查車隊號存不存在
      with dm1 do begin
         qy1.Close;
         qy1.SQL.clear;
         qy1.SQL.Text :=
             'select mcname,mcshortname,mclong,mclat,mccity,mcminperkm,mcbidtimeout,mcticketcity'
             + '  from mtrcd where mcid = ' + IntToStr(MotorCade.iID);
         qy1.open;
         if qy1.recordcount = 0 then // 查無車隊
         begin
            ShowMessageDlag('無此車隊號!');
            exit;
         end
         else begin
            MotorCade.sName := qy1.fieldbyname('mcname').asstring;
            MotorCade.sShortName := qy1.fieldbyname('mcshortname').asstring;
            MotorCade.smccity := qy1.fieldbyname('mccity').asstring;
            MotorCade.mcticketcity := qy1.fieldbyname('mcticketcity').asstring;
            MotorCade.imcbidtimeout := qy1.fieldbyname('mcbidtimeout')
                .AsInteger;
            MotorCade.imcminperkm := qy1.fieldbyname('mcminperkm').AsFloat;
            if not qy1.fieldbyname('mclong').IsNull then
               MotorCade.rLng := qy1.fieldbyname('mclong').AsFloat
            else
               MotorCade.rLng := 0;
            if not qy1.fieldbyname('mclat').IsNull then
               MotorCade.rLat := qy1.fieldbyname('mclat').AsFloat
            else
               MotorCade.rLat := 0;
         end;
         /// 2.判斷權限是否存在

         qy1.Close;
         qy1.SQL.clear;
         qy1.SQL.Text := 'SELECT * from user where usid =' +
             quotedstr(LoginUser.sUserID) + ' and mcid =' +
             quotedstr(IntToStr(MotorCade.iID));
         qy1.open;

         if qy1.recordcount = 0 then begin
            ShowMessageDlag('無此帳號！請重新輸入!');
            exit;
         end;

         LoginUser.sPassword := qy1.fieldbyname('uspw').asstring; // 密碼
         LoginUser.sUsername := qy1.fieldbyname('usnm').asstring;
         LoginUser.sPhone := qy1.fieldbyname('usphone').asstring;
         LoginUser.sRole := qy1.fieldbyname('usrole').asstring;

         qy1.Close;
         qy1.SQL.Text := 'select pid, quid, qunm from queue where mcid = ' +
             IntToStr(MotorCade.iID) + ' order by quid';
         qy1.open;
         ComboBox4.Items.Add('自動計算');
         ComboBox15.Items.Add('直接空派');
         gt_Queue.Add('0');
         gt_quid.Add('0');
         ComboBox4.Items.Add('直接空派');
         gt_Queue.Add('-1');
         gt_quid.Add('-1');
         ComboBox4.ItemIndex := 0;

         while not qy1.Eof do begin
            ComboBox4.Items.Add(qy1.fieldbyname('qunm').asstring);
            ComboBox15.Items.Add(qy1.fieldbyname('qunm').asstring);
            gt_Queue.Add(qy1.fieldbyname('pid').asstring);
            gt_quid.Add(qy1.fieldbyname('quid').asstring);
            qy1.next;
         end;
         ComboBox15.ItemIndex := 0;

         ComboBox8.Items.Assign(ComboBox4.Items);
         ComboBox8.Items[0] := '';
         ComboBox8.ItemIndex := 0;
         qy1.Close;
         qy1.SQL.Text := 'select cwtext from commonword where mcid = ' +
             IntToStr(MotorCade.iID) + ' order by cwcounter desc';
         qy1.open;

         while not qy1.Eof do begin
            ComboBox5.Items.Add(qy1.fieldbyname('cwtext').asstring);
            qy1.next;
         end;

         ComboBox6.Items.Assign(ComboBox5.Items);
      end;
   finally
      Fini.Free;
   end;
   // 1.啟動google
   sStartPosition := Format(' var latlng = new google.maps.LatLng(%s,%s);',
       [FloatToStr(MotorCade.rLat), FloatToStr(MotorCade.rLng)]);

   // CSR2 screen layout jieshu 14/6/2 舊的CSR定位
   // HTMLStr := HTMLStr1 + sStartPosition + HTMLStr2;
   HTMLStr := HTMLStr1 + AnsiString('    var latlng = new google.maps.LatLng(' +
       FloatToStr(MotorCade.rLat) + ',' + FloatToStr(MotorCade.rLng) + '); ')
       + HTMLStr2;
   ls_Path := ExtractFilePath(application.ExeName);

   if ls_Path[Length(ls_Path)] <> '\' then
      ls_Path := ls_Path + '\';

   ls_Path := ls_Path + 'image\';
   ls_Path := StringReplace(ls_Path, '\', '/', [rfReplaceAll]);
   HTMLStr := AnsiString(StringReplace(String(HTMLStr), '{ImagePath}', ls_Path,
       [rfReplaceAll]));

   { ls_tmp := gt_quid.Text;
     gt_quid.Text := HTMLStr;
     gt_quid.SaveToFile(ls_Path + 'map.htm');
     gt_quid.Text := ls_tmp;
     WebBrowser1.Navigate(ls_Path + 'map.htm');
   }

   WebBrowser1.Navigate('about:blank');

   if (ParamStr(1) = '已派') then begin
      if Assigned(WebBrowser1.Document) then begin
         aStream := TMemoryStream.Create;
         try
            aStream.WriteBuffer(Pointer(HTMLStr)^, Length(HTMLStr));
            aStream.Seek(0, soFromBeginning);
            (WebBrowser1.Document as IPersistStreamInit)
                .Load(TStreamAdapter.Create(aStream));
         finally
            aStream.Free;
         end;
         HTMLWindow2 := (WebBrowser1.Document as IHTMLDocument2).parentWindow;
      end;
   end;
end;

procedure TfrmSendCar.sbtCommon1Click(Sender: TObject);
// var
// dx, dy: Integer;
// s: string;
begin
   gp_ValidateInput;
   // s := TSpeedButton(Sender).Parent.Name;

   // dx := TSpeedButton(Sender).Parent.left + TSpeedButton(Sender).left;
   // +Tspeedbutton(sender).width;
   // dy := (height - frmAddList.height) div 2;
   // PanelHistRgion.top + PageControl1.top;

   with dm1.qy1 do begin
      Close;
      SQL.Text := 'select pgid from psgr where mcid = ' +
          IntToStr(MotorCade.iID) + ' and pgphone=' +
          quotedstr(cds_ExtBuffer_ebPhone.asstring) + ' and asno = ''''';
      open;
   end;

   if dm1.qy1.IsEmpty then begin
      gf_ShowMsg('新電話，無常用地點', 'CSR', mtWarning, 0, 128, Self);
      exit;
   end;

   // frmAddList.left := dx;
   // frmAddList.top := dy;
   frmAddList.gb_In := RadioButton1.Checked;
   frmAddList.pgid := dm1.qy1.fieldbyname('pgid').AsInteger;
   // frmAddlist.grdAddList.Tag := TSpeedButton(Sender).Tag;
   frmAddList.ShowModal; // .Show;
end;

procedure TfrmSendCar.btnHist1Click(Sender: TObject);
var
   iIndex, i, sIdx: Integer;
begin
   if not(cds_ExtBuffer_.State in [dsInsert, dsEdit]) then
      cds_ExtBuffer_.Edit;

   if not dm1.qyCalltxHist.Active then
      dm1.qyCalltxHist.open;

   iIndex := TSpeedButton(Sender).Tag;
   i := 0;
   with dm1.qyCalltxHist do begin
      first;
      while not Eof do begin
         inc(i);
         if i = iIndex then begin
            cds_ExtBuffer_ebFrLoc.asstring := fieldbyname('cxfrloc').asstring;
            // cds_ExtBuffer_ebFrAddr.asstring := fieldbyname('cxfraddr').asstring;
            cds_ExtBuffer_ebFrLocRef.asstring :=
                fieldbyname('cxfrlocref').asstring;
            cds_ExtBuffer_ebFrLng.asstring := fieldbyname('cxfrlong').asstring;
            cds_ExtBuffer_ebFrLat.asstring := fieldbyname('cxfrlat').asstring;
            // btnStartMap.OnClick(self);
            cds_ExtBuffer_ebToLoc.asstring := fieldbyname('cxtoloc').asstring;
            // cds_ExtBuffer_ebToAddr.asstring := fieldbyname('cxtoaddr').asstring;
            cds_ExtBuffer_ebToLocRef.asstring :=
                fieldbyname('cxtolocref').asstring;
            cds_ExtBuffer_ebToLng.asstring := fieldbyname('cxtolong').asstring;
            cds_ExtBuffer_ebToLat.asstring := fieldbyname('cxtolat').asstring;

            if GroupBox4.Visible then
               RadioButton6.Checked := True;

            if GroupBox5.Visible then
               RadioButton8.Checked := True;
            // btnEndMap.OnClick(self);
            if fieldbyname('quid').asstring = '' then begin
               Label45.Caption := '排班站：無';
               Label45.Font.Color := clBlack;
               ComboBox4.ItemIndex := 0;
            end
            else begin
               Label45.Font.Color := clred;
               sIdx := gt_quid.IndexOf(fieldbyname('quid').asstring);

               if sIdx = -1 then
                  sIdx := 0;
               ComboBox4.ItemIndex := sIdx;

               Label45.Caption := '排班站：' + ComboBox4.Text;
            end;
         end;
         next;
      end;
   end;

end;

procedure TfrmSendCar.btnHistCancel1Click(Sender: TObject);
var
   icxno: Integer;
begin
   if gf_ShowMsg('確定要取消派車嗎？', 'CSR', mtConfirmation, 1, 128, Self) = mrNo then
      exit;
   // 如果最近的一筆，乘客還未接到，則出現紅色 X，按下去可以取消該派車。（VTA，司機APP 要配合修改）
   // 自動結案待規格提供//先將calltx.cxsuccess=-1
   icxno := StrToIntDef(TSpeedButton(Sender).Hint, 0);

   if dm1.qyCalltxHist.locate('cxno', icxno, []) then
      if gf_CancelPaiche(dm1.qyCalltxHist.fieldbyname('cxno').asstring,
          dm1.qyCalltxHist.fieldbyname('dvid').asstring,
          dm1.qyCalltxHist.fieldbyname('cxpgPhone').asstring,
          dm1.qyCalltxHist.fieldbyname('dvno').asstring) then
         TSpeedButton(Sender).Visible := false;
end;

procedure TfrmSendCar.btnRePaiNowClick(Sender: TObject);
var
   icxno, iEXTNO, iThreadNo: Integer;
   tmpStr: String;
   i: Integer;
begin
   if gf_ShowMsg('確定要取消派車嗎？', 'CSR', mtConfirmation, 1, 128, Self) = mrNo then
      exit;
   // CSR 2.2.1 Issue 10.	取消派车成功，必须 设定 cxdtcsrclick 的时间。
   cds_car_.first;

   WriteUsrtx(MotorCade.iID, LoginUser.sUserID, '取消一般派車',
       '客戶電話:' + SocketSendData.sPhone, '上車地點:' + SocketSendData.scxfrPlace,
       '司機台號 = ' + cds_car_.fieldbyname('dvno').asstring);

   SocketSendData.icxno := cds_ExtBuffer_ebCxno.AsInteger;
   iEXTNO := StrToIntDef(ExtNo.Caption, 1);
   // 如果最近的一筆，乘客還未接到，則出現紅色 X，按下去可以取消該派車。
   if (SocketData.sRemortIP <> '') then begin
      // SocketSendData.idvid := StrToIntDef(aPnlPhone[iEXTNO - 1].Hint, -99);
      // 司機ID
      if SocketSendData.idvid = -1 then // thread尚未回傳
      begin
         iThreadNo := SocketSendData.iThreadNo;
         // aPnlPhone[iEXTNO - 1].Tag;
         if iThreadNo > 0 then
            TClientThread(fThreads[iThreadNo]).bCancelSocket := True;
         // 通知thread停止
         // aPnlPhone[iEXTNO - 1].Tag
         SocketSendData.iThreadNo := -1; // 復原
         Self.ShowAddMessage(iEXTNO, '執行緒尚未回傳，通知取消派車。');
      end
      else if SocketSendData.idvid > 0 then begin
         SocketSendData.iEXTNO := iEXTNO; // 切換後要重給
         Self.ShowAddMessage(iEXTNO, '啟動執行緒送出取消派車。');
         StartThreads(200, SocketSendData); // state=200 發送派車取消需求
         btnCancelSendCar.Enabled := false;
      end;
   end;

   tmpStr := SocketSendData.sPhone;

   gb_WaitingForCancel := True;
   repeat
      application.ProcessMessages;
      Sleep(100);
   until not gb_WaitingForCancel;

   Sleep(2000);

   icxno := StrToIntDef(Editcxno.Text, 0);
   updatecalltx(1, icxno, 3); // 將calltx.cxsuccess=3 代表取消
   DeleteBuffer(iEXTNO); // 分機暫存檔刪除
   // alabPhone[iEXTNO - 1].Caption := ''; // 14/5/31 完成、取消才能清電話 jieshu
   // ClearEdit;
   btnSendCar.flat := True;
   sbtCommon2.Enabled := false;

   for i := 0 to GroupBoxtTEST.ControlCount - 1 do
      if GroupBoxtTEST.Controls[i] is TEdit then
         TEdit(GroupBoxtTEST.Controls[i]).Text := '';

   for i := 0 to Panel3.ControlCount - 1 do
      if Panel3.Controls[i] is TEdit then
         TEdit(Panel3.Controls[i]).Text := '';

   for i := 0 to GroupBox4.ControlCount - 1 do
      if GroupBox4.Controls[i] is TEdit then
         TEdit(GroupBox4.Controls[i]).Text := '';

   for i := 0 to GroupBox5.ControlCount - 1 do
      if GroupBox5.Controls[i] is TEdit then
         TEdit(GroupBox5.Controls[i]).Text := '';

   // ListBoxSendRTMsg.Items.Clear;

   // 三筆歷史資料隱藏
   btnHistCancel1.Visible := false;
   for i := 0 to 2 do
      TPanel(PanelHist.Controls[i]).Visible := false;

   cds_ExtBuffer_ebPaiche.AsBoolean := True;
   cds_ExtBuffer_ebOkQty.Value := 0;
   Self.editPhone.Text := tmpStr;
   cds_ExtBuffer_ebPhone.asstring := tmpStr;

   gp_CheckPhone;
   ColorButton6.Tag := 1; // 表有按過了
   if cds_ExtBuffer_ebPhone.asstring = '' then
      exit;
   writeout;
   btnSendCarClick(Self.btnSendCar);
end;

procedure TfrmSendCar.btnSendCarClick(Sender: TObject);
var
   iCurEXT: Integer;
   li_i, li_Qty: Integer;
   i: Integer; // , li_j
   ls_Term, quid, originalInput, tmpStr, tmpAddr: String;
   lt_CB: TCheckBox;

   function lf_Value(ab_Y: Boolean): String;
   begin
      if ab_Y then
         Result := 'Y'
      else
         Result := 'N';
   end;

begin
   btnRePaiNow.Visible := false;
   if not(cds_ExtBuffer_.State in [dsInsert, dsEdit]) then
      cds_ExtBuffer_.Edit;

   gp_ValidateInput;
   gp_CheckPhone;
   // 檢查輸入欄位
   if (cds_ExtBuffer_ebPhone.asstring = '') then begin
      // W-140530 2．	CSR 按 “派车”bug：已有电话，却说没电话。另外，这个讯息框里，字太小。
      if editPhone.Text = '' then begin
         Label44.Caption := '請輸入乘客電話!';
         exit;
      end
      else
         cds_ExtBuffer_ebPhone.asstring := editPhone.Text;
   end;

   if RadioButton5.Checked then begin
      if RadioButton5.Checked then begin
         if Edit4.Text = '' then begin
            ShowMessageDlag('請輸入門牌號');
            Edit4.SetFocus;
            Abort;
         end;

         originalInput := editStartPlace.Text;
         cds_ExtBuffer_ebFrLoc.asstring := EnCode_ebFrLoc();
         editStartPlace.Text := EnCode_ebFrLoc();
      end
      else begin
         originalInput := editStartPlace.Text;
         tmpStr := editStartPlace.Text;
         tmpAddr := '';
         repeat
            if pos('號', tmpStr) > 0 then
               tmpAddr := tmpAddr + Fetch(tmpStr, '號') + '號'
            else
               tmpAddr := tmpAddr + Fetch(tmpStr, '號');
         until pos('號', tmpStr) = 0;
         cds_ExtBuffer_ebFrLoc.asstring := tmpAddr; // editStartPlace.Text;
      end;

      Self.queryAddressReplaceClick;
   end;
   // btnStartMapClick(Sender);

   if (cds_ExtBuffer_ebFrLoc.asstring = '') then begin
      Label44.Caption := '請輸入上車位置!';
      exit;
   end;

   if (cds_ExtBuffer_ebFrLng.asstring = '') then begin
      Label44.Caption := '沒有坐標,請先按定位!';
      exit;
   end;

   { if (cds_ExtBuffer_ebFrAddr.asstring = '') then
     begin
     ShowMessageDlag('沒有地址,請再確認!');
     Exit;
     end;
   }  { if cds_ExtBuffer_ebFrLng.AsString = '' then
     btnStartMapClick(Sender);

     if (cds_ExtBuffer_ebToLng.AsString = '') and (cds_ExtBuffer_ebToLoc.AsString <> '') then
     btnEndMapClick(Sender);
   }
   // TimerPollingPhone.Enabled := False; // jieshu 14/7/5 派車暫停inCall查詢
   try
      li_Qty := cds_ExtBuffer_ebTaxi.Value - cds_ExtBuffer_ebOkQty.Value;
      btnSendCar.flat := false;
      cds_ExtBuffer_ebFinish.AsBoolean := false;
      cds_ExtBuffer_ebResult.asstring := '';
      btnFinish.flat := false;
      // btnFinish.Enabled := false;
      screen.Cursor := crHourGlass;
      // 4.送出socket
      SocketSendData.sLoginID := LoginUser.sUserID;
      SocketSendData.sPassword := LoginUser.sPassword;
      SocketSendData.iMotorCadeID := MotorCade.iID;
      SocketSendData.iEXTNO := StrToIntDef(ExtNo.Caption, 1);
      SocketSendData.ipgid := StrToIntDef(EditPgid.Text, 0);
      SocketSendData.sName := cds_ExtBuffer_ebPsgrName.asstring;
      SocketSendData.sPhone := cds_ExtBuffer_ebPhone.asstring;
      SocketSendData.scxface := '';
      SocketSendData.scxfrPlace := cds_ExtBuffer_ebFrLoc.asstring;
      // SocketSendData.scxfraddr := cds_ExtBuffer_ebFrAddr.asstring;
      SocketSendData.scxfrPS := cds_ExtBuffer_ebFrLocRef.asstring;
      SocketSendData.icxfrlng := strtofloat(cds_ExtBuffer_ebFrLng.asstring);
      SocketSendData.icxfrlat := strtofloat(cds_ExtBuffer_ebFrLat.asstring);
      SocketSendData.scxtoPlace := cds_ExtBuffer_ebToLoc.asstring;
      // SocketSendData.scxtoaddr := cds_ExtBuffer_ebToAddr.asstring;
      SocketSendData.scxtoPS := cds_ExtBuffer_ebToLocRef.asstring;
      SocketSendData.icxtolng :=
          strtofloatdef(cds_ExtBuffer_ebToLng.asstring, 0);
      SocketSendData.icxtolat :=
          strtofloatdef(cds_ExtBuffer_ebToLat.asstring, 0);
      // jieshu 14/6/17 共用派車用
      SocketSendData.sSex := cds_ExtBuffer_ebSex.asstring;
      SocketSendData.iDataToDrv := cds_ExtBuffer_ebdatatodrv.AsInteger;

      if ComboBox4.ItemIndex = 0 then begin
         SocketSendData.sQunm := '';
         SocketSendData.iQupid := 0;
         quid := gt_quid[ComboBox4.ItemIndex];
         SocketSendData.iquid := 0;
      end
      else begin
         SocketSendData.sQunm := ComboBox4.Items[ComboBox4.ItemIndex];
         SocketSendData.iQupid := strtoint(gt_Queue[ComboBox4.ItemIndex]);
         quid := gt_quid[ComboBox4.ItemIndex];
         SocketSendData.iquid := strtoint(quid);
      end;

      for li_i := 1 to 3 do
         case li_i of
            1:
               Lcxno1.Caption := '';
            2:
               Lcxno2.Caption := '';
            3:
               Lcxno3.Caption := '';
         end;

      if cds_car_.Active then
         cds_car_.EmptyDataSet
      else
         cds_car_.CreateDataSet;

      writein(SocketSendData, quid, '', Sender <> ColorButton7);
      CancelPaich := false;
      for li_i := 1 to li_Qty do // 派多輛車 jieshu 14/7/4
      begin
         if CancelPaich = True then
            Break;

         cds_ExtBuffer_ebPaiche.AsBoolean := false;
         ls_Term := '';
         SocketSendData.sTermListName := '';
         for i := 0 to high(ls_Field) do begin
            lt_CB := TCheckBox(GroupBox4.FindChildControl('cbFlag' +
                IntToStr(i)));
            ls_Term := ls_Term + lf_Value(lt_CB.Checked) + ',';

            if lt_CB.Checked and
                (AnsiPos(lt_CB.Caption, SocketSendData.sTermListName) < 1) then
               if SocketSendData.sTermListName = '' then
                  SocketSendData.sTermListName := '， 車輛條件：' + lt_CB.Caption
               else
                  SocketSendData.sTermListName := SocketSendData.sTermListName +
                      ' ' + lt_CB.Caption;
         end;

         // 1.寫入table (生成派車單)
         SocketSendData.sTermList := ls_Term;
         // ls_Term := '';

         case li_i of
            1:
               Lcxno1.Caption := Editcxno.Text;
            2:
               Lcxno2.Caption := Editcxno.Text;
            3:
               Lcxno3.Caption := Editcxno.Text;
         end;

         iCurEXT := StrToIntDef(ExtNo.Caption, 1); // 都沒按分機的話，預設為1
         // if iCurEXT > 0 then
         // aImgLight[iCurEXT - 1].Hint := Editcxno.Text; // 紀錄派車單號

         // SocketSendData.icxno := StrToIntDef(Editcxno.Text, 0);

         // for 存檔 only, 存檔時, cxnoList 是空的
         if Self.cxnoList.Count > 0 then begin
            SocketSendData.icxno :=
                StrToIntDef(Self.cxnoList.Strings[li_i - 1], 0);
            Editcxno.Text := Self.cxnoList.Strings[li_i - 1];
            // CSR2 screen layout jieshu 14/6/2 未完工
            cds_ExtBuffer_ebCxno.AsInteger := SocketSendData.icxno;
         end;

         cds_ExtBuffer_ebCxnoList.asstring := cds_ExtBuffer_ebCxnoList.asstring
             + cds_ExtBuffer_ebCxno.asstring + ',';

         if gs_dcno <> '' then begin
            dm1.zq_DeferCall_.Edit;
            dm1.zq_DeferCall_.fieldbyname('dcCsidExec').asstring :=
                LoginUser.sUserID;
            dm1.zq_DeferCall_.fieldbyname('dcdtexec').AsDateTime := now;
            dm1.zq_DeferCall_.fieldbyname('cxno').AsInteger :=
                SocketSendData.icxno;
            dm1.zq_DeferCall_.Post;
         end;

         if not bCancelcar and (Sender <> ColorButton7) then
            if (SocketData.sRemortIP <> '') and (SocketData.iRemortPort <> 0)
            then begin
               gb_Finish := false;
               // 取得執行緒編號
               Self.ShowAddMessage(iCurEXT, '啟動執行緒送出派車。' + #10);
               StartThreads(100, SocketSendData); // state=100 發送派車需求
               // aPnlPhone[iCurEXT - 1].Tag
               SocketSendData.iThreadNo :=
                   StrToIntDef(editThreads.Text, -1) - 1;

               repeat
                  Sleep(200); // 兩筆之間 sleep 3 秒 jieshu 14/6/18
                  application.ProcessMessages;
               until gb_Finish or
                   (cds_ExtBuffer_ebTaxi.Value = cds_ExtBuffer_ebOkQty.Value);

               for i := 1 to 5 do begin
                  Sleep(200); // 兩筆之間 sleep 3 秒 jieshu 14/6/18
                  application.ProcessMessages;
               end;
            end;
      end;

      if cds_ExtBuffer_ebTaxi.Value <= frmSendCar.cds_ExtBuffer_ebOkQty.Value
      then
         cds_ExtBuffer_ebPaiche.Value := false
      else
         cds_ExtBuffer_ebPaiche.Value := True;

      if Sender = ColorButton7 then
         Label44.Caption := '乘客 ' + cds_ExtBuffer_ebPsgrName.asstring + ' 建檔成功';
   finally
      // TimerPollingPhone.Enabled := True; // jieshu 14/7/5 派車暫停inCall查詢
      screen.Cursor := crDefault; // 遊標復原
      btnFinish.flat := True;
      cds_ExtBuffer_ebFinish.AsBoolean := True;

      editStartPlace.Text := originalInput;
   end;

   application.ProcessMessages;
end;

procedure TfrmSendCar.SpeedButton1Click(Sender: TObject);
begin
   Self.ShowAddMessage(cds_ExtBuffer_ebExtNo.AsInteger, '執行預約完工。');
   // alabPhone[cds_ExtBuffer_ebExtNo.AsInteger - 1].Caption := '';
   // 14/5/31 完成、取消才能清電話 jieshu
   DeleteBuffer(cds_ExtBuffer_ebExtNo.AsInteger); // 分機暫存檔刪除
   ClearEdit;
   Close;
   // ClearPhoneStatus;
end;

procedure TfrmSendCar.StartThreads(iState: Integer; at_SSD: TSocketSendData);
var
   i: Integer;
   newThread: TClientThread;
begin
   lvStatus.Items.clear;

   i := StrToIntDef(editThreads.Text, 0);
   editThreads.Text := IntToStr(i + 1);

   iConnectionsMade := 0;
   iMaxConnections := 0;

   newThread := TClientThread.Create(True);
   // fThreads.Add(newThread);
   // with newThread do
   // begin
   newThread.FreeOnTerminate := True; // 自動銷燬
   newThread.ListItem := lvStatus.Items.Add;
   newThread.ListItem.Caption := editThreads.Text;
   newThread.ListItem.SubItems.Add('Creating');
   newThread.SleepTime := 30;
   // newThread.uiLock := Self.uiLock;
   newThread.State := iState;
   newThread.bCancelSocket := false;
   newThread.CreateClientSocket(SocketData, at_SSD);
   // W-140529 1.Table extbuffer 不再使用，改用 memory。
   // Resume;
   newThread.Start;
   // end;

end;

procedure TfrmSendCar.StopThreads;
begin

   if fThreads.Count > 0 then
      while fThreads.Count > 0 do begin
         TClientThread(fThreads[0]).FreeOnTerminate := True;
         TClientThread(fThreads[0]).Terminate;
         fThreads.Delete(0);
      end;
end;

procedure TfrmSendCar.Timer1Timer(Sender: TObject);
begin
   if gb_WebOK and (AnsiPos(ExtNo.Caption, '已派,再派') > 0) then begin
      Timer1.Enabled := false;
      ZoomToTwoPlaceBtn.Click;
   end;
end;

procedure TfrmSendCar.TimerReFireShowDriverAndCustomerTimer(Sender: TObject);
begin
   Self.TimerReFireShowDriverAndCustomer.Enabled := false;

   Self.ZoomToTwoPlaceBtnClick(nil);
end;

Function TfrmSendCar.EnCode_ebFrLoc: String;
begin

   Result := ComboBox9.Text + ComboBox10.Text + ComboBox11.Text;

   if Edit2.Text <> '' then
      Result := Result + Edit2.Text + '巷';

   if Edit3.Text <> '' then
      Result := Result + Edit3.Text + '弄';

   if Edit4.Text <> '' then
      Result := Result + Edit4.Text + '號';

   if Edit5.Text <> '' then
      Result := Result + '之' + Edit5.Text;

   if (Edit6.Text <> '') and (Edit5.Text <> '') then
      Result := Result + '，';

   if Edit6.Text <> '' then
      Result := Result + Edit6.Text + '樓';

   if Edit7.Text <> '' then
      Result := Result + '之' + Edit7.Text;

end;

procedure TfrmSendCar.btnStartMapClick(Sender: TObject);
var
   // sJSFn: string ;
   originalInput, stempAddress, sTemp, tmpStr, tmpAddr: string;
   aStream: TMemoryStream;
   lb_First: Boolean;
begin
   if RadioButton5.Checked then begin
      if Edit4.Text = '' then begin
         ShowMessageDlag('請輸入門牌號');
         Edit4.SetFocus;
         Abort;
      end;

      originalInput := editStartPlace.Text;
      cds_ExtBuffer_ebFrLoc.asstring := EnCode_ebFrLoc();
      editStartPlace.Text := EnCode_ebFrLoc();
   end
   else begin
      originalInput := editStartPlace.Text;
      tmpStr := editStartPlace.Text;
      tmpAddr := '';
      repeat
         if pos('號', tmpStr) > 0 then
            tmpAddr := tmpAddr + Fetch(tmpStr, '號') + '號'
         else
            tmpAddr := tmpAddr + Fetch(tmpStr, '號');
      until pos('號', tmpStr) = 0;
      cds_ExtBuffer_ebFrLoc.asstring := tmpAddr; // editStartPlace.Text;
   end;

   Self.queryAddressReplaceClick;

   // cds_ExtBuffer_ebFrLoc.asstring := originalInput;
   editStartPlace.Text := originalInput;
end;

procedure TfrmSendCar.FormActivate(Sender: TObject);
var
   li_w, li_T, li_L, li_No, i: Integer;
begin
   if gb_First then begin
      gb_First := false;
      readini;
      dm1.zqTemp.Close;
      dm1.zqTemp.SQL.Text := 'select a1 from addr1';
      dm1.zqTemp.open();

      while not dm1.zqTemp.Eof do begin
         ComboBox9.Items.Add(dm1.zqTemp.Fields[0].asstring);
         dm1.zqTemp.next;
      end;

      ComboBox12.Items.Assign(ComboBox9.Items);
      ComboBox9.ItemIndex := ComboBox9.Items.IndexOf(MotorCade.smccity);
      ComboBox12.ItemIndex := ComboBox9.ItemIndex;
      ComboBox9Change(ComboBox9);
      ComboBox12Change(ComboBox12);

      Label2.Caption := 'Ver：' + gf_GetFileVersion(ParamStr(0)) + #13#10 +
          SocketData.sRemortIP;
      // W-140529 1.Table extbuffer 不再使用，改用 memory。
      // Marked by Dennies in 2016/3/29, using 10 Seattle design time create dataset
      // feature to create in advance, but not in run-tim to speed up the launching
      // procedure.
      // cds_ExtBuffer_.CreateDataSet;
      cds_ExtBuffer_.append;
      li_w := Width div 3;
      PnlHist1.Width := li_w;
      PnlHist3.Width := li_w;
      PageControl1.ActivePageIndex := 0;

      if sLanguage = 'zh-cn' then
         big5TOgbk(TForm(Sender));
      // CSR2 screen layout jieshu 14/6/2 車輛
      gd_CarTime := now;
      gp_ShowCarInfo;
      gp_ShowDefer; // jieshu 14/6/16 預約資訊
      // 14/6/14 jieshu 紀錄Log
      { //20151209 停用
        gs_LogPath := ExtractFilePath(ParamStr(0));

        if gs_LogPath[Length(gs_LogPath)] <> '\' then
        gs_LogPath := gs_LogPath + '\';

        gs_LogPath := gs_LogPath + 'CSR-LOG\';

        if not DirectoryExists(gs_LogPath) then
        ForceDirectories(gs_LogPath);
      }

      gs_dcno := '';
      // TabSheet3.TabVisible := false;
      Self.PageControl1.Pages[2].Free;
      Self.PageControl1.ActivePageIndex := 0;

      if ParamCount >= 1 then begin
         ExtNo.Caption := ParamStr(1);
         cds_ExtBuffer_ebPhone.asstring := ParamStr(2);

         if ParamStr(1) = '已派' then begin
            Self.Panel14.Visible := false;
            ColorButton3.Enabled := false;
            PageControl1.Visible := false;
            Panel12.Visible := True;
            dm1.qyCalltx.Close;
            dm1.qyCalltx.SQL.Text :=
                'select a.cxfrloc, a.cxfrlocref, a.cxfrlong, a.cxfrlat, a.cxtoloc,'
                + '       a.cxtolocref, a.cxtolong, a.cxtolat, a.cxpgphone, b.pgnm,'
                + '       b.pggender, b.pgdatatodrv, a.dvid, cxdtconfirm, cxcfmlong, cxcfmlat,'
                + '       cxdtinplace, cxinlong, cxinlat, cxdtpickup, cxonlong, cxonlat,'
                + '       cxdtfinish, cxoflong, cxoflat, a.dvno, a.cxtimetopickup, c.dvphone,'
                + '       a.quid, d.qunm, a.csid, e.asnm, a.cxoutlineno';

            for i := 0 to High(ls_FieldCx) do
               dm1.qyCalltx.SQL.Text := dm1.qyCalltx.SQL.Text + ',' +
                   ls_FieldCx[i];

            dm1.qyCalltx.SQL.Text := dm1.qyCalltx.SQL.Text +
                '  from Calltx a left join psgr b on a.pgid = b.pgid' +
                '  left join drv c on a.dvid = c.dvid left join queue d' +
                '    on a.mcid = d.mcid and a.quid = d.quid left join agentstation e'
                + '    on a.mcid = e.mcid and a.asno = e.asno' +
                ' where cxno = ' + ParamStr(3);
            dm1.qyCalltx.open();
            cds_ExtBuffer_ebDvLat.asstring := '0';
            cds_ExtBuffer_ebDvLng.asstring := '0';
            cds_ExtBuffer_ebFrLoc.asstring :=
                dm1.qyCalltx.fieldbyname('cxfrloc').asstring;
            cds_ExtBuffer_ebFrLocRef.asstring :=
                dm1.qyCalltx.fieldbyname('cxfrlocref').asstring;
            cds_ExtBuffer_ebFrLng.asstring :=
                dm1.qyCalltx.fieldbyname('cxfrlong').asstring;
            cds_ExtBuffer_ebFrLat.asstring :=
                dm1.qyCalltx.fieldbyname('cxfrlat').asstring;
            cds_ExtBuffer_ebToLoc.asstring :=
                dm1.qyCalltx.fieldbyname('cxtoloc').asstring;
            cds_ExtBuffer_ebToLocRef.asstring :=
                dm1.qyCalltx.fieldbyname('cxtolocref').asstring;
            cds_ExtBuffer_ebToLng.asstring :=
                dm1.qyCalltx.fieldbyname('cxtolong').asstring;
            cds_ExtBuffer_ebToLat.asstring :=
                dm1.qyCalltx.fieldbyname('cxtolat').asstring;
            cds_ExtBuffer_ebPhone.asstring :=
                dm1.qyCalltx.fieldbyname('cxpgphone').asstring;
            cds_ExtBuffer_ebPsgrName.asstring :=
                dm1.qyCalltx.fieldbyname('pgnm').asstring;
            cds_ExtBuffer_ebSex.asstring :=
                dm1.qyCalltx.fieldbyname('pggender').asstring;
            cds_ExtBuffer_ebdatatodrv.asstring :=
                dm1.qyCalltx.fieldbyname('pgdatatodrv').asstring;
            cds_ExtBuffer_ebDvid.AsInteger := dm1.qyCalltx.fieldbyname('dvid')
                .AsInteger;

            for i := 0 to High(ls_FieldCx) do
               TCheckBox(GroupBox4.FindChildControl('cbFlag' + IntToStr(i)))
                   .Checked := dm1.qyCalltx.fieldbyname(ls_FieldCx[i])
                   .asstring = 'Y';

            if not dm1.qyCalltx.fieldbyname('cxdtconfirm').IsNull then
               Label22.Caption := '接單  ' + formatdatetime('mm/dd hh:nn',
                   dm1.qyCalltx.fieldbyname('cxdtconfirm').AsDateTime);

            if not dm1.qyCalltx.fieldbyname('cxdtinplace').IsNull then
               Label23.Caption := '定點  ' + formatdatetime('mm/dd hh:nn',
                   dm1.qyCalltx.fieldbyname('cxdtinplace').AsDateTime);

            if not dm1.qyCalltx.fieldbyname('cxdtpickup').IsNull then
               Label33.Caption := '接到  ' + formatdatetime('mm/dd hh:nn',
                   dm1.qyCalltx.fieldbyname('cxdtpickup').AsDateTime);

            if not dm1.qyCalltx.fieldbyname('cxdtfinish').IsNull then
               Label34.Caption := '完成  ' + formatdatetime('mm/dd hh:nn',
                   dm1.qyCalltx.fieldbyname('cxdtfinish').AsDateTime);

            Label35.Caption := '司機台號：' + dm1.qyCalltx.fieldbyname
                ('dvno').asstring;
            Label36.Caption := '到達分鐘：' +
                IntToStr(Ceil(dm1.qyCalltx.fieldbyname('cxtimetopickup')
                .AsFloat)) + ' 分';
            Label37.Caption := '司機電話：' + dm1.qyCalltx.fieldbyname
                ('dvphone').asstring;

            if dm1.qyCalltx.fieldbyname('quid').asstring <> '0' then begin
               Label27.Caption := dm1.qyCalltx.fieldbyname('quid').asstring +
                   ' ' + dm1.qyCalltx.fieldbyname('qunm').asstring
            end
            else begin
               Label27.Caption := '';
            end;
            Label42.Caption := dm1.qyCalltx.fieldbyname('csid').asstring;
            Label40.Caption := '特約站名：' + dm1.qyCalltx.fieldbyname
                ('asnm').asstring;
            Label41.Caption := '外線 ' + dm1.qyCalltx.fieldbyname
                ('cxoutlineno').asstring;
            { editStartPlace.ReadOnly := True;
              editStartPS.ReadOnly := True;
              editPhone.ReadOnly := True;
              editName.ReadOnly := True;
            } end
         else if ParamStr(1) = '再派' then begin
            FormStyle := fsStayOnTop;
            ColorButton6.Enabled := false;
            ColorButton2.Enabled := false;
            dm1.q_Recall.Close;
            dm1.q_Recall.SQL.Text :=
                'select rcdtcreate, rcpgphone, rcpgname, rcfrloc, rcfrlocref, rcresult, rccsidexec,'
                + '       rcdtfinish, rcno, cxno, cxtimetopickup, rcFrLong, rcFrLat, rcToLoc,'
                + '       rcToLocRef, rcToLong, rcToLat, a.dvno, a.asno from recall a'
                + ' where rcno = ' + ParamStr(3);
            dm1.q_Recall.open();

            cds_ExtBuffer_ebPhone.asstring :=
                dm1.q_Recall.fieldbyname('rcpgphone').asstring;
            cds_ExtBuffer_ebPsgrName.asstring :=
                dm1.q_Recall.fieldbyname('rcpgname').asstring;
            cds_ExtBuffer_ebFrLoc.asstring :=
                dm1.q_Recall.fieldbyname('rcfrloc').asstring;
            cds_ExtBuffer_ebFrLocRef.asstring :=
                dm1.q_Recall.fieldbyname('rcfrlocref').asstring;
            cds_ExtBuffer_ebFrLng.asstring :=
                dm1.q_Recall.fieldbyname('rcFrLong').asstring;
            cds_ExtBuffer_ebFrLat.asstring :=
                dm1.q_Recall.fieldbyname('rcFrLat').asstring;
            cds_ExtBuffer_ebToLoc.asstring :=
                dm1.q_Recall.fieldbyname('rcToLoc').asstring;
            cds_ExtBuffer_ebToLocRef.asstring :=
                dm1.q_Recall.fieldbyname('rcToLocRef').asstring;
            cds_ExtBuffer_ebToLng.asstring :=
                dm1.q_Recall.fieldbyname('rcToLong').asstring;
            cds_ExtBuffer_ebToLat.asstring :=
                dm1.q_Recall.fieldbyname('rcToLat').asstring;

            Self.PageControl1.Pages[0].Free;
            Self.PageControl1.Pages[0].Free;
            // TabSheet1.TabVisible := false;
            // TabSheet2.TabVisible := false;
            TabSheet3.TabVisible := True;
            Self.PageControl1.ActivePageIndex := 0;

            if dm1.q_Recall.fieldbyname('asno').asstring <> '' then begin
               dm1.zqTemp.Close;
               dm1.zqTemp.SQL.Text :=
                   'select asnm from agentstation where mcid = ' +
                   IntToStr(MotorCade.iID) + ' and asno = ' +
                   quotedstr(dm1.q_Recall.fieldbyname('asno').asstring);
               dm1.zqTemp.open();
               Edit1.Text := dm1.zqTemp.fieldbyname('asnm').asstring + '(' +
                   dm1.q_Recall.fieldbyname('asno').asstring + ')';
            end;
         end
         // else if ParamCount > 2 then
         else if ParamStr(1) = '預約' then begin
            // ExtNo.Caption := '預約';
            gs_dcno := ParamStr(3);
            dm1.zq_DeferCall_.Close;
            dm1.zq_DeferCall_.SQL.Text := 'select * from DeferCall' +
                ' where dcno = ' + ParamStr(3);
            dm1.zq_DeferCall_.open();
            cds_ExtBuffer_ebFrLoc.asstring := dm1.zq_DeferCall_.fieldbyname
                ('dcFrLoc').asstring;
            cds_ExtBuffer_ebFrLocRef.asstring := dm1.zq_DeferCall_.fieldbyname
                ('dcFrLocRef').asstring;
            cds_ExtBuffer_ebFrLng.asstring := dm1.zq_DeferCall_.fieldbyname
                ('dcFrLong').asstring;
            cds_ExtBuffer_ebFrLat.asstring := dm1.zq_DeferCall_.fieldbyname
                ('dcFrLat').asstring;
            cds_ExtBuffer_ebToLoc.asstring := dm1.zq_DeferCall_.fieldbyname
                ('dcToLoc').asstring;
            cds_ExtBuffer_ebToLocRef.asstring := dm1.zq_DeferCall_.fieldbyname
                ('dcToLocRef').asstring;
            cds_ExtBuffer_ebToLng.asstring := dm1.zq_DeferCall_.fieldbyname
                ('dcToLong').asstring;
            cds_ExtBuffer_ebToLat.asstring := dm1.zq_DeferCall_.fieldbyname
                ('dcToLat').asstring;
            SocketSendData.idcno := strtoint(ParamStr(3));
         end;
      end
      else
         ExtNo.Caption := '1'; // 預設為1線

      if (cds_ExtBuffer_ebPhone.asstring = '') then begin
         Self.editPhone.SetFocus;
      end;

      // 加入司機的台號
      with dm1 do begin
         qyDrv.Close;
         qyDrv.SQL.Text := 'select * from drv where mcid=' +
             IntToStr(MotorCade.iID);
         qyDrv.open;
         // cmbDrvNO.Items.clear;
         ComboBox16.Items.clear;
         ComboBox19.Items.clear;
         ComboBox20.Items.clear;

         while not qyDrv.Eof do begin
            // cmbDrvNO.Items.Add(qyDrv.fieldbyname('dvno').asstring + ' ' +
            // qyDrv.fieldbyname('dvnm').asstring);
            ComboBox16.Items.Add(qyDrv.fieldbyname('dvno').asstring + ' ' +
                qyDrv.fieldbyname('dvnm').asstring);
            ComboBox19.Items.Add(qyDrv.fieldbyname('dvno').asstring + ' ' +
                qyDrv.fieldbyname('dvnm').asstring);
            ComboBox20.Items.Add(qyDrv.fieldbyname('dvno').asstring + ' ' +
                qyDrv.fieldbyname('dvnm').asstring);

            qyDrv.next;
         end;

         qyDrv.Close;
      end;

      if AnsiPos(ExtNo.Caption, '已派,再派,預約') > 0 then begin
         ExtNo.Font.Size := 24;
         TForm(Sender).Caption := ParamStr(2) + ExtNo.Caption;
         li_No := 999;

         for i := 0 to High(ls_Field) do
            TCheckBox(GroupBox4.FindChildControl('cbFlag' + IntToStr(i)))
                .Enabled := false;

         if ExtNo.Caption <> '預約' then begin
            ColorButton6.Enabled := false;
            ColorButton2.Enabled := false;
            ColorButton4.Enabled := false;
            // sbtCommon1.Enabled := false;
            sbtCommon2.Enabled := false;
            ComboBox5.Enabled := false;
            ComboBox6.Enabled := false;
            SearchFromAddressBtn.Enabled := false;
            SearchToAddressBtn.Enabled := false;
         end;
      end
      else begin
         TForm(Sender).Caption := ExtNo.Caption + '線派車';
         li_No := strtoint(ExtNo.Caption);
      end;

      cds_ExtBuffer_ebExtNo.AsInteger := li_No;

      if AnsiPos(ExtNo.Caption, '已派,再派') > 0 then begin
         cds_ExtBuffer_.Post;
         cds_ExtBuffer_.ReadOnly := True;
         ds_ExtBuffer.AutoEdit := false;
      end;

      if li_No = 999 then
         li_No := 1;

      li_T := Trunc((li_No - 1) / 4);
      li_L := (li_No - 1) mod 4;

      top := 25 + li_T * 40;
      left := 180 + li_L * 80;

      if (cds_ExtBuffer_ebPhone.asstring <> '') and
          (AnsiPos(ExtNo.Caption, '已派,再派') < 1) then
         ColorButton6.Click;
      // editStartPlace.SetFocus;
      editStartPlace.SelStart := Length(editStartPlace.Text);

      if gs_dcno <> '' then begin
         Self.Refresh;
         application.ProcessMessages;
         // btnSendCar.Click;
      end;
   end;
end;

procedure TfrmSendCar.Do_CloseRtn;
var
   i, iEXTNO, icxno: Integer;
begin
   // 2014/05/14 改為永遠都要有一個紅色編輯狀態，也就是不要自動變灰色
   // “完工”鍵，按了之後，相對的 “外線鍵”號碼去掉，變成灰色，清除 三個紅圈
   i := StrToIntDef(ExtNo.Caption, 99);
   iEXTNO := i; // 0;

   Self.ShowAddMessage(iEXTNO, '執行派車完工。');
   icxno := SocketSendData.icxno;
   // StrToIntDef(aImgLight[iEXTNO - 1].Hint, 0);

   if pos('目前無車可派', cds_ExtBuffer_ebResult.asstring) > 0 then
      updatecalltx(1, icxno, 0); // 將calltx.cxsuccess=0 （1:成功,0:失败，3：乘客取消)

   DeleteBuffer(iEXTNO); // 分機暫存檔刪除
   ClearEdit;
   // btnFinish.Enabled := false;
   cds_ExtBuffer_ebFinish.AsBoolean := false;

end;

procedure TfrmSendCar.btnFinishClick(Sender: TObject);
Var
   i, iThreadNo: Integer;
   PSStepFlag: String;
begin
   if (fThreads.Count > 0) and (Not gb_Finish) then begin
      if gf_ShowMsg('確定要取消派車嗎？', 'CSR', mtConfirmation, 1, 128, Self) = mrNo
      then
         exit;

      WriteUsrtx(MotorCade.iID, LoginUser.sUserID, '緊急取消一般派車',
          '客戶電話:' + SocketSendData.sPhone,
          '上車地點:' + SocketSendData.scxfrPlace, '');
      iThreadNo := SocketSendData.iThreadNo;
      CancelPaich := True;
      with TClientThread(fThreads[iThreadNo]) Do begin
         PSStepFlag := sStepFlag;
         bCancelSocket := True;
      end;

      for i := 1 to 3 do begin
         case i of
            1:
               if Lcxno1.Caption <> '' Then
                  Editcxno.Text := Lcxno1.Caption;
            2:
               if Lcxno2.Caption <> '' Then
                  Editcxno.Text := Lcxno2.Caption;
            3:
               if Lcxno3.Caption <> '' Then
                  Editcxno.Text := Lcxno3.Caption;
         end;
         if Self.cxnoList.Strings[i] <> '' then begin
            dm1.gp_ExecuteSQL
                ('update calltx set dvno = "@@@",cxsuccess = 3 , cxdtcsrclick = NOW() where cxno = '
                + Self.cxnoList.Strings[i] + ' and dvno is null');
         end;

      end;

      if PSStepFlag = 'PS排班模式' then begin
         dm1.gp_ExecuteSQL('update queuetx set cxno = null where mcid= ' +
             IntToStr(SocketSendData.iMotorCadeID) + ' and cxno=' +
             IntToStr(SocketSendData.icxno));
      end;
      btnCancelSendCar.Enabled := false;

   end
   else begin
      Do_CloseRtn;
      Close;
   end;
end;

procedure TfrmSendCar.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   if Not gb_Finish then begin
      ShowMessage('派車作業處理中,無法關閉');
      Abort;
   end;

   bExit := True; // 程式離開
   with dm1 do begin
      qyIncall.Close;
      qy1.Close;
      qyPsgr.Close;
      qypsgr_locbk.Close;
      // zconMySQL.Disconnect;
      FDConnection1.Connected := false;
   end;

   Action := cafree;
end;

procedure TfrmSendCar.FormCreate(Sender: TObject);
var
   Reg: TRegistry;
begin
   {
     Reg := TRegistry.Create;
     try
     Reg.RootKey := HKEY_Current_User;
     if Reg.OpenKey('\SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION', false) then begin
     Reg.WriteInteger('TK.exe', 11999);
     end;

     Reg.RootKey := HKEY_Local_Machine;
     if Reg.OpenKey('\SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION', false) then begin
     Reg.WriteInteger('TK.exe', 11999);
     end;
     finally
     Reg.Free;
     end;
   }
   gb_WebOK := false;
   gb_First := True;
   gb_Finish := True;
   // 2.建立電話物件
   CreatePhoneObject; // 建立電話物件
   manualFillInCarCount := 1;

   bExit := false; // 程式離開
   uiLock := TCriticalSection.Create;
   fThreads := TList.Create;
   gt_Queue := TStringList.Create;
   gt_quid := TStringList.Create;
   cxnoList := TStringList.Create;

   EditHWND.Text := IntToStr(iApHWND); // application handle

   // frmAddList := TfrmAddlist.Create(Self);
   // frmAddList.Hide;

   // 3.建立相關資要表
   with dm1 do begin
      qyPsgr.Close;
      qyPsgr.SQL.clear;
      qyPsgr.SQL.Text := 'select * from Psgr where mcid=' +
          IntToStr(MotorCade.iID); // 屬於該車隊的資料
      // qypsgr.open;  //先不open

      qypsgr_locbk.Close;
      // qypsgr_locbk.SQL.Text;
      qypsgr_locbk.datasource := dspsgr;
      qypsgr_locbk.SQL.Text :=
          'select * from psgr_locbk  order by lbdtlast desc';
      // qypsgr_locbk.open;  //先不open

      qyCalltx.Close;
      qyCalltx.SQL.clear;
      qyCalltx.SQL.Text := 'select * from Calltx where mcid=' +
          IntToStr(MotorCade.iID); // 屬於該車隊的資料
      // qyCalltx.open;  //先不open

   end;
end;

procedure TfrmSendCar.FormDestroy(Sender: TObject);
begin
   cxnoList.Free;
   uiLock.Free;
   gt_Queue.Free;
   gt_quid.Free;
end;

procedure TfrmSendCar.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if Key = #13 then begin
      Key := #0;
      gb_NotEnter := True;
      try
         perform(wm_NextDlgCtl, 0, 0);
      finally
         gb_NotEnter := false;
      end;
   end
   else if not CharInSet(Key, ['0' .. '9', '+', Chr(VK_Delete), Chr(VK_BACK)])
       and (Sender = editPhone) then
      Key := #0;
end;

procedure TfrmSendCar.FormResize(Sender: TObject);
var
   li_w: Integer;
begin
   li_w := Width div 3;
   PnlHist1.Width := li_w;
   PnlHist3.Width := li_w;
end;

procedure TfrmSendCar.CheckBoxTrafficClick(Sender: TObject);
begin
   { if CheckBoxTraffic.Checked then
     HTMLWindow2.execScript('TrafficOn()', 'JavaScript')
     else
     HTMLWindow2.execScript('TrafficOff()', 'JavaScript');
   } end;

procedure TfrmSendCar.CheckBoxStreeViewClick(Sender: TObject);
begin
   { if CheckBoxStreeView.Checked then
     HTMLWindow2.execScript('StreetViewOn()', 'JavaScript')
     else
     HTMLWindow2.execScript('StreetViewOff()', 'JavaScript');
   } end;

procedure TfrmSendCar.ApplicationEvents1Exception(Sender: TObject;
    E: Exception);
begin
   Self.ShowAddMessage(StrToIntDef(ExtNo.Caption, 0), '非預期錯誤：' + E.Message);

   if pos('Access violation', E.Message) > 0 then
      // else if pos('Lost connection to MySQL', E.Message) > 0 then
      // gf_ShowMsg('電腦中心連線不成功，請檢查網路情況', 'CSR', mtWarning, 0, 128, Self)
   else if AnsiPos('Lost connection', E.Message) > 0 then
      gf_ShowMsg('資料庫聯繫發生問題，請先確認電腦是否上網。'#13#10'系統錯誤訊息：非預期錯誤：' + E.Message,
          'CSR', mtWarning, 0, 128, Self)
   else
      gf_ShowMsg('非預期錯誤：' + E.Message, 'CSR', mtWarning, 0, 128, Self);
end;

procedure TfrmSendCar.btnCancelSendCarClick(Sender: TObject);
var
   icxno, iEXTNO, iThreadNo: Integer;
begin
   if gf_ShowMsg('確定要取消派車嗎？', 'CSR', mtConfirmation, 1, 128, Self) = mrNo then
      exit;
   // CSR 2.2.1 Issue 10.	取消派车成功，必须 设定 cxdtcsrclick 的时间。
   cds_car_.first;

   WriteUsrtx(MotorCade.iID, LoginUser.sUserID, '取消一般派車',
       '客戶電話:' + SocketSendData.sPhone, '上車地點:' + SocketSendData.scxfrPlace,
       '司機台號 = ' + cds_car_.fieldbyname('dvno').asstring);

   SocketSendData.icxno := cds_ExtBuffer_ebCxno.AsInteger;
   iEXTNO := StrToIntDef(ExtNo.Caption, 1);
   // 如果最近的一筆，乘客還未接到，則出現紅色 X，按下去可以取消該派車。
   if (SocketData.sRemortIP <> '') then begin
      // SocketSendData.idvid := StrToIntDef(aPnlPhone[iEXTNO - 1].Hint, -99);
      // 司機ID
      if SocketSendData.idvid = -1 then // thread尚未回傳
      begin
         iThreadNo := SocketSendData.iThreadNo;
         // aPnlPhone[iEXTNO - 1].Tag;
         if iThreadNo > 0 then
            TClientThread(fThreads[iThreadNo]).bCancelSocket := True;
         // 通知thread停止
         // aPnlPhone[iEXTNO - 1].Tag
         SocketSendData.iThreadNo := -1; // 復原
         Self.ShowAddMessage(iEXTNO, '執行緒尚未回傳，通知取消派車。');
      end
      else if SocketSendData.idvid > 0 then begin
         SocketSendData.iEXTNO := iEXTNO; // 切換後要重給
         Self.ShowAddMessage(iEXTNO, '啟動執行緒送出取消派車。');
         StartThreads(200, SocketSendData); // state=200 發送派車取消需求
         btnCancelSendCar.Enabled := false;
      end;
   end;
   /// ////////////////////////////////////////////////////////////////////////////////////////
   if (trim(cds_ExtBuffer_ebPhone.asstring) = '') or (Editcxno.Text = '') then
      exit;

   icxno := StrToIntDef(Editcxno.Text, 0);
   updatecalltx(1, icxno, 3); // 將calltx.cxsuccess=3 代表取消
   DeleteBuffer(iEXTNO); // 分機暫存檔刪除
   // alabPhone[iEXTNO - 1].Caption := ''; // 14/5/31 完成、取消才能清電話 jieshu
   ClearEdit;
   // ClearPhoneStatus;
end;

procedure TfrmSendCar.btnEndMapClick(Sender: TObject);
var
   // sJSFn: string ;, s1, s2
   stempAddress, sTemp: string;
begin
   if RadioButton7.Checked then begin
      if Edit10.Text = '' then begin
         ShowMessageDlag('請輸入門牌號');
         Abort;
      end;

      cds_ExtBuffer_.Edit;
      cds_ExtBuffer_ebToLoc.asstring := ComboBox12.Text + ComboBox13.Text +
          ComboBox14.Text;

      if Edit8.Text <> '' then
         cds_ExtBuffer_ebToLoc.asstring := cds_ExtBuffer_ebToLoc.asstring +
             Edit8.Text + '巷';

      if Edit9.Text <> '' then
         cds_ExtBuffer_ebToLoc.asstring := cds_ExtBuffer_ebToLoc.asstring +
             Edit9.Text + '弄';

      cds_ExtBuffer_ebToLoc.asstring := cds_ExtBuffer_ebToLoc.asstring +
          Edit10.Text + '號';

      if Edit11.Text <> '' then
         cds_ExtBuffer_ebToLoc.asstring := cds_ExtBuffer_ebToLoc.asstring + '之'
             + Edit11.Text;

      if (Edit12.Text <> '') and (Edit11.Text <> '') then
         cds_ExtBuffer_ebToLoc.asstring := cds_ExtBuffer_ebToLoc.asstring + '，';

      if Edit12.Text <> '' then
         cds_ExtBuffer_ebToLoc.asstring := cds_ExtBuffer_ebToLoc.asstring +
             Edit12.Text + '樓';

      if Edit13.Text <> '' then
         cds_ExtBuffer_ebToLoc.asstring := cds_ExtBuffer_ebToLoc.asstring + '之'
             + Edit13.Text;
   end;

   gp_GetAddrNo(cds_ExtBuffer_ebToLoc.asstring);

   if editEndPlace = ActiveControl then
      editEndPlaceExit(nil);

   ShowAddMessage(StrToIntDef(ExtNo.Caption, 0), '執行下車地點定位');
   RadioButton1.Checked := false;

   if cds_ExtBuffer_.State <> dsInsert then
      cds_ExtBuffer_.Edit;

   if ActiveControl = editEndPlace then
      gp_ValidateInput;

   screen.Cursor := crHourGlass;
   // HTMLWindow2.execScript('ClearMarkers()', 'JavaScript'); // 清除圖標
   try
      // W-140530 8. 目前查地址有 bug
      if (trim(cds_ExtBuffer_ebToLoc.asstring) = '') then
         // and (cds_ExtBuffer_ebToAddr.asstring = '') then
         exit;

      // if (trim(cds_ExtBuffer_ebToLoc.asstring) <> '') then
      sTemp := cds_ExtBuffer_ebToLoc.asstring;
      editLongitudeE.Text := '';

      // if (trim(cds_ExtBuffer_ebToAddr.asstring) <> '') then
      // sTemp := cds_ExtBuffer_ebToAddr.asstring;
      if CheckBox2.Checked then
         gf_CheckMapData(sTemp, false); // 補充圖資 jieshu 14/7/11

      if editLongitudeE.Text = '' then // 補充圖資 jieshu 14/7/11
         stempAddress := gotoaddress(sTemp, True);

      if not(cds_ExtBuffer_.State in [dsInsert, dsEdit]) then
         cds_ExtBuffer_.Edit;

      if editLongitudeE.Text = '' then // 找不到時....
      begin
         cds_ExtBuffer_ebToLng.asstring := '';
         cds_ExtBuffer_ebToLat.asstring := '';
      end
      else begin
         // 因為有時查找的地點會在Google的回覆字串中，要再次處理
         { if pos(cds_ExtBuffer_ebToLoc.asstring, stempAddress) > 0 then
           cds_ExtBuffer_ebToAddr.asstring :=
           Copy(stempAddress, 1, pos(cds_ExtBuffer_ebToLoc.asstring,
           stempAddress) - 1)
           else
           cds_ExtBuffer_ebToAddr.asstring := stempAddress;
         }
         cds_ExtBuffer_ebToLng.asstring := editLongitudeE.Text;
         cds_ExtBuffer_ebToLat.asstring := editLatitudeE.Text

      end;

      // 若開始定位沒按仍能自動執行
      // 如果有出發座標》標出圖標
      if (strtofloatdef(cds_ExtBuffer_ebFrLat.asstring, 0) > 0) and
          (strtofloatdef(cds_ExtBuffer_ebFrLng.asstring, 0) > 0) then
         HTMLWindow2.execScript(Format('PutMarkerIco(%s,%s,%s,%d,%s)',
             [cds_ExtBuffer_ebFrLat.asstring, cds_ExtBuffer_ebFrLng.asstring,
             quotedstr('上車點'), 6, quotedstr(sImagePath)]), 'JavaScript');
      { else
        btnStartMap.OnClick(self);
      }
      // 如果有目的地座標》標出圖標
      if (strtofloatdef(cds_ExtBuffer_ebToLat.asstring, 0) > 0) and
          (strtofloatdef(cds_ExtBuffer_ebToLng.asstring, 0) > 0) then
         HTMLWindow2.execScript(Format('PutMarkerIco(%s,%s,%s,%d,%s)',
             [cds_ExtBuffer_ebToLat.asstring, cds_ExtBuffer_ebToLng.asstring,
             quotedstr('下車點'), 7, quotedstr(sImagePath)]), 'JavaScript');

      showPosition; // 把地圖的範圍拉到二個圖標的大小
   finally
      screen.Cursor := crDefault;
   end;

end;

procedure TfrmSendCar.lp_AddCar(as_dvno: String; ar_min: String);
begin
   if frmSendCar.cds_car_.recordcount > 2 then
      exit;

   frmSendCar.cds_car_.append;
   frmSendCar.cds_car_.fieldbyname('dvno').asstring := as_dvno;
   frmSendCar.cds_car_.fieldbyname('ToMin').asstring := ar_min;
   frmSendCar.cds_car_.Post;
end;

{ TDBGrid }

procedure TDBGrid.UpdateScrollBar;
begin
   // inherited;

end;

end.
