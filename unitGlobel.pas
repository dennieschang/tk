﻿// W-140530 2．	CSR 按 “派车”bug：已有电话，却说没电话。另外，这个讯息框里，字太小。
unit unitGlobel;

interface

uses
   Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
   SHDocVw, Math, wininet, OleCtrls, MSHTML, Dialogs, Mask, inifiles, ComObj,
   ExtCtrls, Printers, ActiveX, HttpApp, Buttons, ComCtrls, StdCtrls, Menus;

const

   // EARTH_RADIUS = 6371000; //地球半徑
   EARTH_RADIUS = 6378137; // 地球半徑
   System_INI_FileName = 'maininfo.ini';

   // 車行資料
type
   TMotorCade = Record
      iID: integer;
      sName: string;
      sShortName: string;
      smccity: string; // 所在city
      rLng: Real;
      rlat: Real;
      imcminperkm: Double;
      imcbidtimeout: integer;
      mcticketcity: String;
   end;

type // 登錄者資料
   TLoginUser = Record
      sUserID: string;
      sUserName: string;
      sPassWord: string;
      sPhone: string;
      sRole: string;
      iStatus: integer;
   end;

type
   TSocketData = record
      sRemortIP: string; // IP address
      sLocalPort: string;
      iRemortPort: word;
      iTimeout: integer;
   end;

   // 派車封包內容
type
   TSocketSendData = record
      iMotorCadeID: integer;
      iEXTNO: integer;
      icxgotime, icxgodis: integer; // 乘客出發地至目的地的時間距離
      icxno, ipgid: integer; // 派車單號 // 乘客代碼
      sName, sPhone, scxface, scxfrPlace, scxfrPS, scxtoPlace, scxtoPS: string;
      // 乘車資料
      isex: integer; // , scxfraddr, scxtoaddr
      icxfrlng, icxfrlat, icxtolng, icxtolat, iDvLng, iDvLat: Double; // 乘客座標
      iRouteTimes, iRoutedist: integer; // 路線所需距離時間
      sLoginID, sPassWord: string; //
      iReply: integer; // 0:派車失敗 1:派車通知成功// 狀態 phoneTag
      idvid: integer; // 派車成功後給的司機ID
      sSex: String; // 用來共用派車
      idcno: integer; // 預約單號
      iThreadNo: integer;
      iDataToDrv: SmallInt;
      iQupid, iquid, itimetopickup: integer;
      sQunm, sDvno: String;
      sTermList: String; // Y N Y NN
      sTermListName: String; // 新車、無煙、RV車、載輪椅、載寵物
   end;

function CheckReservedWord(Str: string): string;
function GetDistance(lng1, lat1, lng2, lat2: Double): Double;
function DistanceBetweenPoint2(const StartLong, StartLat, EndLong,
    EndLat: Double): Double;
function LoadansistringFromFile(anFile: string): AnsiString;

function GetLocaleLang: string;

/// /////////////////////////////////////////////////////////////////////////
Function GBCht2Chs(sBIG5Str: String): unicodeString; // 繁体转简体
procedure big5TOgbk(Aform: Tform); // 整個視窗進入時轉換
procedure ShowMessageDlag(smsg: string);
/// /////////////////////////////////////////////////////////////////////////

var
   HTMLWindow2: IHTMLWindow2;
   MotorCade: TMotorCade; // 登入車隊資料
   LoginUser: TLoginUser; // 登入者資料
   SocketData: TSocketData; // SOCKET資料
   icseqno: integer = -99; // 每0.3秒取得的icseqno
   sLanguage: string;
   sFilePath, sImagePath: string;
   iVTA_MaxScrolBar: integer;
   sPCName: string;
   iApHWND: integer; // 本程式HANDLE值

implementation

uses CDMsg, DataModule, FireDAC.Comp.Client;

/// ////////////////////////////////////////////////////////////////////////////////////////////////////
procedure ShowMessageDlag(smsg: string);
begin
   if sLanguage = 'zh-cn' then
      smsg := GBCht2Chs(smsg);

   // W-140530 2．	CSR 按 “派车”bug：已有电话，却说没电话。另外，这个讯息框里，字太小。
   gf_ShowMsg(smsg); // ,mtcustom,[mbOK],0)
end;

function gf_Get0Value(carList_ID: integer; as_SQL: String): Variant;
var
   // MysqlConn: TZConnection;
   Zquery: TFDQuery;
begin
   Result := null;
   // MysqlConn := TZConnection.Create(self);
   Zquery := TFDQuery.Create(nil);
   try
      { MysqlConn.Protocol := 'mysql';
        MysqlConn.Properties.Add('oidasblob=true');
        MysqlConn.HostName := zHostName;
        MysqlConn.Database := zDatabase;
        MysqlConn.Port := zPort;
        MysqlConn.User := zUser;
        MysqlConn.password := zPassword;
      } Zquery.Connection := dm1.FDConnection1;
      Zquery.Close;
      Zquery.SQL.Text := as_SQL;
      try
         dm1.FDConnection1.Connected := True;
         Zquery.Open;
         Result := Zquery.Fields[0].Value;
      except
         on E: Exception do
            gf_ShowMsg('出現錯誤：' + E.Message + '，' + Zquery.SQL.Text);
      end;
   finally
      Zquery.Active := false;
      // MysqlConn.Disconnect;
      Zquery.Free;
      // MysqlConn.Free;
   end;
end;

function GetLocaleLang: string;
var
   iThread: integer;
   s: string;
begin
   // 繁簡偵測
   iThread := GetThreadLocale; // 呼叫api
   if iThread = 2052 then // 中國  zh-cn
      s := 'zh-cn'
   else
      s := ' zh-tw';

   Result := s;

end;

Function GBCht2Chs(sBIG5Str: String): unicodeString; // 繁体转简体
var
   sGBstr: string;
begin
   SetLength(sGBstr, Length(sBIG5Str));
   LCMapString($804, LCMAP_SIMPLIFIED_CHINESE, pchar(sBIG5Str),
       Length(sBIG5Str), pchar(sGBstr), Length(sGBstr));
   Result := sGBstr;

end;

procedure big5TOgbk(Aform: Tform);
var
   i: integer; // ,j
begin

   Aform.Caption := GBCht2Chs(Aform.Caption);

   for i := 0 to Aform.ComponentCount - 1 do begin
      if Aform.Components[i] is TMenuItem then
         TMenuItem(Aform.Components[i]).Caption :=
             GBCht2Chs(TMenuItem(Aform.Components[i]).Caption)
      else if Aform.Components[i] is Tspeedbutton then begin
         if Tspeedbutton(Aform.Components[i]).Hint <> '' then begin
            try
               Tspeedbutton(Aform.Components[i])
                   .Glyph.LoadFromFile(sFilePath + 'image\' +
                   Tspeedbutton(Aform.Components[i]).Hint);
            except
               // 忽略
            end;
         end
         else
            Tspeedbutton(Aform.Components[i]).Caption :=
                GBCht2Chs(Tspeedbutton(Aform.Components[i]).Caption)
      end
      else if Aform.Components[i] is Tbutton then
         Tbutton(Aform.Components[i]).Caption :=
             GBCht2Chs(Tbutton(Aform.Components[i]).Caption)
      else if Aform.Components[i] is Tlabel then
         Tlabel(Aform.Components[i]).Caption :=
             GBCht2Chs(Tlabel(Aform.Components[i]).Caption)
      else if Aform.Components[i] is TBitbtn then
         TBitbtn(Aform.Components[i]).Caption :=
             GBCht2Chs(TBitbtn(Aform.Components[i]).Caption)
      else if Aform.Components[i] is TTabSheet then
         TTabSheet(Aform.Components[i]).Caption :=
             GBCht2Chs(TTabSheet(Aform.Components[i]).Caption)
      else if Aform.Components[i] is TGroupBox then
         TGroupBox(Aform.Components[i]).Caption :=
             GBCht2Chs(TGroupBox(Aform.Components[i]).Caption)
      else if Aform.Components[i] is Tcheckbox then
         Tcheckbox(Aform.Components[i]).Caption :=
             GBCht2Chs(Tcheckbox(Aform.Components[i]).Caption)
      else if Aform.Components[i] is TRadiobutton then
         TRadiobutton(Aform.Components[i]).Caption :=
             GBCht2Chs(TRadiobutton(Aform.Components[i]).Caption)
      else if Aform.Components[i] is TBitbtn then
         Tcheckbox(Aform.Components[i]).Caption :=
             GBCht2Chs(Tcheckbox(Aform.Components[i]).Caption);

   end;

end;

/// /////////////////////////////////////////////////////////////////////////////////////////////////////

function LoadansistringFromFile(anFile: string): AnsiString;
var
   Filelist: tstringlist;
begin
   Filelist := tstringlist.Create;
   Filelist.LoadFromFile(anFile);
   Result := AnsiString(Filelist.Text);
   Filelist.Free;
end;

function GetDistance(lng1, lat1, lng2, lat2: Double): Double;
var
   c, s: Double;
begin

   c := Sin(lat1) * Sin(lat2) + Cos(lat1) * Cos(lat2) * Cos(lng1 - lng2);
   s := EARTH_RADIUS * arccos(c) * (3.14159265 / 180);
   Result := Trunc(s * 10000) / 10000;
end;

function ConvertDegreeToRadians(const Degrees: Double): Double;
begin
   Result := (PI / 180) * Degrees;
end;

// 以下計算式參考自
// http://www.dotblogs.com.tw/jeff-yeh/archive/2009/02/04/7034.aspx
// 回傳值為公里
function DistanceBetweenPoint2(const StartLong, StartLat, EndLong,
    EndLat: Double): Double;
var
   Lat1r, Lat2r, Long1r, Long2r: Double;
   D: Double;
const
   R: Double = 6378.137; // Earth's radius (km)
begin
   Lat1r := ConvertDegreeToRadians(StartLat);
   Lat2r := ConvertDegreeToRadians(EndLat);
   Long1r := ConvertDegreeToRadians(StartLong);
   Long2r := ConvertDegreeToRadians(EndLong);

   D := Math.arccos(Sin(Lat1r) * Sin(Lat2r) + Cos(Lat1r) * Cos(Lat2r) *
       Cos(Long2r - Long1r)) * R;
   Result := D;
end;

function CheckReservedWord(Str: string): string;
var
   i: integer;
begin
   Result := '';
   for i := 1 to Length(Str) do begin
      if (Str[i] <> ',') and (Str[i] <> ';') then
         Result := Result + Str[i];
   end;
end;

end.
