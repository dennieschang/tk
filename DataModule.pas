unit DataModule;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, XMLDoc, inifiles, XMLIntf, DB, Forms, ADODB, DateUtils,
  unitGlobel, activex,
  FireDAC.Stan.Intf, FireDAC.Stan.Param, FireDAC.Phys.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.Comp.UI, FireDAC.Phys.MySQL, FireDAC.Phys.MySQLDef;

type
  TDM1 = class(TDataModule)
    dsPsgr: TDataSource;
    dspsgr_locbk: TDataSource;
    UpdateSQL_psgrlocbk: TFDUpdateSQL;
    ZUpdateSQL_Calltx: TFDUpdateSQL;
    DataSource1: TDataSource;
    FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink;
    FDConnection1: TFDConnection;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    qy1: TFDQuery;
    qyIncall: TFDQuery;
    qyCalltxHist: TFDQuery;
    qyPsgr: TFDQuery;
    qyCalltx: TFDQuery;
    qyWriteIn: TFDQuery;
    qypsgr_locbk: TFDQuery;
    qyDrv: TFDQuery;
    zqTemp: TFDQuery;
    zq_DeferCall_: TFDQuery;
    q_Recall: TFDQuery;
    procedure DataModuleCreate(Sender: TObject);
  private

    { Private declarations }
  public
    { Public declarations }
    gs_TestIP : String;
    procedure readDatabaseini;
    function gp_ExecuteSQL(av_SQL: String): integer;
    function gp_Set_psgr_locbk(iPgid, iquid: Integer; sLoc, sAddr, sPS: String;
      sLong, slat: real): String;

  end;

var
  DM1: TDM1;

implementation



{$R *.dfm}

uses unitSendCar;


function TDM1.gp_ExecuteSQL(av_SQL: String): integer;
var
  aFDCommand: TFDCommand;
begin
  Result := -1;
  aFDCommand := TFDCommand.Create(nil);
  try
    with aFDCommand Do
    begin
      Connection := dm1.FDConnection1;
      try
        CommandText.Clear;
        CommandText.Add(av_SQL);
        Execute();
        Result:=aFDCommand.RowsAffected;
      except
        on E: Exception do
        begin
          frmSendCar.ShowAddMessage(0,'出現錯誤：' + E.Message + '，' + av_SQL);
        end;
      end;
    end;
  finally
    if Assigned(aFDCommand) then
      aFDCommand.Free;

  end;
end;




procedure TDM1.readDatabaseini;
var
  Fini: Tinifile;
  sPath:string;
begin
//  zconMySQL.Protocol := 'mysql';
  sPath := ExtractFilePath(ParamStr(0));
  Fini := nil;
  try
    Fini := Tinifile.Create(sPath+System_INI_FileName);
    FDConnection1.Params.Values['Server'] := Fini.ReadString('CSR', 'DatabaseIP', '');
    FDConnection1.Params.Values['Database'] := Fini.ReadString('CSR', 'DatabaseName', '');
    FDConnection1.Params.Values['User_Name'] := Fini.ReadString('CSR', 'DatabaseAcc', '');
    FDConnection1.Params.Values['Password'] := Fini.ReadString('CSR', 'DatabasePass', '');
    FDConnection1.Params.Values['Port'] := Fini.ReadString('CSR', 'DatabasePort', '3306');
    gs_TestIP := Fini.ReadString('CSR', 'TestIP', '114.33.151.110');
  finally
    Fini.Free;
  end;

end;





procedure TDM1.DataModuleCreate(Sender: TObject);
//var
//  SystemDir, disN: string;
begin
  readDatabaseini;

//  zconMySQL.Protocol := 'mysql';
//  zconMySQL.Properties.Add('oidasblob=true');

 // ADOConnection1.Connected := false;
 //// ADOConnection1.ConnectionString := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' +
 //   ExtractFilePath(Application.EXEName) + 'data\CSR.mdb' + ';Persist Security Info=False; ' +
 //   'Jet OLEDB:Database Password=88865';

 //2013/05/12 MELODY 拿掉搬移圖片，直接讀取\image FOLDER
{  SystemDir := ExtractFilePath(Application.EXEName);
  disN := Copy(SystemDir, 1, 2);
  if not FileExists(disN + '\icontt.png') then MyCopyFile(SystemDir + 'image\icontt.png', disN + '\icontt.png');
  if not FileExists(disN + '\icont2.png') then MyCopyFile(SystemDir + 'image\icont2.png', disN + '\icont2.png');
  if not FileExists(disN + '\icon_red.png') then MyCopyFile(SystemDir + 'image\icon_red.png', disN + '\icon_red.png');
  if not FileExists(disN + '\icon_green.png') then MyCopyFile(SystemDir + 'image\icon_green.png', disN + '\icon_green.png');
  if not FileExists(disN + '\redico.png') then MyCopyFile(SystemDir + 'image\redico.png', disN + '\redico.png');
  if not FileExists(disN + '\greenico.png') then MyCopyFile(SystemDir + 'image\greenico.png', disN + '\greenico.png');
 }
end;


function TDM1.gp_Set_psgr_locbk(iPgid,iquid:Integer; sLoc, sAddr, sPS:String; sLong,
  slat: real): String;
begin
  with dm1.qypsgr_locbk do
  begin
    Close;
    SQL.Text := 'select *' + '  from psgr_locbk where pgid = ' + IntToStr(iPgid) +
      '   and lbloc = ' + quotedstr(sLoc) + ' order by lbdtlast desc limit 1';
    open;

    if not IsEmpty then
      // locate('pgid;lbloc', VarArrayOf([sPgid, sLoc]), []) then
      Edit
    else
    begin
      append;
      fieldbyname('pgid').AsInteger := iPgid;
      fieldbyname('lbloc').asstring := sLoc;
      FieldByName('lbdtcreate').AsDateTime := Now;
    end;

    fieldbyname('lblocref').asstring := sPS;
    fieldbyname('lblong').AsFloat := sLong;
    fieldbyname('lblat').AsFloat := slat;
    fieldbyname('lbdtlast').AsDateTime := now;
    FieldByName('lbquid').AsInteger:=iquid;
    Post;
  end;


end;




end.

