object DM1: TDM1
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 350
  Width = 446
  object dsPsgr: TDataSource
    DataSet = qyPsgr
    Left = 260
    Top = 76
  end
  object dspsgr_locbk: TDataSource
    DataSet = qypsgr_locbk
    Left = 344
    Top = 144
  end
  object UpdateSQL_psgrlocbk: TFDUpdateSQL
    InsertSQL.Strings = (
      'INSERT INTO psgr_locbk'
      '  (pgid,lbloc, lblocref, lblong, lblat, lbdtlast)'
      'VALUES'
      '  (:pgid,:lbloc, :lblocref, :lblong, :lblat, sysdate())')
    ModifySQL.Strings = (
      'UPDATE psgr_locbk SET'
      '  lbloc = :lbloc,'
      '  lblocref = :lblocref,'
      '  lblong = :lblong,'
      '  lblat = :lblat,'
      '  lbdtlast = sysdate()'
      'WHERE'
      '  psgr_locbk.pid = :OLD_pid  AND'
      '  psgr_locbk.pgid = :OLD_pgid')
    DeleteSQL.Strings = (
      'DELETE FROM psgr_locbk'
      'WHERE'
      '  psgr_locbk.pid = :OLD_pid AND'
      
        '  ((psgr_locbk.pgid IS NULL AND :OLD_pgid IS NULL) OR (psgr_locb' +
        'k.pgid = :OLD_pgid))')
    Left = 260
    Top = 208
  end
  object ZUpdateSQL_Calltx: TFDUpdateSQL
    InsertSQL.Strings = (
      'INSERT INTO calltx'
      '(pgid,cxdtcreate,cxfrloc,cxfrlocref,cxfrlong,cxfrlat,cxtoloc,'
      
        ' cxtolocref,cxtolong,cxtolat,csid,mcid,cxoutlineno,cxpgphone,dcn' +
        'o)'
      'VALUES'
      
        '  (:pgid,sysdate(),:cxfrloc,:cxfrlocref,:cxfrlong,:cxfrlat,:cxto' +
        'loc,'
      
        '   :cxtolocref,:cxtolong,:cxtolat,:csid,:mcid,:cxoutlineno,:cxpg' +
        'phone,:dcno)')
    ModifySQL.Strings = (
      'UPDATE calltx SET'
      '   cxsuccess=:cxsuccess'
      'WHERE'
      '   calltx.cxno = :OLD_cxno ')
    DeleteSQL.Strings = (
      'DELETE FROM calltx'
      'WHERE'
      ' cxno = :cxno')
    Left = 132
    Top = 268
  end
  object DataSource1: TDataSource
    DataSet = qyCalltxHist
    Left = 260
    Top = 268
  end
  object FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink
    Left = 132
    Top = 80
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=hypertaxidb'
      'User_Name=htmgmt'
      'Password=slbthtmgmt'
      'Server=114.33.151.110'
      'CharacterSet=utf8'
      'DriverID=MySQL')
    LoginPrompt = False
    Left = 32
    Top = 16
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    ScreenCursor = gcrNone
    Left = 130
    Top = 16
  end
  object qy1: TFDQuery
    Connection = FDConnection1
    Left = 32
    Top = 76
  end
  object qyIncall: TFDQuery
    Connection = FDConnection1
    Left = 32
    Top = 144
  end
  object qyCalltxHist: TFDQuery
    Connection = FDConnection1
    Left = 36
    Top = 212
  end
  object qyPsgr: TFDQuery
    Connection = FDConnection1
    Left = 40
    Top = 280
  end
  object qyCalltx: TFDQuery
    Connection = FDConnection1
    Left = 132
    Top = 204
  end
  object qyWriteIn: TFDQuery
    Connection = FDConnection1
    Left = 260
    Top = 20
  end
  object qypsgr_locbk: TFDQuery
    Connection = FDConnection1
    Left = 260
    Top = 144
  end
  object qyDrv: TFDQuery
    Connection = FDConnection1
    Left = 344
    Top = 24
  end
  object zqTemp: TFDQuery
    Connection = FDConnection1
    Left = 344
    Top = 84
  end
  object zq_DeferCall_: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      
        'select a.*, b.pgnm RecvName, c.pgnm ExecName, c.pggender, c.pgid' +
        ', b.pgnm as_State'
      '  from Defercall a, psgr b, psgr c'
      ' where a.dccsidcreate = b.pgid'
      '   and a.dccsidexec = c.pgid')
    Left = 364
    Top = 216
  end
  object q_Recall: TFDQuery
    Connection = FDConnection1
    Left = 132
    Top = 148
  end
end
