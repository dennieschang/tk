// CSR 2.2.1 Issue 2.	未指派司机的 calltx （即 dvno is null） 不能接受 取消派�Q 要求
// CSR 2.2.1 Issue 7.	CSR SQL Error
unit Paiche;

interface

uses
   Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
   System.Classes, Vcl.Graphics,
   Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.Grids, Vcl.DBGrids,
   Vcl.ExtCtrls, Data.DB,
   Vcl.StdCtrls, Vcl.Buttons, FireDAC.Stan.Intf, FireDAC.Stan.Option,
   FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
   FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
   FireDAC.Comp.Client;

type
   TfPaiche = class(TForm)
      Panel1: TPanel;
      DBGrid1: TDBGrid;
      zq_CallTx_: TFDQuery;
      ds_CallTx: TDataSource;
      BitBtn1: TBitBtn;
      BitBtn2: TBitBtn;
      PanelCalltxDetail: TPanel;
      Panel3: TPanel;
      DBGrid2: TDBGrid;
      DataSource1: TDataSource;
      FDMemTable1: TFDMemTable;
      CheckBox1: TCheckBox;
      procedure FormShow(Sender: TObject);
      procedure FormClose(Sender: TObject; var Action: TCloseAction);
      procedure BitBtn2Click(Sender: TObject);
      procedure zq_CallTx_AfterScroll(DataSet: TDataSet);
      procedure CheckBox1Click(Sender: TObject);
      procedure DBGrid1DblClick(Sender: TObject);
   private
      { Private declarations }
      gs_Path: String;
   public
      { Public declarations }
   end;

var
   fPaiche: TfPaiche;

implementation

uses DataModule, unitGlobel, CDMsg, unitSendCar, IdGlobal, HttpApp, ShellAPI;

{$R *.dfm}

procedure TfPaiche.BitBtn2Click(Sender: TObject);
begin
   FormShow(nil);
end;

procedure TfPaiche.CheckBox1Click(Sender: TObject);
begin
   PanelCalltxDetail.Visible := CheckBox1.Checked;
end;

procedure TfPaiche.DBGrid1DblClick(Sender: TObject);
begin
   ShellExecute(Application.Handle, 'Open', PChar(gs_Path + 'TK.exe'),
       PChar('已派 ' + zq_CallTx_.FieldByName('cxpgphone').AsString + ' ' +
       zq_CallTx_.FieldByName('cxno').AsString), PChar(gs_Path), 1);
end;

procedure TfPaiche.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   zq_CallTx_.Close;
   zq_CallTx_.Active := False;
//   Action := caHide;
end;

procedure TfPaiche.FormShow(Sender: TObject);
begin
   // CSR 2.2.1 Issue 7.	CSR SQL Error
   // DM1.zconMySQL.Connected := False;
   gs_Path := ExtractFilePath(Application.ExeName);

   if gs_Path[Length(gs_Path)] <> '\' then
      gs_Path := gs_Path + '\';

   if zq_CallTx_.Active then
      zq_CallTx_.Active := False;

   zq_CallTx_.Close;
   zq_CallTx_.SQL.Text :=
       'select a.cxoutlineno, a.cxpgphone, DATE_FORMAT(a.cxdtcreate,''%Y-%m-%d %k:%i'') cxdtcreate , a.dvno,'
       + 'a.cxdtcreate, a.cxfrloc, a.quid, a.cxdtconfirm, a.cxdtpickup, a.cxdtpgnotfound,'
       + 'a.cxdtfinish,a.csid, a.cxdtcsrclick,a.cxno,a.cxtimetopickup,' +
       '       CONCAT(ancode, '' '', anmeaning) as_State,' + '       c.pgnm,  '
       + '       concat(a.quid, '' '', d.qunm) qunm' + '  from calltx a ' +
       '   left join attname b on b.ancategoy = ''cxsuccess''  and a.cxsuccess = b.ancode '
       + '   left join psgr c on a.pgid = c.pgid' +
       '   left join queue d on a.mcid = d.mcid and a.quid = d.quid' +
       ' where a.mcid = ' + IntToStr(MotorCade.iID) + '   and a.cxpgphone = ' +
       QuotedStr(frmSendCar.cds_ExtBuffer_ebPhone.AsString) +
       '   and a.asno = ''''';
   {
     zq_CallTx_.Close;
     zq_CallTx_.SQL.Text :=
     'select cxdtcreate, cxfrloc, dvno, quid, cxdtconfirm, cxdtpickup, cxdtpgnotfound,' +
     '       cxdtfinish, CONCAT(ancode, '' '', anmeaning) as_State,' +
     '       csid, cxdtcsrclick' +
     '  from calltx a, attname b where b.ancategoy = ''cxsuccess''' +
     '   and a.cxsuccess = b.ancode' +
     '   and mcid = ' + IntToStr(MotorCade.iID) +
     '   and cxpgphone = ' + QuotedStr(frmSendCar.cds_ExtBuffer_ebPhone.AsString) +
     '   and asno = ''''';
   }
//   if Sender <> nil then // 不是全部派車紀錄
//   begin
//      zq_CallTx_.SQL.Text := zq_CallTx_.SQL.Text + '   and cxdtcreate >= :a1';
//      zq_CallTx_.ParamByName('a1').AsDateTime := Now - 30;
//   end;

   zq_CallTx_.SQL.Text := zq_CallTx_.SQL.Text + ' order by cxdtcreate desc';
   zq_CallTx_.Open;
//   FDMemTable1.Filtered := False;
//   FDMemTable1.CloneCursor(zq_CallTx_, False, False);
//   FDMemTable1.Filter := 'cxno=' + zq_CallTx_.FieldByName('cxno').AsString;
//   FDMemTable1.Filtered := True;

end;

procedure TfPaiche.zq_CallTx_AfterScroll(DataSet: TDataSet);
begin
//   if FDMemTable1.Active and zq_CallTx_.Active then begin
//      FDMemTable1.Filter := 'cxno=' + zq_CallTx_.FieldByName('cxno').AsString;
//      FDMemTable1.Filtered := True;
//      FDMemTable1.First;
//   end;

end;

end.
