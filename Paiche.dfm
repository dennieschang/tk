object fPaiche: TfPaiche
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = #27966#36554#32000#37636
  ClientHeight = 616
  ClientWidth = 1076
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = #32048#26126#39636
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1076
    Height = 35
    Align = alTop
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 601
      Top = 1
      Width = 89
      Height = 31
      Caption = #38364#38281
      Kind = bkClose
      NumGlyphs = 2
      TabOrder = 0
    end
    object BitBtn2: TBitBtn
      Left = 1
      Top = 2
      Width = 176
      Height = 31
      Caption = #20840#37096#27966#36554#32000#37636
      Glyph.Data = {
        F2010000424DF201000000000000760000002800000024000000130000000100
        0400000000007C01000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333334433333
        3333333333388F3333333333000033334224333333333333338338F333333333
        0000333422224333333333333833338F33333333000033422222243333333333
        83333338F3333333000034222A22224333333338F33F33338F33333300003222
        A2A2224333333338F383F3338F33333300003A2A222A222433333338F8333F33
        38F33333000034A22222A22243333338833333F3338F333300004222A2222A22
        2433338F338F333F3338F3330000222A3A2224A22243338F3838F338F3338F33
        0000A2A333A2224A2224338F83338F338F3338F300003A33333A2224A2224338
        333338F338F3338F000033333333A2224A2243333333338F338F338F00003333
        33333A2224A2233333333338F338F83300003333333333A2224A333333333333
        8F338F33000033333333333A222433333333333338F338F30000333333333333
        A224333333333333338F38F300003333333333333A223333333333333338F8F3
        000033333333333333A3333333333333333383330000}
      NumGlyphs = 2
      TabOrder = 1
      Visible = False
      OnClick = BitBtn2Click
    end
    object CheckBox1: TCheckBox
      Left = 208
      Top = 8
      Width = 113
      Height = 17
      Caption = #39023#31034#27966#36554#21934
      TabOrder = 2
      Visible = False
      OnClick = CheckBox1Click
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 35
    Width = 1076
    Height = 397
    Align = alClient
    DataSource = ds_CallTx
    ImeName = #23567#29436#27627
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -16
    TitleFont.Name = #32048#26126#39636
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'cxdtcreate'
        Title.Caption = #27966#36554#26178#38291
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'cxfrloc'
        Title.Caption = #19978#36554#22320#40670
        Width = 162
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'dvno'
        Title.Caption = #21496#27231#21488#34399
        Width = 73
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'quid'
        Title.Caption = #25490#29677#40670
        Width = 56
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'cxdtconfirm'
        Title.Caption = #21496#27231#30906#35469
        Width = 95
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'cxdtpickup'
        Title.Caption = #36617#21040#23458#20154
        Width = 93
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'cxdtpgnotfound'
        Title.Caption = #19981#35211#20056#23458
        Width = 91
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'cxdtfinish'
        Title.Caption = #21496#27231#23436#25104#26381#21209
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'as_State'
        Title.Caption = #29376#24907
        Width = 101
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'csid'
        Title.Caption = #27966#36554#24115#34399
        Width = 74
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'cxdtcsrclick'
        Title.Caption = #27966#36554#23436#25104
        Width = 95
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'cxno'
        Title.Caption = #27966#36554#21934#34399
        Visible = True
      end>
  end
  object PanelCalltxDetail: TPanel
    Left = 0
    Top = 432
    Width = 1076
    Height = 184
    Align = alBottom
    TabOrder = 2
    Visible = False
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1074
      Height = 41
      Align = alTop
      Caption = #27966#36554#21934
      Color = 16771022
      ParentBackground = False
      TabOrder = 0
    end
    object DBGrid2: TDBGrid
      Left = 1
      Top = 42
      Width = 1074
      Height = 141
      Align = alClient
      DataSource = DataSource1
      ImeName = #23567#29436#27627
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      ReadOnly = True
      TabOrder = 1
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -16
      TitleFont.Name = #32048#26126#39636
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'cxdtcreate'
          Title.Caption = #27966#36554#26178#38291
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'cxpgphone'
          Title.Caption = #23458#25142#38651#35441
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'pgnm'
          Title.Caption = #22995#21517
          Width = 42
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'cxfrloc'
          Title.Caption = #19978#36554#22320#40670
          Width = 298
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'qunm'
          Title.Caption = #25490#29677#40670
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'dvno'
          Title.Caption = #21496#27231#21488#34399
          Width = 96
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'cxtimetopickup'
          Title.Alignment = taRightJustify
          Title.Caption = #20998#37912
          Width = 54
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'cxkmtopickup'
          Title.Alignment = taCenter
          Title.Caption = #20844#37324
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'as_State'
          Title.Caption = #29376#24907
          Width = 125
          Visible = True
        end>
    end
  end
  object zq_CallTx_: TFDQuery
    Connection = DM1.FDConnection1
    SQL.Strings = (
      'select *, cxfrLoc as_State from calltx limit 10')
    Left = 268
    Top = 164
  end
  object ds_CallTx: TDataSource
    DataSet = zq_CallTx_
    Left = 328
    Top = 164
  end
  object DataSource1: TDataSource
    DataSet = FDMemTable1
    Left = 516
    Top = 542
  end
  object FDMemTable1: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired]
    UpdateOptions.CheckRequired = False
    Left = 432
    Top = 536
  end
end
