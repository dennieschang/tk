unit unitAddList;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.DBGrids, Data.DB,
  Vcl.Buttons, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Vcl.Menus;

type
  TfrmAddlist = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    zq_psgr_locbk: TFDQuery;
    ds_psgr_locbk: TDataSource;
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    PopupMenu1: TPopupMenu;
    D1: TMenuItem;
    // procedure grdAddListClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure D1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    pgid: Integer;
    gb_In: Boolean;
  end;

var
  frmAddlist: TfrmAddlist;

implementation

uses datamodule, unitSendCar, CDMsg;

{$R *.dfm}

procedure TfrmAddlist.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmAddlist.Button1Click(Sender: TObject);
begin
  zq_psgr_locbk.First;

  while not zq_psgr_locbk.Eof do
  begin
    if AnsiPos(Edit1.Text, zq_psgr_locbk.FieldByName('lbloc').AsString+zq_psgr_locbk.FieldByName('lblocref').AsString ) > 0 then
      Exit;

    zq_psgr_locbk.Next;
  end;
end;

procedure TfrmAddlist.D1Click(Sender: TObject);
begin
  if gf_ShowMsg('確定要刪除這筆資料嗎？', 'CSR', mtConfirmation, 1) = mrNo then
    Exit;

  zq_psgr_locbk.Delete;
end;

procedure TfrmAddlist.DBGrid1CellClick(Column: TColumn);
begin
  with frmSendcar do
  begin
    if gb_In then
    begin
      cds_ExtBuffer_ebFrLoc.AsString := zq_psgr_locbk.FieldByName
        ('lbLoc').AsString;
      // cds_ExtBuffer_ebFrAddr.AsString := zq_psgr_locbk.FieldByName('lbAddr').AsString;
      cds_ExtBuffer_ebFrLocRef.AsString := zq_psgr_locbk.FieldByName
        ('lbLocRef').AsString;
      cds_ExtBuffer_ebFrLat.AsString := zq_psgr_locbk.FieldByName
        ('lblat').AsString;
      cds_ExtBuffer_ebFrLng.AsString := zq_psgr_locbk.FieldByName
        ('lblong').AsString;

      if zq_psgr_locbk.FieldByName('lbquid').AsString = '' then
      begin
        Label45.Caption := '無';
        Label45.Font.Color := clBlack;
        ComboBox4.ItemIndex := 0;
      end
      else
      begin
        Label45.Font.Color := clRed;
        ComboBox4.ItemIndex := gt_quid.IndexOf(zq_psgr_locbk.FieldByName('lbquid').AsString);
        Label45.Caption := '排班站：' + ComboBox4.Text;
      end;

      if  ComboBox4.ItemIndex<0 then
      begin
        Label45.Caption := '無';
        Label45.Font.Color := clBlack;
        ComboBox4.ItemIndex := 0;
      end;


      RadioButton6.Checked := True;
    end
    else
    begin
      cds_ExtBuffer_ebToLoc.AsString := zq_psgr_locbk.FieldByName
        ('lbLoc').AsString;
      // cds_ExtBuffer_ebToAddr.AsString := zq_psgr_locbk.FieldByName('lbAddr').AsString;
      cds_ExtBuffer_ebToLocRef.AsString := zq_psgr_locbk.FieldByName
        ('lbLocRef').AsString;
      cds_ExtBuffer_ebToLat.AsString := zq_psgr_locbk.FieldByName
        ('lblat').AsString;
      cds_ExtBuffer_ebToLng.AsString := zq_psgr_locbk.FieldByName
        ('lblong').AsString;
      RadioButton8.Checked := True;
    end;
  end; // with frmSendcar do

  // frmAddlist.Hide;
  Close;
end;

procedure TfrmAddlist.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_Return then
    Button1Click(Sender);
end;

procedure TfrmAddlist.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caHide;
end;

procedure TfrmAddlist.FormShow(Sender: TObject);
begin
  // DM1.zconMySQL.Disconnect;
  zq_psgr_locbk.Close;
  zq_psgr_locbk.SQL.Text := 'select a.*, concat(a.lbquid, '' '', b.qunm) lbqunm'
    + '  from psgr_locbk a join psgr c on a.pgid = c.pgid left join queue b'
    + '    on c.mcid = b.mcid and a.lbquid = b.quid where a.pgid = ' +
    IntToStr(pgid) + ' order by a.lbdtlast desc';
  zq_psgr_locbk.Open;
end;

end.
